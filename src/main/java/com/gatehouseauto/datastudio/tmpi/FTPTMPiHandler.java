package com.gatehouseauto.datastudio.tmpi;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;

import org.apache.commons.net.ftp.FTP;
import org.apache.commons.net.ftp.FTPClient;
import org.apache.commons.net.ftp.FTPFile;
import org.apache.commons.net.ftp.FTPReply;

import com.gatehouseauto.datastudio.constant.ApplicationConstants;
import com.gatehouseauto.datastudio.constant.PropertyValue;
import com.gatehouseauto.datastudio.sns.SNSHandler;
import com.gatehouseauto.datastudio.util.LoggingUtils;
import com.gatehouseauto.datastudio.util.PropertyLoaderUtil;

public class FTPTMPiHandler {
	private static String FTPSERVER_ADDRESS = "inventory.TargetMediaPartners.com";
	private static String FTPSERVER_USR = "bestrideftp";  
	private static String FTPSERVER_PWD = "F&a9HwUV"; 
	private static String FTPSERVER_FOLDERNAME = "/Outbound/";


	private static FTPClient ftp = new FTPClient();
	
	public static String downlaodTodayFile(){
		
  	    if(PropertyValue.DB_URL == null || "".equals(PropertyValue.DB_URL.trim())){
		   PropertyLoaderUtil.loadProperties();
	    }
		
		try{
			ftp.connect(FTPSERVER_ADDRESS);		
			
			if(!ftp.login(FTPSERVER_USR, FTPSERVER_PWD))
            {
                ftp.logout();
                return null;
            }
			
            int reply = ftp.getReplyCode();
            //FTPReply stores a set of constants for FTP reply codes.
            if (!FTPReply.isPositiveCompletion(reply))
            {
                ftp.disconnect();
                return null;
            }
            
            ftp.enterLocalPassiveMode();
            ftp.setFileType(FTP.BINARY_FILE_TYPE);
            ftp.changeWorkingDirectory(FTPSERVER_FOLDERNAME);
            
            FTPFile ftpFiles[] = ftp.listFiles()	;
            FTPFile todayFTPFile = null;
            for(FTPFile ftpFile : ftpFiles){
             	String fileName = ftpFile.getName().substring(0, ftpFile.getName().length()-9);
            	 
            	if(fileName.contains(ApplicationConstants.TMPI_ARGUMENT_FILE_DATE)){
            		todayFTPFile = ftpFile;
            		break;
            	}
            }
            
            if(todayFTPFile == null){
            	return null;
            }
            
            String selectedFileName = todayFTPFile.getName();
            	
            File downloadFile = new File(PropertyLoaderUtil.usedPath+selectedFileName);
            
            OutputStream outputStream = new BufferedOutputStream(new FileOutputStream(downloadFile));
            ftp.retrieveFile(selectedFileName, outputStream);
            outputStream.close();
 
            return selectedFileName;
		}catch(Exception e){
			e.printStackTrace();
			
			String exception = LoggingUtils.getExceptionString(e);
			LoggingUtils.logInfo(exception);
			SNSHandler.sendSimpleMessage(exception, true);
		}

		return null;
	}

}
