package com.gatehouseauto.datastudio.tmpi.constants;

public enum VehicleListEnum {
	CAPTURED_DATE("captured", "captured_date", 1),
	ACC_ID("acc_ID", "acc_id", 2),
	LST_ID("lst_id", "lst_id", 3),
	PRICE("lst_price", "price", 4),
	PRICE_RANGE("pricerange", "price_range", 5),
	EMPTY1("lst_count", "empty1", 6),
	VIN("veh_vin", "vin", 7),
	YEAR("veh_year", "year", 8),
	MAKE("veh_make", "make", 9),
	MODEL("veh_model", "model", 10),
	TRIM("veh_trim", "trim", 11),
	CREATED("lst_created", "created", 12),
	LAST_UPLOAD("lst_lastupload", "last_upload", 13),
	STATUS("lst_status", "status", 14);
	
	private String fileColumn;
	private String tableColumn;
	private int columnOrder;
	  
	VehicleListEnum(String fileColumn, String tableColumn, int columnOrder){
		this.fileColumn = fileColumn;
		this.tableColumn = tableColumn;
		this.columnOrder = columnOrder;
	}


	public String getFileColumn() {
		return fileColumn;
	}


	public String getTableColumn() {
		return tableColumn;
	}


	public int getColumnOrder() {
		return columnOrder;
	}
}
