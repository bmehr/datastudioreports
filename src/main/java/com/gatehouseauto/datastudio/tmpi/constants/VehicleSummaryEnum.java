package com.gatehouseauto.datastudio.tmpi.constants;

public enum VehicleSummaryEnum {
	ACC_ID("acc_ID", "acc_id", 1),
	PAC_ID("pac_ID", "pac_id", 2),
	PAC_NAME("pac_Name", "pac_name", 3),
	PAC_SHORT_NAME("pac_ShortName", "pac_short_name", 4),
	PAC_SORT("pac_Sort", "pac_sort", 5),
	TOTAL("total", "total", 6),
	COUNT("count", "count", 7),
	EXPORTED("exported", "exported", 8);
	
	private String fileColumn;
	private String tableColumn;
	private int columnOrder;
	  
	VehicleSummaryEnum(String fileColumn, String tableColumn, int columnOrder){
		this.fileColumn = fileColumn;
		this.tableColumn = tableColumn;
		this.columnOrder = columnOrder;
	}


	public String getFileColumn() {
		return fileColumn;
	}


	public String getTableColumn() {
		return tableColumn;
	}


	public int getColumnOrder() {
		return columnOrder;
	}
}
