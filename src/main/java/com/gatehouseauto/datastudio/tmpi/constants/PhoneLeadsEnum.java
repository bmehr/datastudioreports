package com.gatehouseauto.datastudio.tmpi.constants;

public enum PhoneLeadsEnum {	
	ACC_ID("acc_ID", "acc_id", 1),
	LEAD_ID("LeadID", "lead_id", 2),
	LEAD_DATE("Captured", "lead_date", 3),
	FIRST_NAME("FirstName", "first_name", 4),
	LAST_NAME("LastName", "last_name", 5),
	PHONE("Phone", "phone", 6),
	ZIP("Zip", "zip", 7);
	
	private String fileColumn;
	private String tableColumn;
	private int columnOrder;
	  
	PhoneLeadsEnum(String fileColumn, String tableColumn, int columnOrder){
		this.fileColumn = fileColumn;
		this.tableColumn = tableColumn;
		this.columnOrder = columnOrder;
	}


	public String getFileColumn() {
		return fileColumn;
	}


	public String getTableColumn() {
		return tableColumn;
	}


	public int getColumnOrder() {
		return columnOrder;
	}
}
