package com.gatehouseauto.datastudio.tmpi.constants;

public enum DisplayClicksEnum {
	ACC_ID("acc_ID", "acc_id", 1),
	CAMPAIGN_ID("_Master_ID", "campaign_id", 2),
	CAPTURED_DATE("Captured", "captured_date", 3),
	CLICKS("Clicks", "clicks", 4),
	IMPRESSIONS("Impressions", "impressions", 5);
	
	private String fileColumn;
	private String tableColumn;
	private int columnOrder;

	  
	DisplayClicksEnum(String fileColumn, String tableColumn, int columnOrder){
		this.fileColumn = fileColumn;
		this.tableColumn = tableColumn;
		this.columnOrder = columnOrder;
	}


	public String getFileColumn() {
		return fileColumn;
	}


	public String getTableColumn() {
		return tableColumn;
	}


	public int getColumnOrder() {
		return columnOrder;
	}
}
