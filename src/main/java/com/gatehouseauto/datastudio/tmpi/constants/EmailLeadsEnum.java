package com.gatehouseauto.datastudio.tmpi.constants;

public enum EmailLeadsEnum {
	ACC_ID("acc_ID", "acc_id", 1),
	EMPTY1("lst_ID", "__", 2),
	VIN("VIN", "vin", 3),
	LEAD_ID("LeadID", "lead_id", 4),
	LEAD_DATE("Captured", "lead_date", 5),
	EMAIL("FromMail", "email", 6),
	FIRST_NAME("FirstName", "first_name", 7),
	LAST_NAME("LastName", "last_name", 8),
	PHONE("Phone", "phone", 9),
	ZIP("Zip", "zip", 10),
	COMMENT("Comment", "comment", 11);
	
	private String fileColumn;
	private String tableColumn;
	private int columnOrder;

	  
	EmailLeadsEnum(String fileColumn, String tableColumn, int columnOrder){
		this.fileColumn = fileColumn;
		this.tableColumn = tableColumn;
		this.columnOrder = columnOrder;
	}


	public String getFileColumn() {
		return fileColumn;
	}


	public String getTableColumn() {
		return tableColumn;
	}


	public int getColumnOrder() {
		return columnOrder;
	}
}
