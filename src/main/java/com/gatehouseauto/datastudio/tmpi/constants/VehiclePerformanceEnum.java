package com.gatehouseauto.datastudio.tmpi.constants;

public enum VehiclePerformanceEnum {
	ACC_ID("acc_ID", "acc_id", 1),
	LST_ID("lst_id", "lst_id", 2),
	CAMPAIGN_ID("_Master_ID", "campaign_id", 3),
	FLIGHT_ID("_Flight_ID", "flight_id", 4),
	FILE_DATE("Captured", "file_date", 5),
	SOURCE("Source", "source", 6),
	TYPE("Type", "type", 7),
	TOTAL("Total", "total", 8);
	
	private String fileColumn;
	private String tableColumn;
	private int columnOrder;
	  
	VehiclePerformanceEnum(String fileColumn, String tableColumn, int columnOrder){
		this.fileColumn = fileColumn;
		this.tableColumn = tableColumn;
		this.columnOrder = columnOrder;
	}


	public String getFileColumn() {
		return fileColumn;
	}


	public String getTableColumn() {
		return tableColumn;
	}


	public int getColumnOrder() {
		return columnOrder;
	}
}
