package com.gatehouseauto.datastudio.tmpi.constants;

public enum CustomerListEnum {

	ACC_ID("acc_ID", "acc_id", 1),
	DEALER_NAME("Customer", "dealer_name", 2),
	PHONE("Phone", "Phone", 3),
	FID("external_id", "fid", 4),
	EXPORTED("exported", "exported", 5);
	
	private String fileColumn;
	private String tableColumn;
	private int columnOrder;

	  
	CustomerListEnum(String fileColumn, String tableColumn, int columnOrder){
		this.fileColumn = fileColumn;
		this.tableColumn = tableColumn;
		this.columnOrder = columnOrder;
	}


	public String getFileColumn() {
		return fileColumn;
	}


	public String getTableColumn() {
		return tableColumn;
	}


	public int getColumnOrder() {
		return columnOrder;
	}

}
