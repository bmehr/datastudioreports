package com.gatehouseauto.datastudio.tmpi.constants;

public enum FlightInfoEnum {
	ACC_ID("acc_ID", "acc_id", 1),
	CAMPAIGN_ID("_Master_ID", "campaign_id", 2),
	FLIGHT_ID("_Flight_ID", "flight_id", 3),
	STATUS("flight_status", "status", 4),	
	START_DATE("flight_start_date", "start_date", 5),
	END_DATE("flight_end_date", "end_date", 6),
	VDPS("flight_vdps", "vdps", 7),
	IMPRESSIONS("flight_impressions", "impressions", 8),
	CLICKS("flight_clicks", "clicks", 9),
	PROFILE_VIEWS("flight_profile_views", "profile_views", 10),
	ACTIONS("flight_actions", "actions", 11),
	COST("price_paid", "cost", 12),
	PACKAGE("package", "package", 13),
	CAPTURED_DATE("exported", "captured_date", 14),
	CAMPAIGN_NAME("campaign_name", "campaign_name", 15),
	INVENTORY("vehicle_type", "inventory", 16),
	LANGUAGE("language", "language", 17);
	
	private String fileColumn;
	private String tableColumn;
	private int columnOrder;

	  
	FlightInfoEnum(String fileColumn, String tableColumn, int columnOrder){
		this.fileColumn = fileColumn;
		this.tableColumn = tableColumn;
		this.columnOrder = columnOrder;
	}


	public String getFileColumn() {
		return fileColumn;
	}


	public String getTableColumn() {
		return tableColumn;
	}


	public int getColumnOrder() {
		return columnOrder;
	}
}
