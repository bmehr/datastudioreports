package com.gatehouseauto.datastudio.tmpi.constants;

public enum TMPiFilesEnum {
	CUSTOMER_LIST("CustomerList", "tmpi_customer_list"),
	FLIGHT_INFO("Flight-Info", "tmpi_flight_info"),
	DISPLAY_CLICKS("DisplayClicks", "tmpi_display_clicks"),
	EMAIL_LEADS("EmailLeads", "tmpi_email_leads"),
	PHONE_LEADS("PhoneLeads", "tmpi_phone_leads"),
	VEHICLE_LIST("VehicleList", "tmpi_vehicle_list"),
	VEHICLE_SUMMARY("VehicleSummary", "tmpi_vehicle_summary"),
	VEHICLE_PERFORMANCE("VehiclePerformance", "tmpi_vehicle_performance");
	
	private String fileName;
	private String tableName;

	  
	TMPiFilesEnum(String fileName, String tableName){
		this.fileName = fileName;
		this.tableName = tableName;
	}

	public String getFileName() {
		return fileName;
	}

	public String getTableName() {
		return tableName;
	}

}
