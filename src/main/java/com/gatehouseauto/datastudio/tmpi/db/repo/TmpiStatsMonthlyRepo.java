package com.gatehouseauto.datastudio.tmpi.db.repo;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.gatehouseauto.datastudio.db.domain.TmpiStatsMonthly;

@Repository(value="tmpiStatsMonthlyRepo")
public interface TmpiStatsMonthlyRepo extends CrudRepository<TmpiStatsMonthly, Integer>{
	
	@Modifying
	@Transactional
	@Query("DELETE FROM TmpiStatsMonthly tsm WHERE month(tsm.fileDate) = (:month)  AND year(tsm.fileDate) = (:year) ")
	int deleteForMonthOf(@Param("month") int month, @Param("year") int year);
}
