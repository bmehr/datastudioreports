package com.gatehouseauto.datastudio.tmpi.db.repo;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.gatehouseauto.datastudio.db.domain.TmpiPhoneLeads;

@Repository(value="tmpiPhoneLeadsRepo")
public interface TmpiPhoneLeadsRepo extends CrudRepository<TmpiPhoneLeads, Integer>{
	
	@Query("SELECT di FROM TmpiPhoneLeads di WHERE di.accId = (:accId) AND di.leadId = (:leadId) AND di.leadDate = (:leadDate)  ")
	public TmpiPhoneLeads findByAccIdLeadIdLeadDate(@Param("accId") String accId, @Param("leadId") int leadId, @Param("leadDate") String leadDate);

}
