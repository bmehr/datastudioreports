package com.gatehouseauto.datastudio.tmpi.db.repo;

import java.util.Date;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.gatehouseauto.datastudio.db.domain.TmpiStatsDaily;

@Repository(value="tmpiStatsDailyRepo")
public interface TmpiStatsDailyRepo extends CrudRepository<TmpiStatsDaily, Integer>{

	@Query("SELECT count(*) FROM TmpiStatsDaily tsd WHERE tsd.fileDate = (:fileDate) ")
	public Integer getDailyStatsCount(@Param("fileDate") Date fileDate);
	
	@Modifying
	@Transactional
	@Query("DELETE FROM TmpiStatsDaily tsd WHERE tsd.fileDate = (:fileDate)")
	int deleteOnDay(@Param("fileDate") Date fileDate);

}
