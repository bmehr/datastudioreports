package com.gatehouseauto.datastudio.tmpi.db.repo;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.gatehouseauto.datastudio.db.domain.DealerInformation;

@Repository(value="dealerInformationRepo")
public interface DealerInformationRepo extends CrudRepository<DealerInformation, Integer>{

}
