package com.gatehouseauto.datastudio.tmpi.db.repo;

import java.time.LocalDate;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.gatehouseauto.datastudio.constant.ApplicationConstants;
import com.gatehouseauto.datastudio.util.CommonUtils;
import com.gatehouseauto.datastudio.util.LoggingUtils;

@Repository(value="tmpiCustomRepository")
public class TmpiCustomRepository {
	
	@PersistenceContext
    private EntityManager entityManager; 
	
	@Autowired
	protected TmpiStatsDailyRepo tmpiStatsDailyRepo;
	
	@Autowired
	protected TmpiStatsMonthlyRepo tmpiStatsMonthlyRepo;
	
	
	@Transactional
	public boolean updateProductsPostProcess() {
		
		Query tmpiCustomerListQInitialize = entityManager.createNativeQuery(
				" UPDATE tmpi_customer_list SET social = 0 , smartads = 0 , display = 0 , leads = 0 " );
		tmpiCustomerListQInitialize.executeUpdate();
				
		Query tmpiCustomerListQUpdateSocial = entityManager.createNativeQuery(new StringBuilder(
				" UPDATE tmpi_customer_list SET social = 1 WHERE acc_id IN ( " )
				.append( " SELECT DISTINCT acc_id FROM tmpi_flight_info ")
				.append(" WHERE status = 'Active' ")
				.append(" AND start_date <= '").append(ApplicationConstants.TMPI_ARGUMENT_FILE_DATE).append("'")
				.append(" AND end_date >= '").append(ApplicationConstants.TMPI_ARGUMENT_FILE_DATE).append("'")
				.append(" AND package = 'Intelligent Facebook' ) " ).toString() );
		tmpiCustomerListQUpdateSocial.executeUpdate();
		
		Query tmpiCustomerListQUpdateSAds = entityManager.createNativeQuery(new StringBuilder(
				" UPDATE tmpi_customer_list SET smartads = 1  WHERE acc_id IN ( " )
				.append( " SELECT DISTINCT acc_id FROM tmpi_flight_info ")
				.append(" WHERE status = 'Active' ")
				.append(" AND start_date <= '").append(ApplicationConstants.TMPI_ARGUMENT_FILE_DATE).append("'")
				.append(" AND end_date >= '").append(ApplicationConstants.TMPI_ARGUMENT_FILE_DATE).append("'")
				.append(" AND package = 'Intelligent Display' ) " ).toString() );
		tmpiCustomerListQUpdateSAds.executeUpdate();		
		
		Query tmpiCustomerListQUpdateDisplay = entityManager.createNativeQuery(new StringBuilder(
				" UPDATE tmpi_customer_list SET display = 1  WHERE acc_id IN ( " )
				.append( " SELECT DISTINCT acc_id FROM tmpi_flight_info ")
				.append(" WHERE status = 'Active' ")
				.append(" AND start_date <= '").append(ApplicationConstants.TMPI_ARGUMENT_FILE_DATE).append("'")
				.append(" AND end_date >= '").append(ApplicationConstants.TMPI_ARGUMENT_FILE_DATE).append("'")
				.append(" AND package = 'Auto Channel' ) " ).toString() );
		tmpiCustomerListQUpdateDisplay.executeUpdate();
		
		Query tmpiCustomerListQUpdateLeads = entityManager.createNativeQuery(new StringBuilder(
				" UPDATE tmpi_customer_list SET leads = 1  WHERE acc_id IN ( " )
				.append( " SELECT DISTINCT acc_id FROM tmpi_flight_info ")
				.append(" WHERE status = 'Active' ")
				.append(" AND start_date <= '").append(ApplicationConstants.TMPI_ARGUMENT_FILE_DATE).append("'")
				.append(" AND end_date >= '").append(ApplicationConstants.TMPI_ARGUMENT_FILE_DATE).append("'")
				.append(" AND package = 'Overdrive' AND campaign_name LIKE '%Guaranteed%' ) " ).toString() );
		tmpiCustomerListQUpdateLeads.executeUpdate();
		
		Query tmpiCustomerListQUpdateGuaranteed = entityManager.createNativeQuery(new StringBuilder(
				" UPDATE dealer_information SET is_guaranteed_leads = 1  WHERE fid IN ( " )
				.append( " SELECT DISTINCT b.fid FROM tmpi_flight_info a JOIN tmpi_customer_list b ON (a.acc_id = b.acc_id) ")
				.append(" WHERE a.status = 'Active' ")
				.append(" AND a.start_date <= '").append(ApplicationConstants.TMPI_ARGUMENT_FILE_DATE).append("'")
				.append(" AND a.end_date >= '").append(ApplicationConstants.TMPI_ARGUMENT_FILE_DATE).append("'")
				.append(" AND a.package = 'Overdrive' AND a.campaign_name LIKE '%Guaranteed%' ) " ).toString() );
		tmpiCustomerListQUpdateGuaranteed.executeUpdate();
		
		return false;
	}
	
	@Transactional
	public void updateDailyStats(){
		deleteDailyTmpiStats();
		insertDailyTmpiStats();
		
		LocalDate localDate = CommonUtils.toLocalDate(ApplicationConstants.TMPI_ARGUMENT_FILE_DATE);
		
		LoggingUtils.logInfo(new StringBuilder("DailyTmpiStats for the day :")
				.append(localDate.toString()).toString());
	}
	
	@Transactional
	public void updateMonthlyStats(){
		LocalDate localDate = CommonUtils.toLocalDate(ApplicationConstants.TMPI_ARGUMENT_FILE_DATE);
		
		tmpiStatsMonthlyRepo.deleteForMonthOf(localDate.getMonthValue(), localDate.getYear());
		
		
		insertMonthlyTmpiStats(localDate.getMonthValue(), localDate.getYear());
		
		LoggingUtils.logInfo(new StringBuilder("MonthlyTmpiStats for month :")
				.append(localDate.toString()).toString());
	}
	
	private void deleteDailyTmpiStats(){
		int dailyStatsCount = tmpiStatsDailyRepo.getDailyStatsCount(CommonUtils.toDate(ApplicationConstants.TMPI_ARGUMENT_FILE_DATE, "yyyyMMdd"));
		if(dailyStatsCount > 1){
			dailyStatsCount = tmpiStatsDailyRepo.deleteOnDay(CommonUtils.toDate(ApplicationConstants.TMPI_ARGUMENT_FILE_DATE, "yyyyMMdd"));
		}
	}

	private void insertDailyTmpiStats(){
		/*
		 * dealer_inventory will be union with the dealer_inventory_sold table to get accurate serps and vdps
		 */		
		Query insertDailyStatQuery = entityManager.createNativeQuery(
				new StringBuilder(" INSERT INTO dealer_stats.tmpi_stats_daily (fid, file_date, tmp_used_cars_search_results_page_views, tmp_new_cars_search_results_page_views, ")
				.append(" tmp_used_cars_vehicle_detail_page_views, tmp_new_cars_vehicle_detail_page_views) ")
				.append(" SELECT fid, file_date, SUM(userps) userps, SUM(newserps) newserps, SUM(uvdps) uvdps, SUM(newvdps) newvdps from  ")
				.append(" ( ")
				.append(" SELECT tcl.fid, tvp.file_date,  ")
				.append(" IFNULL( SUM(case WHEN tvp.type = 'SERP' AND vehicle_type = 'Used' then tvp.total end),0) userps,  ")
				.append(" IFNULL( SUM(case WHEN tvp.type = 'SERP' AND vehicle_type = 'New' then tvp.total end),0) newserps,  ")
				.append(" IFNULL( SUM(case WHEN tvp.type = 'VDP' AND vehicle_type = 'Used' then tvp.total end),0) uvdps,  ")
				.append(" IFNULL( SUM(case WHEN tvp.type = 'VDP' AND vehicle_type = 'New' then tvp.total end),0) newvdps ")
				.append(" FROM tmpi_vehicle_performance tvp   ")
				.append(" JOIN tmpi_customer_list tcl ON ( tvp.acc_id = tcl.acc_id )   ")
				.append(" JOIN tmpi_vehicle_list tvl ON (tvp.lst_id = tvl.lst_id)   ")
				.append(" JOIN dealer_inventory di ON ( di.vin = tvl.vin )  ")
				.append(" WHERE tvp.file_date = '")
				.append(ApplicationConstants.TMPI_ARGUMENT_FILE_DATE) //.append("2018-04-02")
				.append("' AND di.id = (SELECT MAX(id) FROM dealer_inventory WHERE vin = di.vin)  GROUP BY tcl.fid ")
				.append("UNION")
				.append(" SELECT tcl.fid, tvp.file_date, ")
				.append(" IFNULL( SUM(case WHEN tvp.type = 'SERP' AND vehicle_type = 'Used' then tvp.total end),0) userps, ")
				.append(" IFNULL( SUM(case WHEN tvp.type = 'SERP' AND vehicle_type = 'New' then tvp.total end),0) newserps, ")
				.append(" IFNULL( SUM(case WHEN tvp.type = 'VDP' AND vehicle_type = 'Used' then tvp.total end),0) uvdps, ")
				.append(" IFNULL( SUM(case WHEN tvp.type = 'VDP' AND vehicle_type = 'New' then tvp.total end),0) newvdps ")
				.append(" FROM tmpi_vehicle_performance tvp  ")
				.append(" JOIN tmpi_customer_list tcl ON ( tvp.acc_id = tcl.acc_id ) ")
				.append(" JOIN tmpi_vehicle_list tvl ON (tvp.lst_id = tvl.lst_id)  ")
				.append(" JOIN dealer_inventory_sold dis ON ( dis.vin = tvl.vin ) ")
				.append(" WHERE tvp.file_date = '")
				.append(ApplicationConstants.TMPI_ARGUMENT_FILE_DATE)
				.append("' AND dis.id = (SELECT MAX(id) FROM dealer_inventory_sold WHERE vin = dis.vin)  ")
				.append(" GROUP BY tcl.fid ")
				.append(" ) temp ")
				.append(" GROUP BY fid ")
				.toString() );
		
		insertDailyStatQuery.executeUpdate();
	}
	
	private void insertMonthlyTmpiStats(int month, int year){		
		Query monthlyStatQuery = entityManager.createNativeQuery(
				new StringBuilder(" INSERT INTO tmpi_stats_monthly (fid, file_date, tmp_used_cars_search_results_page_views, tmp_new_cars_search_results_page_views, ")
				.append(" tmp_used_cars_vehicle_detail_page_views, tmp_new_cars_vehicle_detail_page_views) ")
				.append(" SELECT fid, file_date, ")
				.append(" SUM(tmp_used_cars_search_results_page_views) userps, ")
				.append(" SUM(tmp_new_cars_search_results_page_views) newserps, ")
				.append(" SUM(tmp_used_cars_vehicle_detail_page_views) uvdps, ")
				.append(" SUM(tmp_new_cars_vehicle_detail_page_views) newvdps ")
				.append(" FROM tmpi_stats_daily ")
				.append(" WHERE month(file_date) = ").append(month).append(" and year(file_date) = ").append(year)
				.append(" GROUP BY fid ")
				.toString() );
		
		monthlyStatQuery.executeUpdate();
	}

}
