package com.gatehouseauto.datastudio.tmpi.db.repo;

import java.util.Date;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.gatehouseauto.datastudio.db.domain.TmpiVehiclePerformance;

@Repository(value="tmpiVehiclePerformanceRepo")
public interface TmpiVehiclePerformanceRepo extends CrudRepository<TmpiVehiclePerformance, Integer>{

	@Query("SELECT di FROM TmpiVehiclePerformance di WHERE di.lstId = (:lstId) AND di.accId = (:accId) AND di.flightId = (:flightId) AND di.type = (:type) AND di.fileDate = (:fileDate) ")
	public TmpiVehiclePerformance findByLstAccIdFlishtIdTypeFileDate(@Param("lstId") String lstId, @Param("accId") String accId, @Param("flightId") String flightId, @Param("type") String type, @Param("fileDate") Date fileDate);

	
}
