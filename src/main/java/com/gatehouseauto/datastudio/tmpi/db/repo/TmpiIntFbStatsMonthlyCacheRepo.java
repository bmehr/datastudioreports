package com.gatehouseauto.datastudio.tmpi.db.repo;

import java.util.Date;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.gatehouseauto.datastudio.db.domain.TmpiIntFbStatsMonthlyCache;

@Repository(value="tmpiIntFbStatsMonthlyCacheRepo")
public interface TmpiIntFbStatsMonthlyCacheRepo extends CrudRepository<TmpiIntFbStatsMonthlyCache, Integer>{

	@Query("SELECT di FROM TmpiIntFbStatsMonthlyCache di WHERE di.accId = (:accId) AND di.campaignId = (:campaignId) AND di.statsMonth = (:statsMonth)")
	public TmpiIntFbStatsMonthlyCache findByAccIdCampaignIdStatsMonth(@Param("accId") String accId, @Param("campaignId") String campaignId, @Param("statsMonth") Date statsMonth);
}
