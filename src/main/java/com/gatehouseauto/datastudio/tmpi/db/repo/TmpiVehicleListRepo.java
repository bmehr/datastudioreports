package com.gatehouseauto.datastudio.tmpi.db.repo;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.gatehouseauto.datastudio.db.domain.TmpiVehicleList;

@Repository(value="tmpiVehicleListRepo")
public interface TmpiVehicleListRepo extends CrudRepository<TmpiVehicleList, Integer>{

	@Query("SELECT di FROM TmpiVehicleList di WHERE di.lstId = (:lstId) AND di.vin = (:vin)  ")
	public TmpiVehicleList findByLstIdVin(@Param("lstId") String lstId, @Param("vin") String vin);
	
}
