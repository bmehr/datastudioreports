package com.gatehouseauto.datastudio.tmpi.db;

import java.io.FileReader;
import java.io.Reader;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVRecord;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.gatehouseauto.datastudio.db.domain.TmpiCustomerList;
import com.gatehouseauto.datastudio.db.domain.TmpiDisplayClicks;
import com.gatehouseauto.datastudio.db.domain.TmpiEmailLeads;
import com.gatehouseauto.datastudio.db.domain.TmpiFlightInfo;
import com.gatehouseauto.datastudio.db.domain.TmpiPhoneLeads;
import com.gatehouseauto.datastudio.db.domain.TmpiVehicleList;
import com.gatehouseauto.datastudio.db.domain.TmpiVehiclePerformance;
import com.gatehouseauto.datastudio.db.domain.TmpiVehicleSummary;
import com.gatehouseauto.datastudio.tmpi.constants.CustomerListEnum;
import com.gatehouseauto.datastudio.tmpi.constants.DisplayClicksEnum;
import com.gatehouseauto.datastudio.tmpi.constants.EmailLeadsEnum;
import com.gatehouseauto.datastudio.tmpi.constants.FlightInfoEnum;
import com.gatehouseauto.datastudio.tmpi.constants.PhoneLeadsEnum;
import com.gatehouseauto.datastudio.tmpi.constants.VehicleListEnum;
import com.gatehouseauto.datastudio.tmpi.constants.VehiclePerformanceEnum;
import com.gatehouseauto.datastudio.tmpi.constants.VehicleSummaryEnum;
import com.gatehouseauto.datastudio.tmpi.db.repo.TmpiCustomerListRepo;
import com.gatehouseauto.datastudio.tmpi.db.repo.TmpiDisplayClicksRepo;
import com.gatehouseauto.datastudio.tmpi.db.repo.TmpiEmailLeadsRepo;
import com.gatehouseauto.datastudio.tmpi.db.repo.TmpiFlightInfoRepo;
import com.gatehouseauto.datastudio.tmpi.db.repo.TmpiPhoneLeadsRepo;
import com.gatehouseauto.datastudio.tmpi.db.repo.TmpiVehicleListRepo;
import com.gatehouseauto.datastudio.tmpi.db.repo.TmpiVehiclePerformanceRepo;
import com.gatehouseauto.datastudio.tmpi.db.repo.TmpiVehicleSummaryRepo;

@Component(value="tMPiDAO")
public class TMPiDAO {
	
	@Autowired
	protected TmpiCustomerListRepo tmpiCustomerListRepo;
	
	@Autowired
	protected TmpiFlightInfoRepo tmpiFlightInfoRepo;
	
	@Autowired
	protected TmpiDisplayClicksRepo tmpiDisplayClicksRepo;
	
	@Autowired
	protected TmpiEmailLeadsRepo tmpiEmailLeadsRepo;
	
	@Autowired
	protected TmpiPhoneLeadsRepo tmpiPhoneLeadsRepo;
	
	@Autowired
	protected TmpiVehicleListRepo tmpiVehicleListRepo;
	
	@Autowired
	protected TmpiVehicleSummaryRepo tmpiVehicleSummaryRepo;
	
	@Autowired
	protected TmpiVehiclePerformanceRepo tmpiVehiclePerformanceRepo;
	
	@PersistenceContext
    private EntityManager entityManager; 
	
	public void customerList(String zipFolderFullPath) throws Exception{
		
		Reader fileReader = new FileReader(zipFolderFullPath);
		Iterable<CSVRecord> records = CSVFormat.TDF.parse(fileReader);
		
		boolean isHeader = true;
		for (CSVRecord record : records) {
			if(isHeader){
				isHeader = false;
				continue;
			}
			TmpiCustomerList tmpiCustomerList = new TmpiCustomerList();
			for(int i=0; record.size() > i; i++){
				if(i==1){
					tmpiCustomerList.setAccId(record.get(CustomerListEnum.ACC_ID.getColumnOrder()));
				}else if(i==2){
					tmpiCustomerList.setDealerName(record.get(CustomerListEnum.DEALER_NAME.getColumnOrder()));
				}else if(i==3){
					tmpiCustomerList.setPhone(record.get(CustomerListEnum.PHONE.getColumnOrder()));
				}else if(i==4){
					tmpiCustomerList.setFid(record.get(CustomerListEnum.FID.getColumnOrder()));
				}else if(i==5){
					tmpiCustomerList.setExported(record.get(CustomerListEnum.EXPORTED.getColumnOrder()));
				}
			}
			
			TmpiCustomerList tmpiCustomerListDB = tmpiCustomerListRepo.findByAccId(tmpiCustomerList.getAccId());
			if(tmpiCustomerListDB != null){
				tmpiCustomerList.setId(tmpiCustomerListDB.getId());
			}
			
			tmpiCustomerList.setUpdatedOn(new Date());
			tmpiCustomerListRepo.save(tmpiCustomerList);
		}
	}
	
	public void flightInfo(String zipFolderFullPath) throws Exception{
		
		Reader fileReader = new FileReader(zipFolderFullPath);
		Iterable<CSVRecord> records = CSVFormat.TDF.parse(fileReader);
		
		boolean isHeader = true;
		for (CSVRecord record : records) {
			if(isHeader){
				isHeader = false;
				continue;
			}
			TmpiFlightInfo tmpiFlightInfo = new TmpiFlightInfo();
			for(int i=0; record.size() > i; i++){
				if(i==1){
					tmpiFlightInfo.setAccId(record.get(FlightInfoEnum.ACC_ID.getColumnOrder()));
				}else if(i==2){
					tmpiFlightInfo.setCampaignId(record.get(FlightInfoEnum.CAMPAIGN_ID.getColumnOrder()));
					
				}else if(i==3){
					tmpiFlightInfo.setFlightId(record.get(FlightInfoEnum.FLIGHT_ID.getColumnOrder()));
					
				}else if(i==4){
					tmpiFlightInfo.setStatus(record.get(FlightInfoEnum.STATUS.getColumnOrder()));
					
				}else if(i==5){
					tmpiFlightInfo.setStartDate(stringToDate(record.get(FlightInfoEnum.START_DATE.getColumnOrder())));
					
				}else if(i==6){
					tmpiFlightInfo.setEndDate(stringToDate(record.get(FlightInfoEnum.END_DATE.getColumnOrder())));
					
				}else if(i==7){
					tmpiFlightInfo.setVdps(record.get(FlightInfoEnum.VDPS.getColumnOrder()));
					
				}else if(i==8){
					tmpiFlightInfo.setImpressions(record.get(FlightInfoEnum.IMPRESSIONS.getColumnOrder()));
					
				}else if(i==9){
					tmpiFlightInfo.setClicks(record.get(FlightInfoEnum.CLICKS.getColumnOrder()));
					
				}else if(i==10){
					tmpiFlightInfo.setProfileViews(record.get(FlightInfoEnum.PROFILE_VIEWS.getColumnOrder()));
					
				}else if(i==11){
					tmpiFlightInfo.setActions(record.get(FlightInfoEnum.ACTIONS.getColumnOrder()));
					
				}else if(i==12){
					tmpiFlightInfo.setCost(record.get(FlightInfoEnum.COST.getColumnOrder()));
					
				}else if(i==13){
					tmpiFlightInfo.setPackage1(record.get(FlightInfoEnum.PACKAGE.getColumnOrder()));
					
				}else if(i==14){
					tmpiFlightInfo.setCapturedDate(stringToDate(record.get(FlightInfoEnum.CAPTURED_DATE.getColumnOrder())));
					
				}else if(i==15){
					tmpiFlightInfo.setCampaignName(record.get(FlightInfoEnum.CAMPAIGN_NAME.getColumnOrder()));
					
				}else if(i==16){
					tmpiFlightInfo.setInventory(record.get(FlightInfoEnum.INVENTORY.getColumnOrder()));
					
				}else if(i==17){
					tmpiFlightInfo.setLanguage(record.get(FlightInfoEnum.LANGUAGE.getColumnOrder()));
					
				}
			}
			
			TmpiFlightInfo tmpiFlightInfoDB = tmpiFlightInfoRepo.findByAccIdFlightIdStartDate(tmpiFlightInfo.getAccId(), tmpiFlightInfo.getFlightId(), tmpiFlightInfo.getStartDate());
			if(tmpiFlightInfoDB != null){				
				tmpiFlightInfo.setId(tmpiFlightInfoDB.getId());
				
			}
			
			tmpiFlightInfo.setUpdatedOn(new Date());
			tmpiFlightInfoRepo.save(tmpiFlightInfo);
		}
	}
	
	public void displayClicks(String zipFolderFullPath) throws Exception{
		
		Reader fileReader = new FileReader(zipFolderFullPath);
		Iterable<CSVRecord> records = CSVFormat.TDF.parse(fileReader);
		
		boolean isHeader = true;
		for (CSVRecord record : records) {
			if(isHeader){
				isHeader = false;
				continue;
			}
			TmpiDisplayClicks tmpiDisplayClicks = new TmpiDisplayClicks();
			for(int i=0; record.size() > i; i++){
				if(i==1){
					tmpiDisplayClicks.setAccId(stringToInt(record.get(DisplayClicksEnum.ACC_ID.getColumnOrder())));
				}else if(i==2){
					tmpiDisplayClicks.setCampaignId(record.get(DisplayClicksEnum.CAMPAIGN_ID.getColumnOrder()));
				}else if(i==3){
					tmpiDisplayClicks.setCapturedDate(dateToString(stringToDate(record.get(DisplayClicksEnum.CAPTURED_DATE.getColumnOrder())),"yyyy-MM-dd"));
				}else if(i==4){
					tmpiDisplayClicks.setClicks(stringToInt(record.get(DisplayClicksEnum.CLICKS.getColumnOrder())));
				}else if(i==5){
					tmpiDisplayClicks.setImpressions(stringToInt(record.get(DisplayClicksEnum.IMPRESSIONS.getColumnOrder())));
				}
			}
			
			TmpiDisplayClicks tmpiDisplayClicksDB = tmpiDisplayClicksRepo.findByAccIdCampaignIdCapturedDate(tmpiDisplayClicks.getAccId(), tmpiDisplayClicks.getCampaignId(), tmpiDisplayClicks.getCapturedDate());
			if(tmpiDisplayClicksDB != null){
				tmpiDisplayClicks.setId(tmpiDisplayClicksDB.getId());
			}
			
			tmpiDisplayClicks.setUpdatedOn(new Date());
			tmpiDisplayClicksRepo.save(tmpiDisplayClicks);
		}
	}
	
	public void emailLeads(String zipFolderFullPath) throws Exception{
		
		Reader fileReader = new FileReader(zipFolderFullPath);
		Iterable<CSVRecord> records = CSVFormat.TDF.parse(fileReader);
		
		boolean isHeader = true;
		for (CSVRecord record : records) {
			if(isHeader){
				isHeader = false;
				continue;
			}
			TmpiEmailLeads tmpiEmailLeads = new TmpiEmailLeads();
			for(int i=0; record.size() > i; i++){
				if(i==1){
					tmpiEmailLeads.setAccId(record.get(EmailLeadsEnum.ACC_ID.getColumnOrder()));
				}else if(i==2){

				}else if(i==3){
					tmpiEmailLeads.setVin(record.get(EmailLeadsEnum.VIN.getColumnOrder()));
				}else if(i==4){
					tmpiEmailLeads.setLeadId(record.get(EmailLeadsEnum.LEAD_ID.getColumnOrder()));
				}else if(i==5){
					tmpiEmailLeads.setLeadDate(stringToDate(record.get(EmailLeadsEnum.LEAD_DATE.getColumnOrder())));
				}else if(i==6){
					tmpiEmailLeads.setEmail(record.get(EmailLeadsEnum.EMAIL.getColumnOrder()));
				}else if(i==7){
					tmpiEmailLeads.setFirstName(record.get(EmailLeadsEnum.FIRST_NAME.getColumnOrder()));
				}else if(i==8){
					tmpiEmailLeads.setLastName(record.get(EmailLeadsEnum.LAST_NAME.getColumnOrder()));
				}else if(i==9){
					tmpiEmailLeads.setPhone(record.get(EmailLeadsEnum.PHONE.getColumnOrder()));
				}else if(i==10){
					tmpiEmailLeads.setZip(record.get(EmailLeadsEnum.ZIP.getColumnOrder()));
				}else if(i==11){
					tmpiEmailLeads.setComment(record.get(EmailLeadsEnum.COMMENT.getColumnOrder()));
					
					if(tmpiEmailLeads.getComment() != null && tmpiEmailLeads.getComment().length()>399){
						tmpiEmailLeads.setComment(tmpiEmailLeads.getComment().substring(0, 399));
					}
				}
			}
			
			TmpiEmailLeads tmpiEmailLeadsDB = tmpiEmailLeadsRepo.findByAccIdLeadIdLeadDate(tmpiEmailLeads.getAccId(), tmpiEmailLeads.getLeadId(), tmpiEmailLeads.getLeadDate());
			if(tmpiEmailLeadsDB != null){
				tmpiEmailLeads.setId(tmpiEmailLeadsDB.getId());
			}
			
			tmpiEmailLeads.setUpdatedOn(new Date());
			tmpiEmailLeadsRepo.save(tmpiEmailLeads);
		}
	}
	
	public void phoneLeads(String zipFolderFullPath) throws Exception{
		
		Reader fileReader = new FileReader(zipFolderFullPath);
		Iterable<CSVRecord> records = CSVFormat.TDF.parse(fileReader);
		
		boolean isHeader = true;
		for (CSVRecord record : records) {
			if(isHeader){
				isHeader = false;
				continue;
			}
			TmpiPhoneLeads tmpiPhoneLeads = new TmpiPhoneLeads();
			for(int i=0; record.size() > i; i++){
				if(i==1){
					tmpiPhoneLeads.setAccId(record.get(PhoneLeadsEnum.ACC_ID.getColumnOrder()));
				}else if(i==2){
					tmpiPhoneLeads.setLeadId(stringToInt(record.get(PhoneLeadsEnum.LEAD_ID.getColumnOrder())));
				}else if(i==3){
					tmpiPhoneLeads.setLeadDate(record.get(PhoneLeadsEnum.LEAD_DATE.getColumnOrder()));
				}else if(i==4){
					tmpiPhoneLeads.setFirstName(record.get(PhoneLeadsEnum.FIRST_NAME.getColumnOrder()));
				}else if(i==5){
					tmpiPhoneLeads.setLastName(record.get(PhoneLeadsEnum.LAST_NAME.getColumnOrder()));
				}else if(i==6){
					tmpiPhoneLeads.setPhone(record.get(PhoneLeadsEnum.PHONE.getColumnOrder()));
				}else if(i==7){
					tmpiPhoneLeads.setZip(record.get(PhoneLeadsEnum.ZIP.getColumnOrder()));
				}
			}
			
			TmpiPhoneLeads tmpiPhoneLeadsDB = tmpiPhoneLeadsRepo.findByAccIdLeadIdLeadDate(tmpiPhoneLeads.getAccId(), tmpiPhoneLeads.getLeadId(), tmpiPhoneLeads.getLeadDate());
			if(tmpiPhoneLeadsDB != null){
				tmpiPhoneLeads.setId(tmpiPhoneLeadsDB.getId());
			}
			
			tmpiPhoneLeads.setUpdatedOn(new Date());
			tmpiPhoneLeadsRepo.save(tmpiPhoneLeads);
		}
	}
	
	public void vehicleList(String zipFolderFullPath) throws Exception{
		
		Reader fileReader = new FileReader(zipFolderFullPath);
		Iterable<CSVRecord> records = CSVFormat.TDF.parse(fileReader);
		
		boolean isHeader = true;
		for (CSVRecord record : records) {
			if(isHeader){
				isHeader = false;
				continue;
			}
			TmpiVehicleList tmpiVehicleList = new TmpiVehicleList();
			for(int i=0; record.size() > i; i++){
				if(i==1){
					tmpiVehicleList.setCapturedDate(stringToDate(record.get(VehicleListEnum.CAPTURED_DATE.getColumnOrder())));
				}else if(i==2){
					tmpiVehicleList.setAccId(record.get(VehicleListEnum.ACC_ID.getColumnOrder()));
				}else if(i==3){
					tmpiVehicleList.setLstId(record.get(VehicleListEnum.LST_ID.getColumnOrder()));
				}else if(i==4){
					tmpiVehicleList.setPrice(record.get(VehicleListEnum.PRICE.getColumnOrder()));
				}else if(i==5){
					tmpiVehicleList.setPriceRange(record.get(VehicleListEnum.PRICE_RANGE.getColumnOrder()));
				}else if(i==6){
					
				}else if(i==7){
					tmpiVehicleList.setVin(record.get(VehicleListEnum.VIN.getColumnOrder()));
				}else if(i==8){
					tmpiVehicleList.setYear(record.get(VehicleListEnum.YEAR.getColumnOrder()));
				}else if(i==9){
					tmpiVehicleList.setMake(record.get(VehicleListEnum.MAKE.getColumnOrder()));
				}else if(i==10){
					tmpiVehicleList.setModel(record.get(VehicleListEnum.MODEL.getColumnOrder()));
				}else if(i==11){
					tmpiVehicleList.setTrim(record.get(VehicleListEnum.TRIM.getColumnOrder()));
				}else if(i==12){
					tmpiVehicleList.setCreated(stringToDate(record.get(VehicleListEnum.CREATED.getColumnOrder())));
				}else if(i==13){
					tmpiVehicleList.setLastUpload(stringToDate(record.get(VehicleListEnum.LAST_UPLOAD.getColumnOrder())));
				}else if(i==14){
					tmpiVehicleList.setStatus(record.get(VehicleListEnum.STATUS.getColumnOrder()));
				}
			}
			
			TmpiVehicleList tmpiVehicleListDB = tmpiVehicleListRepo.findByLstIdVin(tmpiVehicleList.getLstId(), tmpiVehicleList.getVin());
			if(tmpiVehicleListDB != null){
				tmpiVehicleList.setId(tmpiVehicleListDB.getId());
			}
			
			tmpiVehicleList.setUpdatedOn(new Date());
			tmpiVehicleListRepo.save(tmpiVehicleList);
		}
	}
	
	public void vehicleSummary(String zipFolderFullPath) throws Exception{
		
		Reader fileReader = new FileReader(zipFolderFullPath);
		Iterable<CSVRecord> records = CSVFormat.TDF.parse(fileReader);
		
		boolean isHeader = true;
		for (CSVRecord record : records) {
			if(isHeader){
				isHeader = false;
				continue;
			}
			TmpiVehicleSummary tmpiVehicleSummary = new TmpiVehicleSummary();
			for(int i=0; record.size() > i; i++){
				if(i==1){
					tmpiVehicleSummary.setAccId(record.get(VehicleSummaryEnum.ACC_ID.getColumnOrder()));
				}else if(i==2){
					tmpiVehicleSummary.setPacId(record.get(VehicleSummaryEnum.PAC_ID.getColumnOrder()));
				}else if(i==3){
					tmpiVehicleSummary.setPacName(record.get(VehicleSummaryEnum.PAC_NAME.getColumnOrder()));
				}else if(i==4){
					tmpiVehicleSummary.setPacShortName(record.get(VehicleSummaryEnum.PAC_SHORT_NAME.getColumnOrder()));
				}else if(i==5){
					tmpiVehicleSummary.setPacSort(record.get(VehicleSummaryEnum.PAC_SORT.getColumnOrder()));
				}else if(i==6){
					tmpiVehicleSummary.setTotal(record.get(VehicleSummaryEnum.TOTAL.getColumnOrder()));
				}else if(i==7){
					tmpiVehicleSummary.setCount(record.get(VehicleSummaryEnum.COUNT.getColumnOrder()));
				}else if(i==8){
					tmpiVehicleSummary.setExported(stringToDate(record.get(VehicleSummaryEnum.EXPORTED.getColumnOrder())));
				}
			}
			
			TmpiVehicleSummary tmpiVehicleSummaryDB = tmpiVehicleSummaryRepo.findByAccIdPacIdExported(tmpiVehicleSummary.getAccId(), tmpiVehicleSummary.getPacId(), tmpiVehicleSummary.getExported());
			if(tmpiVehicleSummaryDB != null){
				tmpiVehicleSummary.setId(tmpiVehicleSummaryDB.getId());
			}
			
			tmpiVehicleSummary.setUpdatedOn(new Date());
			tmpiVehicleSummaryRepo.save(tmpiVehicleSummary);
		}
	}
	
	public void vehiclePerformance(String zipFolderFullPath) throws Exception{
		
		Reader fileReader = new FileReader(zipFolderFullPath);
		Iterable<CSVRecord> records = CSVFormat.TDF.parse(fileReader);
		
		boolean isHeader = true;
		for (CSVRecord record : records) {
			if(isHeader){
				isHeader = false;
				continue;
			}
			TmpiVehiclePerformance tmpiVehiclePerformance = new TmpiVehiclePerformance();
			for(int i=0; record.size() > i; i++){
				if(i==1){
					tmpiVehiclePerformance.setAccId(record.get(VehiclePerformanceEnum.ACC_ID.getColumnOrder()));
				}else if(i==2){
					tmpiVehiclePerformance.setLstId(record.get(VehiclePerformanceEnum.LST_ID.getColumnOrder()));
				}else if(i==3){
					tmpiVehiclePerformance.setCampaignId(record.get(VehiclePerformanceEnum.CAMPAIGN_ID.getColumnOrder()));
				}else if(i==4){
					tmpiVehiclePerformance.setFlightId(record.get(VehiclePerformanceEnum.FLIGHT_ID.getColumnOrder()));
				}else if(i==5){
					tmpiVehiclePerformance.setFileDate(stringToDate(record.get(VehiclePerformanceEnum.FILE_DATE.getColumnOrder())));
				}else if(i==6){
					tmpiVehiclePerformance.setSource(record.get(VehiclePerformanceEnum.SOURCE.getColumnOrder()));
				}else if(i==7){
					tmpiVehiclePerformance.setType(record.get(VehiclePerformanceEnum.TYPE.getColumnOrder()));
				}else if(i==8){
					tmpiVehiclePerformance.setTotal(record.get(VehiclePerformanceEnum.TOTAL.getColumnOrder()));
				}
			}
			
			TmpiVehiclePerformance tmpiVehiclePerformanceDB = tmpiVehiclePerformanceRepo.findByLstAccIdFlishtIdTypeFileDate(tmpiVehiclePerformance.getLstId(), tmpiVehiclePerformance.getAccId(), 
					tmpiVehiclePerformance.getFlightId(), tmpiVehiclePerformance.getType(), tmpiVehiclePerformance.getFileDate());
			if(tmpiVehiclePerformanceDB != null){
				tmpiVehiclePerformance.setId(tmpiVehiclePerformanceDB.getId());
			}
			
			tmpiVehiclePerformance.setUpdatedOn(new Date());
			tmpiVehiclePerformanceRepo.save(tmpiVehiclePerformance);
		}
	}
	
	public void commitAllTrx(){
		if(entityManager.isOpen()){
			entityManager.close();
		}
	}
	
	private Date stringToDate(String strDate){
		if(strDate == null || "".equals(strDate)){
			return null;
		}
		try{
			return new SimpleDateFormat("yyyyMMdd", Locale.ENGLISH).parse(strDate);
		}catch(Exception e){
		}
		return null;
	}
	
	public String dateToString(Date dateParam, String dateFormat){
		String result = "";
		try{
			result =  new SimpleDateFormat(dateFormat).format(dateParam);
		}catch(Exception e){}
		return result;
	}
	
	private int stringToInt(String intStr){
		if(intStr == null || "".equals(intStr)){
			return 0;
		}
		try{
			return Integer.parseInt(intStr);
		}catch(Exception e){
			return 0;
		}
	}
	
	
}
