package com.gatehouseauto.datastudio.tmpi.db.repo;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.gatehouseauto.datastudio.db.domain.TmpiProcessedFilesLog;

@Repository(value="tmpiProcessedFilesLogRepo")
public interface TmpiProcessedFilesLogRepo extends CrudRepository<TmpiProcessedFilesLog, Integer>{

}
