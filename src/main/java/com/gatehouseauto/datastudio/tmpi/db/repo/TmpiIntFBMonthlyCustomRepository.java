package com.gatehouseauto.datastudio.tmpi.db.repo;

import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.gatehouseauto.datastudio.db.domain.TmpiFlightInfo;
import com.gatehouseauto.datastudio.db.domain.TmpiIntFbStatsMonthlyCache;
import com.gatehouseauto.datastudio.db.domain.TmpiIntFbStatsTotalCache;
import com.gatehouseauto.datastudio.sns.SNSHandler;
import com.gatehouseauto.datastudio.util.CommonUtils;
import com.gatehouseauto.datastudio.util.LoggingUtils;

@Repository(value="tmpiIntFBMonthlyCustomRepository")
public class TmpiIntFBMonthlyCustomRepository {

	@PersistenceContext
    private EntityManager entityManager; 
		
	@Autowired
	protected TmpiIntFbStatsMonthlyCacheRepo tmpiIntFbStatsMonthlyCacheRepo;
	
	@Autowired
	protected TmpiIntFbStatsTotalCacheRepo tmpiIntFbStatsTotalCacheRepo;
	
	@Transactional
	public void truncateTmpiFbStatsCache(){
		Query truncateCache = entityManager.createNativeQuery(
		"truncate tmpi_int_fb_stats_total_cache;" );
		truncateCache.executeUpdate();
		
		truncateCache = entityManager.createNativeQuery(
				"truncate tmpi_int_fb_stats_monthly_cache;" );
				truncateCache.executeUpdate();
	}
	
	@Transactional
	public void truncateTmpiFbStats(){
		Query truncateCache = entityManager.createNativeQuery(
		"truncate tmpi_int_fb_stats_total;" );
		truncateCache.executeUpdate();
		
		truncateCache = entityManager.createNativeQuery(
				"truncate tmpi_int_fb_stats_monthly;" );
				truncateCache.executeUpdate();
	}
	
	@Transactional
	public void updateIntStatsFromCache(){
		Query insertFromCache = entityManager.createNativeQuery(
		" insert into tmpi_int_fb_stats_monthly (fid, acc_id, campaign_id, stats_month, status, started_on, last_renew_on, "
		+ " last_renew_end_on, renew_count, language, inventory, cost, manual_target_monthly, vdps_target, impressions_target, "
		+ " clicks_target, vdps_monthly, impressions_monthly, serps_monthly, updated_on) "
		+ " select fid, acc_id, campaign_id, stats_month, status, started_on, last_renew_on, last_renew_end_on, renew_count, "
		+ " language, inventory, cost, manual_target_monthly, vdps_target, impressions_target, clicks_target, vdps_monthly,  "
		+ " impressions_monthly, serps_monthly, current_timestamp() from tmpi_int_fb_stats_monthly_cache" );
		insertFromCache.executeUpdate();
		
		insertFromCache = entityManager.createNativeQuery(
				" insert into tmpi_int_fb_stats_total (fid, acc_id, campaign_id, status, started_on, last_renew_on, last_renew_end_on, "
				+ " renew_count, language, inventory, cost,  manual_target_monthly, vdps_target, impressions_target, clicks_target, "
				+ " vdps_total, impressions_total, serps_total, updated_on) "
				+ " select fid, acc_id, campaign_id, status, started_on, last_renew_on, last_renew_end_on, renew_count, language, "
				+ " inventory, cost, manual_target_monthly, vdps_target, impressions_target, clicks_target, vdps_total, impressions_total, "
				+ " serps_total, updated_on from dealer_stats.tmpi_int_fb_stats_total_cache" );
		insertFromCache.executeUpdate();
	}
	
	
	
	public boolean updateIntFBMonthlyStatsCache(String campaignId) throws Exception{
		List<TmpiFlightInfo> tmpiFlightInfos = getFlightInfos(campaignId);
		if(tmpiFlightInfos == null || tmpiFlightInfos.isEmpty()){
			return false;
		}
		
		TmpiIntFbStatsTotalCache tmpiIntFbStatsTotalCache = new TmpiIntFbStatsTotalCache();
		
		TmpiFlightInfo tmpiFlightInfoLastRenewSelected = null;
		TmpiFlightInfo tmpiFlightInfoCampaignStartedSelected = null;
		int greaterflightIndex = 0;
		for(TmpiFlightInfo tmpiFlightInfo : tmpiFlightInfos){
			if(tmpiFlightInfos.size() == 1){
				tmpiFlightInfoLastRenewSelected = tmpiFlightInfo;
				tmpiFlightInfoCampaignStartedSelected = tmpiFlightInfo;
				break;				
			}
			
			//find greater date
			if(tmpiFlightInfoLastRenewSelected == null){
				tmpiFlightInfoLastRenewSelected = tmpiFlightInfo;
				tmpiFlightInfoCampaignStartedSelected = tmpiFlightInfo;
				greaterflightIndex = greaterflightIndex + 1;
				continue;
			}
			
			if(tmpiFlightInfo.getStartDate().after(tmpiFlightInfoLastRenewSelected.getStartDate()) ){
				greaterflightIndex = greaterflightIndex + 1;
				tmpiFlightInfoLastRenewSelected = tmpiFlightInfo;
			}
			
			//finding first instance when actually the campaign started
			if(tmpiFlightInfoCampaignStartedSelected.getStartDate().after(tmpiFlightInfo.getStartDate())  ){
				tmpiFlightInfoCampaignStartedSelected = tmpiFlightInfo; 
			}
		}
		
		if(tmpiFlightInfoLastRenewSelected == null){
			return false;
		}
		
		setMonthlyFlighInfo(tmpiIntFbStatsTotalCache, tmpiFlightInfoLastRenewSelected, (tmpiFlightInfos.size()-1));
		if(tmpiFlightInfoCampaignStartedSelected == null){
			return false;
		}
		tmpiIntFbStatsTotalCache.setStartedOn(tmpiFlightInfoCampaignStartedSelected.getStartDate());
		
		int totalVDPs = 0;
		int toalSerps = 0;
		int totalImpressions = 0;	
		
		LocalDate localDateStart = Instant.ofEpochMilli(tmpiFlightInfoLastRenewSelected.getStartDate().getTime()).atZone(ZoneId.systemDefault()).toLocalDate();
		localDateStart = localDateStart.withDayOfMonth(1);

		LocalDate localDateEnd = Instant.ofEpochMilli(tmpiFlightInfoLastRenewSelected.getEndDate().getTime()).atZone(ZoneId.systemDefault()).toLocalDate();
		localDateEnd = localDateEnd.withDayOfMonth(1);
		
		String fid = getFid(tmpiIntFbStatsTotalCache.getAccId());
		if(fid == null){
			fid = "0";
		}
		
		for (LocalDate date = localDateStart; date.isBefore(localDateEnd) || date.isEqual(localDateEnd); date = date.plusMonths(1))
		{	
			TmpiIntFbStatsMonthlyCache tmpiIntFbStatsMonthlyCache = new TmpiIntFbStatsMonthlyCache();
			
			putTotalToMonthly(tmpiIntFbStatsTotalCache, tmpiIntFbStatsMonthlyCache);

			int getMonthlyVDPsRetry = 1;
			int monthlyVDPs = getMonthlyVDPs(tmpiFlightInfoLastRenewSelected.getFlightId(), date.getMonthValue(), date.getYear(), getMonthlyVDPsRetry, false);
			totalVDPs = totalVDPs + monthlyVDPs;
			int[] resultIts = getMonthlyDisplayClicks(tmpiFlightInfoLastRenewSelected.getCampaignId(), date.getMonthValue(), date.getYear());
			toalSerps = toalSerps + resultIts[0];
			totalImpressions = totalImpressions + resultIts[1];			
			
			tmpiIntFbStatsMonthlyCache.setVdpsMonthly(monthlyVDPs);
			tmpiIntFbStatsMonthlyCache.setSerpsMonthly(resultIts[0]);
			tmpiIntFbStatsMonthlyCache.setImpressionsMonthly(resultIts[1]);
			
			tmpiIntFbStatsMonthlyCache.setStatsMonth(CommonUtils.toDate(date.getYear()+"-"+date.getMonthValue()+"-01", "yyy-MM-dd"));
			tmpiIntFbStatsMonthlyCache.setFid(fid);
			tmpiIntFbStatsMonthlyCache.setUpdatedOn(new Date());
			
			tmpiIntFbStatsMonthlyCacheSave(tmpiIntFbStatsMonthlyCache);
		}
		
		tmpiIntFbStatsTotalCache.setVdpsTotal(totalVDPs);
		tmpiIntFbStatsTotalCache.setSerpsTotal(toalSerps);
		tmpiIntFbStatsTotalCache.setImpressionsTotal(totalImpressions);
		tmpiIntFbStatsTotalCache.setFid(fid);
		tmpiIntFbStatsTotalCache.setUpdatedOn(new Date());
		
		tmpiIntFbStatsTotalCacheSave(tmpiIntFbStatsTotalCache);
		
		return true;
	}
	
	@Transactional
	protected void tmpiIntFbStatsMonthlyCacheSave(TmpiIntFbStatsMonthlyCache tmpiIntFbStatsMonthlyCache){
		TmpiIntFbStatsMonthlyCache tmpiIntFbStatsMonthlyCacheDB = tmpiIntFbStatsMonthlyCacheRepo.findByAccIdCampaignIdStatsMonth(tmpiIntFbStatsMonthlyCache.getAccId(), 
				tmpiIntFbStatsMonthlyCache.getCampaignId(), tmpiIntFbStatsMonthlyCache.getStatsMonth());
		if(tmpiIntFbStatsMonthlyCacheDB == null){
			tmpiIntFbStatsMonthlyCacheRepo.save(tmpiIntFbStatsMonthlyCache);
		}else{
			tmpiIntFbStatsMonthlyCache.setId(tmpiIntFbStatsMonthlyCacheDB.getId());
			tmpiIntFbStatsMonthlyCacheRepo.save(tmpiIntFbStatsMonthlyCache);
		}
	}
	
	@Transactional
	protected void tmpiIntFbStatsTotalCacheSave(TmpiIntFbStatsTotalCache tmpiIntFbStatsTotalCache){
		TmpiIntFbStatsTotalCache tmpiIntFbStatsTotalCacheDB = tmpiIntFbStatsTotalCacheRepo.findByAccIdCampaignId(tmpiIntFbStatsTotalCache.getAccId(), tmpiIntFbStatsTotalCache.getCampaignId());
		if(tmpiIntFbStatsTotalCacheDB == null){
			tmpiIntFbStatsTotalCacheRepo.save(tmpiIntFbStatsTotalCache);
		}else{
			tmpiIntFbStatsTotalCache.setId(tmpiIntFbStatsTotalCacheDB.getId());
			tmpiIntFbStatsTotalCacheRepo.save(tmpiIntFbStatsTotalCache);			
		}
	}
	
	
	public void putTotalToMonthly(TmpiIntFbStatsTotalCache tmpiIntFbStatsTotalCache, TmpiIntFbStatsMonthlyCache tmpiIntFbStatsMonthlyCache){
		tmpiIntFbStatsMonthlyCache.setAccId(tmpiIntFbStatsTotalCache.getAccId());
		tmpiIntFbStatsMonthlyCache.setCampaignId(tmpiIntFbStatsTotalCache.getCampaignId());
		tmpiIntFbStatsMonthlyCache.setStatus(tmpiIntFbStatsTotalCache.getStatus());
		tmpiIntFbStatsMonthlyCache.setStartedOn(tmpiIntFbStatsTotalCache.getStartedOn());
		tmpiIntFbStatsMonthlyCache.setLastRenewOn(tmpiIntFbStatsTotalCache.getLastRenewOn());
		tmpiIntFbStatsMonthlyCache.setLastRenewEndOn(tmpiIntFbStatsTotalCache.getLastRenewEndOn());
		tmpiIntFbStatsMonthlyCache.setRenewCount(tmpiIntFbStatsTotalCache.getRenewCount());
		tmpiIntFbStatsMonthlyCache.setLanguage(tmpiIntFbStatsTotalCache.getLanguage());
		tmpiIntFbStatsMonthlyCache.setInventory(tmpiIntFbStatsTotalCache.getInventory());
		tmpiIntFbStatsMonthlyCache.setVdpsTarget(tmpiIntFbStatsTotalCache.getVdpsTarget());
		tmpiIntFbStatsMonthlyCache.setImpressionsTarget(tmpiIntFbStatsTotalCache.getImpressionsTarget());
		tmpiIntFbStatsMonthlyCache.setClicksTarget(tmpiIntFbStatsTotalCache.getClicksTarget());
		tmpiIntFbStatsMonthlyCache.setManualTargetMonthly(tmpiIntFbStatsTotalCache.getManualTargetMonthly());
		tmpiIntFbStatsMonthlyCache.setCost(tmpiIntFbStatsTotalCache.getCost());		
	}
	
	private void setMonthlyFlighInfo(TmpiIntFbStatsTotalCache tmpiIntFbStatsTotalCache, TmpiFlightInfo tmpiFlightInfo, int renewCount){
		tmpiIntFbStatsTotalCache.setAccId(tmpiFlightInfo.getAccId());
		tmpiIntFbStatsTotalCache.setCampaignId(tmpiFlightInfo.getCampaignId());
		tmpiIntFbStatsTotalCache.setStatus(tmpiFlightInfo.getStatus());
		tmpiIntFbStatsTotalCache.setLastRenewOn(tmpiFlightInfo.getStartDate());
		tmpiIntFbStatsTotalCache.setLastRenewEndOn(tmpiFlightInfo.getEndDate());
		tmpiIntFbStatsTotalCache.setRenewCount(renewCount);
		tmpiIntFbStatsTotalCache.setLanguage(tmpiFlightInfo.getLanguage()==null||"null".equals(tmpiFlightInfo.getLanguage())?"":tmpiFlightInfo.getLanguage());
		tmpiIntFbStatsTotalCache.setInventory(tmpiFlightInfo.getInventory()==null||"null".equals(tmpiFlightInfo.getInventory())?"":tmpiFlightInfo.getInventory());
		tmpiIntFbStatsTotalCache.setVdpsTarget(CommonUtils.parseIntString(tmpiFlightInfo.getVdps()));
		tmpiIntFbStatsTotalCache.setImpressionsTarget(CommonUtils.parseIntString(tmpiFlightInfo.getImpressions()));
		tmpiIntFbStatsTotalCache.setClicksTarget(CommonUtils.parseIntString(tmpiFlightInfo.getClicks()));
		tmpiIntFbStatsTotalCache.setManualTargetMonthly(CommonUtils.parseIntString(tmpiFlightInfo.getManualTarget()));
		tmpiIntFbStatsTotalCache.setCost(CommonUtils.parseFloatString(tmpiFlightInfo.getCost()));
	}
	
	
	public List<String> getCampaignIds(){
		
		Query monthlyStatQuery = entityManager.createNativeQuery(
				new StringBuilder(" SELECT DISTINCT fi.campaign_id  ")
				.append(" FROM  tmpi_flight_info fi ")
				.append(" WHERE fi.package = 'Intelligent Facebook' ")
				.append(" ORDER BY fi.acc_id , fi.campaign_id   ")
				.toString() );
		
		List<String> campaignIds = new ArrayList<>();
		
		@SuppressWarnings("unchecked")
		List<Object> results =  monthlyStatQuery.getResultList();
		if(results != null && results.size() > 0){
			for(Object result : results){
				campaignIds.add(""+result);
			}
		}
		
		return campaignIds;
	}
		
	private List<TmpiFlightInfo> getFlightInfos(String campaignId){
		
		Query monthlyStatQuery = entityManager.createNativeQuery(
				new StringBuilder(" SELECT acc_id, campaign_id, flight_id, status, start_date, end_date,  ")
				.append(" language, inventory, vdps, impressions, clicks, manual_target, cost ")
				.append(" FROM  tmpi_flight_info ")
				.append(" WHERE campaign_id = '").append(campaignId).append("'")				
				.toString() );
		
		List<TmpiFlightInfo> tmpiFlightInfos = new ArrayList<>();
		
		@SuppressWarnings("unchecked")
		List<Object> results =  monthlyStatQuery.getResultList();
		if(results != null && results.size() > 0){
			for(Object result : results){
				TmpiFlightInfo tmpiFlightInfo = new TmpiFlightInfo();
				
				Object[] objs = (Object[])result;
			
				tmpiFlightInfo.setAccId(""+objs[0]);				
				tmpiFlightInfo.setCampaignId(""+objs[1]);
				tmpiFlightInfo.setFlightId(""+objs[2]); 
				tmpiFlightInfo.setStatus(""+objs[3]); 				
				tmpiFlightInfo.setStartDate(CommonUtils.parseDateObject(objs[4])); 
				tmpiFlightInfo.setEndDate(CommonUtils.parseDateObject(objs[5])); 
				tmpiFlightInfo.setLanguage(""+objs[6]);
				tmpiFlightInfo.setInventory(""+objs[7]);
				tmpiFlightInfo.setVdps(""+objs[8]);
				tmpiFlightInfo.setImpressions(""+objs[9]);
				tmpiFlightInfo.setClicks(""+objs[10]);
				tmpiFlightInfo.setManualTarget(""+objs[11]);
				tmpiFlightInfo.setCost(""+objs[12]);
				
				tmpiFlightInfos.add(tmpiFlightInfo);
			}
		}
		
		return tmpiFlightInfos;
	}
	
	private int getMonthlyVDPs(String flightId, int month, int year, int getMonthlyVDPsRetry, boolean isException) throws Exception{
		float resultFloat = 0.0f;	
		try{
			Query monthlyStatQuery = entityManager.createNativeQuery(
					new StringBuilder(" SELECT	")
					.append(" SUM(case WHEN vp.type = 'VDP' then vp.total end) monthly_vdps ")
					.append(" FROM tmpi_vehicle_performance vp ")
					.append(" WHERE flight_id = '").append(flightId).append("'")	
					.append(" AND month(vp.file_date) = ").append(month)
					.append(" AND year(vp.file_date) =  ").append(year)
					.append(" GROUP BY flight_id  ")
					.toString() );
			
			@SuppressWarnings("unchecked")
			List<Object> result =  monthlyStatQuery.getResultList();
			
			if(result != null && !result.isEmpty()){
				resultFloat = CommonUtils.parseFloatString(""+result.get(0));
			}
		}catch(Exception e){
			e.printStackTrace();
			
			String sString = LoggingUtils.getExceptionString(e);
			
			SNSHandler.sendSimpleMessage(sString, "@DataStudioReports! TMPI-Stats-FailedGetVDPs");
			isException = true;
		}
	
		if(isException && resultFloat == 0 && getMonthlyVDPsRetry < 4){
			Thread.sleep(60000 * getMonthlyVDPsRetry); //60000 = 1   one minute wait
			getMonthlyVDPsRetry++;
			resultFloat = getMonthlyVDPs(flightId, month, year, getMonthlyVDPsRetry, isException);
		}
		return (int)resultFloat;
	}
	
	private int[] getMonthlyDisplayClicks(String campaignId, int month, int year){
		int[] resultsInts = new int[2];
		Query monthlyStatQuery = entityManager.createNativeQuery(
				new StringBuilder(" SELECT SUM(clicks) as serps,	")
				.append(" SUM(impressions) as impressions  ")
				.append(" FROM tmpi_display_clicks ")
				.append(" WHERE campaign_id =  '").append(campaignId).append("'")	
				.append(" AND month(captured_date) =  ").append(month)
				.append(" AND year(captured_date) = ").append(year)
				.append(" GROUP BY  campaign_id  ")
				.toString() );
		
		@SuppressWarnings("unchecked")
		List<Object> results =  monthlyStatQuery.getResultList();
		if(results != null && results.size() > 0){
			for(Object result : results){
				Object[] objs = (Object[])result;
				resultsInts[0] = CommonUtils.parseIntString(""+objs[0]);
				resultsInts[1] = CommonUtils.parseIntString(""+objs[1]);
			}
		}
		
		return resultsInts;
	}
	
	
	private String getFid(String accId){
		Query monthlyStatQuery = entityManager.createNativeQuery(
				new StringBuilder(" SELECT distinct fid FROM tmpi_customer_list where acc_id = '").append(accId).append("' group by acc_id")
				.toString() );
		
		@SuppressWarnings("rawtypes")
		List result =  monthlyStatQuery.getResultList();
		if(result == null || result.isEmpty()){
			return "0";
		}
		return result.get(0)+"";
	}
	

	
}
