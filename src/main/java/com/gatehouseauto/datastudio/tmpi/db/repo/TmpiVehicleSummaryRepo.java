package com.gatehouseauto.datastudio.tmpi.db.repo;

import java.util.Date;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.gatehouseauto.datastudio.db.domain.TmpiVehicleSummary;

@Repository(value="tmpiVehicleSummaryRepo")
public interface TmpiVehicleSummaryRepo extends CrudRepository<TmpiVehicleSummary, Integer>{

	@Query("SELECT di FROM TmpiVehicleSummary di WHERE di.accId = (:accId) AND di.pacId = (:pacId) AND di.exported = (:exported)  ")
	public TmpiVehicleSummary findByAccIdPacIdExported(@Param("accId") String accId, @Param("pacId") String pacId, @Param("exported") Date exported);

	
}
