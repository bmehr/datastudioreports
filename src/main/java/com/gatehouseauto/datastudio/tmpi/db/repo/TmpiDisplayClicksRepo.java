package com.gatehouseauto.datastudio.tmpi.db.repo;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.gatehouseauto.datastudio.db.domain.TmpiDisplayClicks;

@Repository(value="tmpiDisplayClicksRepo")
public interface TmpiDisplayClicksRepo extends CrudRepository<TmpiDisplayClicks, Integer>{

	@Query("SELECT di FROM TmpiDisplayClicks di WHERE di.accId = (:accId) AND di.campaignId = (:campaignId) AND di.capturedDate = (:capturedDate)  ")
	public TmpiDisplayClicks findByAccIdCampaignIdCapturedDate(@Param("accId") int accId, @Param("campaignId") String campaignId, @Param("capturedDate") String capturedDate);

}
