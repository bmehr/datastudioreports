package com.gatehouseauto.datastudio.tmpi.db.repo;

import java.util.Date;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.gatehouseauto.datastudio.db.domain.TmpiEmailLeads;

@Repository(value="tmpiEmailLeadsRepo")
public interface TmpiEmailLeadsRepo extends CrudRepository<TmpiEmailLeads, Integer>{

	@Query("SELECT di FROM TmpiEmailLeads di WHERE di.accId = (:accId) AND di.leadId = (:leadId) AND di.leadDate = (:leadDate)  ")
	public TmpiEmailLeads findByAccIdLeadIdLeadDate(@Param("accId") String accId, @Param("leadId") String leadId, @Param("leadDate") Date leadDate);

}
