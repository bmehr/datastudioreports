package com.gatehouseauto.datastudio.tmpi.db.repo;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.gatehouseauto.datastudio.db.domain.TmpiIntFbStatsTotalCache;

@Repository(value="tmpiIntFbStatsTotalCacheRepo")
public interface TmpiIntFbStatsTotalCacheRepo extends CrudRepository<TmpiIntFbStatsTotalCache, Integer>{

	@Query("SELECT di FROM TmpiIntFbStatsTotalCache di WHERE di.accId = (:accId) AND di.campaignId = (:campaignId)")
	public TmpiIntFbStatsTotalCache findByAccIdCampaignId(@Param("accId") String accId, @Param("campaignId") String campaignId);
}
