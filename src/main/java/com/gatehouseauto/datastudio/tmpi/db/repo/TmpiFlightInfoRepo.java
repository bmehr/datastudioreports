package com.gatehouseauto.datastudio.tmpi.db.repo;

import java.util.Date;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.gatehouseauto.datastudio.db.domain.TmpiFlightInfo;

@Repository(value="tmpiFlightInfoRepo")
public interface TmpiFlightInfoRepo extends CrudRepository<TmpiFlightInfo, Integer>{
	
	@Query("SELECT di FROM TmpiFlightInfo di WHERE di.accId = (:accId) AND di.flightId = (:flightId) AND di.startDate = (:startDate)  ")
	public TmpiFlightInfo findByAccIdFlightIdStartDate(@Param("accId") String accId, @Param("flightId") String flightId, @Param("startDate") Date startDate);

	
}
