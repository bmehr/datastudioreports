package com.gatehouseauto.datastudio.tmpi.db.repo;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.gatehouseauto.datastudio.db.domain.TmpiCustomerList;

@Repository(value="tmpiCustomerListRepo")
public interface TmpiCustomerListRepo extends CrudRepository<TmpiCustomerList, Integer>{
	
	@Query("SELECT di FROM TmpiCustomerList di WHERE di.accId = (:accId) ")
	public TmpiCustomerList findByAccId(@Param("accId") String accId);
	
}
