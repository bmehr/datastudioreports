package com.gatehouseauto.datastudio.lesa;

import org.springframework.stereotype.Component;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.gatehouseauto.datastudio.lesa.bean.CustomLesaBean;
import com.gatehouseauto.datastudio.lesa.bean.LesaBean;
import com.gatehouseauto.datastudio.lesa.bean.youtube.LesaYoutubeStats;

import org.springframework.web.client.HttpServerErrorException;
import org.springframework.web.client.RestTemplate;

import java.util.Date;

import org.springframework.http.ResponseEntity;



@Component(value="lesaHttpClient")
public class LesaHttpClient {
	
	private static ObjectMapper objectMapper = new ObjectMapper();
	
	public CustomLesaBean getDealershipReportByMonth(String userName, String password, int month, int year)throws Exception{
		CustomLesaBean customLesaBean = new CustomLesaBean();
		try{			
			RestTemplate restTemplate = new RestTemplate();
			StringBuilder lesaUrlSB = new 
					StringBuilder("http://reports.lesautomotive.com/Gateway/StatsReport.aspx?user=")
					.append(userName)
					.append("&password=").append(password)
					.append("&month=").append(month)
					.append("&year=").append(year);
			
			
			System.out.println(new Date() + " : "+lesaUrlSB.toString());
			
			ResponseEntity<LesaBean> response  = restTemplate.getForEntity(lesaUrlSB.toString(), LesaBean.class);
			LesaBean lesaBean = response.getBody();
			
			customLesaBean.setLesaBean(lesaBean);
			if(lesaBean != null && lesaBean.getYTMinutesData() != null){
				customLesaBean.setLesaYoutubeStats(objectMapper.readValue(lesaBean.getYTMinutesData(), LesaYoutubeStats.class));
				System.out.println("lesaBean.getYTMinutesData() >>>>> :"+lesaBean.getYTMinutesData() );
			}
		}catch(HttpServerErrorException e){
			e.printStackTrace();
			return null;
		}
		
		return customLesaBean;
	}
	

}
