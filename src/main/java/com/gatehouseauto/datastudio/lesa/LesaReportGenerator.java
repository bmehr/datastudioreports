package com.gatehouseauto.datastudio.lesa;

import java.time.LocalDate;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.gatehouseauto.datastudio.db.domain.LesaCredentials;
import com.gatehouseauto.datastudio.db.domain.LesaTop10Videos;
import com.gatehouseauto.datastudio.db.domain.LesaTotalViews;
import com.gatehouseauto.datastudio.db.domain.LesaViews;
import com.gatehouseauto.datastudio.lesa.bean.CustomLesaBean;
import com.gatehouseauto.datastudio.lesa.bean.LesaBean;
import com.gatehouseauto.datastudio.lesa.bean.StockNumView;
import com.gatehouseauto.datastudio.lesa.bean.VehicleView;
import com.gatehouseauto.datastudio.lesa.bean.VideoCompletion;
import com.gatehouseauto.datastudio.lesa.bean.ViewsByCity;
import com.gatehouseauto.datastudio.lesa.bean.ViewsByMake;
import com.gatehouseauto.datastudio.lesa.bean.ViewsByModel;
import com.gatehouseauto.datastudio.lesa.bean.ViewsByPlatform;
import com.gatehouseauto.datastudio.lesa.db.repo.LesaCredentialsRepo;
import com.gatehouseauto.datastudio.lesa.db.repo.LesaTop10VideosRepo;
import com.gatehouseauto.datastudio.lesa.db.repo.LesaTotalViewsRepo;
import com.gatehouseauto.datastudio.lesa.db.repo.LesaViewsRepo;
import com.gatehouseauto.datastudio.sns.SNSHandler;
import com.gatehouseauto.datastudio.util.CommonUtils;

@Component(value="lesaReportGenerator")
public class LesaReportGenerator {
	
	@Autowired
	protected LesaHttpClient lesaHttpClient;
	
	@Autowired
	protected LesaCredentialsRepo lesaCredentialsRepo;
	
	@Autowired
	protected LesaViewsRepo lesaViewsRepo;
	
	@Autowired
	protected LesaTop10VideosRepo lesaTop10VideosRepo;
	
	@Autowired
	protected LesaTotalViewsRepo lesaTotalViewsRepo;	
	
//	@Scheduled(cron="0 0 14 * * *")
	public void run() throws Exception{
		LocalDate localDateNow = LocalDate.now();
		
		Iterable<LesaCredentials> lesaCredentialses = lesaCredentialsRepo.findAll();
		for(LesaCredentials lesaCredentials : lesaCredentialses){

			for(int i=6; i>=0; i--){
				LocalDate minusLocalDate = localDateNow.minusMonths(i);
				CustomLesaBean customLesaBean = lesaHttpClient.getDealershipReportByMonth(lesaCredentials.getUserName(), lesaCredentials.getPassword(), minusLocalDate.getMonthValue(), minusLocalDate.getYear());
				if(customLesaBean == null){
					int loopCount = 0;
					while(customLesaBean == null && loopCount < 3){
						loopCount++;
						Thread.sleep(loopCount*2000);
						customLesaBean = lesaHttpClient.getDealershipReportByMonth(lesaCredentials.getUserName(), lesaCredentials.getPassword(), minusLocalDate.getMonthValue(), minusLocalDate.getYear());
						
						System.out.println("After Recalling customBean here are the values :"+customLesaBean + " - loopCount:"+loopCount);
					}
					
				}
				
				if(customLesaBean == null){
					StringBuilder sbLog = new StringBuilder();
					sbLog
					.append("UserName:").append(lesaCredentials.getUserName())
					.append("Password:").append("EMPTY")
					.append("Month:").append(minusLocalDate.getMonthValue())					
					.append("Year:").append(minusLocalDate.getYear());
					
					SNSHandler.sendSimpleMessage(sbLog.toString(), "@DataStudioReports! Lesa-Call-Failed");
					System.exit(-1);
				}
				
				if(customLesaBean.getLesaBean() == null || customLesaBean.getLesaBean().getVideoViews() == 0){
					continue;
				}
				
				/*
				 * Save into the database
				 */
				LesaBean lesaBean = customLesaBean.getLesaBean();
				
				/*
				 * VideoCompletion
				 */
				if(lesaBean.getVideoCompletion() != null && !lesaBean.getVideoCompletion().isEmpty()){
					for(VideoCompletion videoCompletion : lesaBean.getVideoCompletion()){
						LesaViews lesaViews = new LesaViews(); 
						lesaViews.setViewType("VideoCompletion");
						lesaViews.setFid(lesaCredentials.getFid());
						lesaViews.setReportMonth(CommonUtils.toDate(minusLocalDate.getYear()+"-"+minusLocalDate.getMonthValue()+"-01", "yyy-MM-dd"));
						lesaViews.setColumnName(videoCompletion.getColumnName());	
						lesaViews.setCount(videoCompletion.getCount());
						
						lesaViewsRepo.save(lesaViews);
					}
				}
				
				/*
				 * VehicleViews
				 */
				if(lesaBean.getVehicleViews() != null && !lesaBean.getVehicleViews().isEmpty()){
					for(VehicleView vehicleView : lesaBean.getVehicleViews()){
						LesaViews lesaViews = new LesaViews(); 
						lesaViews.setViewType("VehicleViews");
						lesaViews.setFid(lesaCredentials.getFid());
						lesaViews.setReportMonth(CommonUtils.toDate(minusLocalDate.getYear()+"-"+minusLocalDate.getMonthValue()+"-01", "yyy-MM-dd"));
						lesaViews.setColumnName(vehicleView.getColumnName());	
						lesaViews.setCount(vehicleView.getCount());
						
						lesaViewsRepo.save(lesaViews);
					}
				}				
				
				/*
				 * ViewsByCity
				 */
				if(lesaBean.getViewsByCity() != null && !lesaBean.getViewsByCity().isEmpty()){
					for(ViewsByCity viewsByCity : lesaBean.getViewsByCity()){
						LesaViews lesaViews = new LesaViews(); 
						lesaViews.setViewType("ViewsByCity");
						lesaViews.setFid(lesaCredentials.getFid());
						lesaViews.setReportMonth(CommonUtils.toDate(minusLocalDate.getYear()+"-"+minusLocalDate.getMonthValue()+"-01", "yyy-MM-dd"));
						lesaViews.setColumnName(viewsByCity.getColumnName());	
						lesaViews.setCount(viewsByCity.getCount());
						
						lesaViewsRepo.save(lesaViews);
					}
				}
				
				/*
				 * ViewsByModels
				 */
				if(lesaBean.getViewsByModels() != null && !lesaBean.getViewsByModels().isEmpty()){
					for(ViewsByModel viewsByModel : lesaBean.getViewsByModels()){
						LesaViews lesaViews = new LesaViews(); 
						lesaViews.setViewType("ViewsByModels");
						lesaViews.setFid(lesaCredentials.getFid());
						lesaViews.setReportMonth(CommonUtils.toDate(minusLocalDate.getYear()+"-"+minusLocalDate.getMonthValue()+"-01", "yyy-MM-dd"));
						lesaViews.setColumnName(viewsByModel.getColumnName());	
						lesaViews.setCount(viewsByModel.getCount());
						
						lesaViewsRepo.save(lesaViews);
					}
				}
				
				/*
				 * ViewsByMake
				 */
				if(lesaBean.getViewsByMake() != null && !lesaBean.getViewsByMake().isEmpty()){
					for(ViewsByMake viewsByMake : lesaBean.getViewsByMake()){
						LesaViews lesaViews = new LesaViews(); 
						lesaViews.setViewType("ViewsByMake");
						lesaViews.setFid(lesaCredentials.getFid());
						lesaViews.setReportMonth(CommonUtils.toDate(minusLocalDate.getYear()+"-"+minusLocalDate.getMonthValue()+"-01", "yyy-MM-dd"));
						lesaViews.setColumnName(viewsByMake.getColumnName());	
						lesaViews.setCount(viewsByMake.getCount());
						
						lesaViewsRepo.save(lesaViews);
					}
				}

				/*
				 * ViewsByPlatforms
				 */
				if(lesaBean.getViewsByPlatforms() != null && !lesaBean.getViewsByPlatforms().isEmpty()){
					for(ViewsByPlatform viewsByPlatform : lesaBean.getViewsByPlatforms()){
						LesaViews lesaViews = new LesaViews(); 
						lesaViews.setViewType("ViewsByPlatforms");
						lesaViews.setFid(lesaCredentials.getFid());
						lesaViews.setReportMonth(CommonUtils.toDate(minusLocalDate.getYear()+"-"+minusLocalDate.getMonthValue()+"-01", "yyy-MM-dd"));
						lesaViews.setColumnName(viewsByPlatform.getColumnName()+"");/////////////////////////////////////////////////////////////////////////////test	
						lesaViews.setCount(viewsByPlatform.getCount());
						
						lesaViewsRepo.save(lesaViews);
					}
				}
				
				/*
				 * StockNumViews
				 */
				if(lesaBean.getStockNumViews() != null && !lesaBean.getStockNumViews().isEmpty()){
					for(StockNumView stockNumView : lesaBean.getStockNumViews()){
						LesaViews lesaViews = new LesaViews(); 
						lesaViews.setViewType("StockNumViews");
						lesaViews.setFid(lesaCredentials.getFid());
						lesaViews.setReportMonth(CommonUtils.toDate(minusLocalDate.getYear()+"-"+minusLocalDate.getMonthValue()+"-01", "yyy-MM-dd"));
						lesaViews.setColumnName(stockNumView.getColumnName());
						lesaViews.setCount(stockNumView.getCount());
						
						lesaViewsRepo.save(lesaViews);
					}
				}
				
				/*
				 * LesaTotalViews
				 */
				LesaTotalViews lesaTotalViews = new LesaTotalViews();
				lesaTotalViews.setAutotraderViews(CommonUtils.parseIntString(lesaBean.getAutotraderViews()));
				lesaTotalViews.setCarscomViews(CommonUtils.parseIntString(lesaBean.getCarscomViews()));
				lesaTotalViews.setCreatedOn(new Date());
				lesaTotalViews.setFid(lesaCredentials.getFid());
				lesaTotalViews.setReportMonth(CommonUtils.toDate(minusLocalDate.getYear()+"-"+minusLocalDate.getMonthValue()+"-01", "yyy-MM-dd"));
				lesaTotalViews.setTotalCustomers(CommonUtils.parseIntString(lesaBean.getTotalCustomers()));
				lesaTotalViews.setVehiclesWithVideo(lesaBean.getVehiclesWithVideo());
				lesaTotalViews.setVideoViews(lesaBean.getVideoViews());
				
				lesaTotalViewsRepo.save(lesaTotalViews);	
				
				/*
				 * LesaTop10Videos
				 */
				if( customLesaBean.getLesaYoutubeStats() != null && customLesaBean.getLesaYoutubeStats().getTopVideos() != null ){
					for(List<String> topVideos : customLesaBean.getLesaYoutubeStats().getTopVideos()){
						if(topVideos.size() == 3){
							LesaTop10Videos lesaTop10Videos = new LesaTop10Videos();
							lesaTop10Videos.setFid(lesaCredentials.getFid());
							lesaTop10Videos.setReportMonth(CommonUtils.toDate(minusLocalDate.getYear()+"-"+minusLocalDate.getMonthValue()+"-01", "yyy-MM-dd"));
							String top10Videos = topVideos.get(0);
							if(top10Videos != null && top10Videos.length() > 430){
								top10Videos = top10Videos.substring(0, 429);
							}
							lesaTop10Videos.setVideo(top10Videos);
							lesaTop10Videos.setWatchTimeMinutes(CommonUtils.parseIntString(topVideos.get(1)));
							lesaTop10Videos.setViews(CommonUtils.parseIntString(topVideos.get(2)));
							
							lesaTop10VideosRepo.save(lesaTop10Videos);
						}
					}
					
				}
				
			}
		}		
		
		System.out.println("......................");
	}
	

}
