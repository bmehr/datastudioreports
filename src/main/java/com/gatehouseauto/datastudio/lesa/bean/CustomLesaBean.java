package com.gatehouseauto.datastudio.lesa.bean;

import com.gatehouseauto.datastudio.lesa.bean.youtube.LesaYoutubeStats;

public class CustomLesaBean {
	private LesaBean lesaBean;
	private LesaYoutubeStats lesaYoutubeStats;
	
	public LesaBean getLesaBean() {
		return lesaBean;
	}
	public void setLesaBean(LesaBean lesaBean) {
		this.lesaBean = lesaBean;
	}
	public LesaYoutubeStats getLesaYoutubeStats() {
		return lesaYoutubeStats;
	}
	public void setLesaYoutubeStats(LesaYoutubeStats lesaYoutubeStats) {
		this.lesaYoutubeStats = lesaYoutubeStats;
	}
	
	
}
