
package com.gatehouseauto.datastudio.lesa.bean;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonIgnoreProperties(ignoreUnknown=true)
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "VehiclesWithVideo",
    "VideoViews",
    "VideoCompletion",
    "TotalCustomers",
    "AutotraderViews",
    "CarscomViews",
    "VehicleViews",
    "ViewsByModels",
    "ViewsByMake",
    "ViewsByYear",
    "ViewsByState",
    "ViewsByCity",
    "ViewsByWeekDay",
    "ViewsByMonthDay",
    "ViewsByOS",
    "ViewsByBrowsers",
    "ViewsByPlatforms",
    "TrendView",
    "TrendUnique",
    "StockNumViews",
    "YTMinutesData"
})
public class LesaBean {

    @JsonProperty("VehiclesWithVideo")
    private Integer vehiclesWithVideo;
    @JsonProperty("VideoViews")
    private Integer videoViews;
    @JsonProperty("VideoCompletion")
    private List<VideoCompletion> videoCompletion = null;
    @JsonProperty("TotalCustomers")
    private String totalCustomers;
    @JsonProperty("AutotraderViews")
    private String autotraderViews;
    @JsonProperty("CarscomViews")
    private String carscomViews;
    @JsonProperty("VehicleViews")
    private List<VehicleView> vehicleViews = null;
    @JsonProperty("ViewsByModels")
    private List<ViewsByModel> viewsByModels = null;
    @JsonProperty("ViewsByMake")
    private List<ViewsByMake> viewsByMake = null;
    @JsonProperty("ViewsByYear")
    private List<ViewsByYear> viewsByYear = null;
    @JsonProperty("ViewsByState")
    private List<ViewsByState> viewsByState = null;
    @JsonProperty("ViewsByCity")
    private List<ViewsByCity> viewsByCity = null;
    @JsonProperty("ViewsByWeekDay")
    private List<ViewsByWeekDay> viewsByWeekDay = null;
    @JsonProperty("ViewsByMonthDay")
    private List<ViewsByMonthDay> viewsByMonthDay = null;
    @JsonProperty("ViewsByOS")
    private List<ViewsByO> viewsByOS = null;
    @JsonProperty("ViewsByBrowsers")
    private List<ViewsByBrowser> viewsByBrowsers = null;
    @JsonProperty("ViewsByPlatforms")
    private List<ViewsByPlatform> viewsByPlatforms = null;
    @JsonProperty("TrendView")
    private List<TrendView> trendView = null;
    @JsonProperty("TrendUnique")
    private List<TrendUnique> trendUnique = null;
    @JsonProperty("StockNumViews")
    private List<StockNumView> stockNumViews = null;
    @JsonProperty("YTMinutesData")
    private String yTMinutesData;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("VehiclesWithVideo")
    public Integer getVehiclesWithVideo() {
        return vehiclesWithVideo;
    }

    @JsonProperty("VehiclesWithVideo")
    public void setVehiclesWithVideo(Integer vehiclesWithVideo) {
        this.vehiclesWithVideo = vehiclesWithVideo;
    }

    @JsonProperty("VideoViews")
    public Integer getVideoViews() {
        return videoViews;
    }

    @JsonProperty("VideoViews")
    public void setVideoViews(Integer videoViews) {
        this.videoViews = videoViews;
    }

    @JsonProperty("VideoCompletion")
    public List<VideoCompletion> getVideoCompletion() {
        return videoCompletion;
    }

    @JsonProperty("VideoCompletion")
    public void setVideoCompletion(List<VideoCompletion> videoCompletion) {
        this.videoCompletion = videoCompletion;
    }

    @JsonProperty("TotalCustomers")
    public String getTotalCustomers() {
        return totalCustomers;
    }

    @JsonProperty("TotalCustomers")
    public void setTotalCustomers(String totalCustomers) {
        this.totalCustomers = totalCustomers;
    }

    @JsonProperty("AutotraderViews")
    public String getAutotraderViews() {
        return autotraderViews;
    }

    @JsonProperty("AutotraderViews")
    public void setAutotraderViews(String autotraderViews) {
        this.autotraderViews = autotraderViews;
    }

    @JsonProperty("CarscomViews")
    public String getCarscomViews() {
        return carscomViews;
    }

    @JsonProperty("CarscomViews")
    public void setCarscomViews(String carscomViews) {
        this.carscomViews = carscomViews;
    }

    @JsonProperty("VehicleViews")
    public List<VehicleView> getVehicleViews() {
        return vehicleViews;
    }

    @JsonProperty("VehicleViews")
    public void setVehicleViews(List<VehicleView> vehicleViews) {
        this.vehicleViews = vehicleViews;
    }

    @JsonProperty("ViewsByModels")
    public List<ViewsByModel> getViewsByModels() {
        return viewsByModels;
    }

    @JsonProperty("ViewsByModels")
    public void setViewsByModels(List<ViewsByModel> viewsByModels) {
        this.viewsByModels = viewsByModels;
    }

    @JsonProperty("ViewsByMake")
    public List<ViewsByMake> getViewsByMake() {
        return viewsByMake;
    }

    @JsonProperty("ViewsByMake")
    public void setViewsByMake(List<ViewsByMake> viewsByMake) {
        this.viewsByMake = viewsByMake;
    }

    @JsonProperty("ViewsByYear")
    public List<ViewsByYear> getViewsByYear() {
        return viewsByYear;
    }

    @JsonProperty("ViewsByYear")
    public void setViewsByYear(List<ViewsByYear> viewsByYear) {
        this.viewsByYear = viewsByYear;
    }

    @JsonProperty("ViewsByState")
    public List<ViewsByState> getViewsByState() {
        return viewsByState;
    }

    @JsonProperty("ViewsByState")
    public void setViewsByState(List<ViewsByState> viewsByState) {
        this.viewsByState = viewsByState;
    }

    @JsonProperty("ViewsByCity")
    public List<ViewsByCity> getViewsByCity() {
        return viewsByCity;
    }

    @JsonProperty("ViewsByCity")
    public void setViewsByCity(List<ViewsByCity> viewsByCity) {
        this.viewsByCity = viewsByCity;
    }

    @JsonProperty("ViewsByWeekDay")
    public List<ViewsByWeekDay> getViewsByWeekDay() {
        return viewsByWeekDay;
    }

    @JsonProperty("ViewsByWeekDay")
    public void setViewsByWeekDay(List<ViewsByWeekDay> viewsByWeekDay) {
        this.viewsByWeekDay = viewsByWeekDay;
    }

    @JsonProperty("ViewsByMonthDay")
    public List<ViewsByMonthDay> getViewsByMonthDay() {
        return viewsByMonthDay;
    }

    @JsonProperty("ViewsByMonthDay")
    public void setViewsByMonthDay(List<ViewsByMonthDay> viewsByMonthDay) {
        this.viewsByMonthDay = viewsByMonthDay;
    }

    @JsonProperty("ViewsByOS")
    public List<ViewsByO> getViewsByOS() {
        return viewsByOS;
    }

    @JsonProperty("ViewsByOS")
    public void setViewsByOS(List<ViewsByO> viewsByOS) {
        this.viewsByOS = viewsByOS;
    }

    @JsonProperty("ViewsByBrowsers")
    public List<ViewsByBrowser> getViewsByBrowsers() {
        return viewsByBrowsers;
    }

    @JsonProperty("ViewsByBrowsers")
    public void setViewsByBrowsers(List<ViewsByBrowser> viewsByBrowsers) {
        this.viewsByBrowsers = viewsByBrowsers;
    }

    @JsonProperty("ViewsByPlatforms")
    public List<ViewsByPlatform> getViewsByPlatforms() {
        return viewsByPlatforms;
    }

    @JsonProperty("ViewsByPlatforms")
    public void setViewsByPlatforms(List<ViewsByPlatform> viewsByPlatforms) {
        this.viewsByPlatforms = viewsByPlatforms;
    }

    @JsonProperty("TrendView")
    public List<TrendView> getTrendView() {
        return trendView;
    }

    @JsonProperty("TrendView")
    public void setTrendView(List<TrendView> trendView) {
        this.trendView = trendView;
    }

    @JsonProperty("TrendUnique")
    public List<TrendUnique> getTrendUnique() {
        return trendUnique;
    }

    @JsonProperty("TrendUnique")
    public void setTrendUnique(List<TrendUnique> trendUnique) {
        this.trendUnique = trendUnique;
    }

    @JsonProperty("StockNumViews")
    public List<StockNumView> getStockNumViews() {
        return stockNumViews;
    }

    @JsonProperty("StockNumViews")
    public void setStockNumViews(List<StockNumView> stockNumViews) {
        this.stockNumViews = stockNumViews;
    }

    @JsonProperty("YTMinutesData")
    public String getYTMinutesData() {
        return yTMinutesData;
    }

    @JsonProperty("YTMinutesData")
    public void setYTMinutesData(String yTMinutesData) {
        this.yTMinutesData = yTMinutesData;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
