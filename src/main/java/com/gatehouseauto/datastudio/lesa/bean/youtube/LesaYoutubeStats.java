
package com.gatehouseauto.datastudio.lesa.bean.youtube;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonIgnoreProperties(ignoreUnknown=true)
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "views",
    "viewsTotal",
    "estimatedMinutesWatched",
    "minutesTotal",
    "topVideos"
})
public class LesaYoutubeStats {

    @JsonProperty("views")
    private List<View> views = null;
    @JsonProperty("viewsTotal")
    private Integer viewsTotal;
    @JsonProperty("estimatedMinutesWatched")
    private List<EstimatedMinutesWatched> estimatedMinutesWatched = null;
    @JsonProperty("minutesTotal")
    private Integer minutesTotal;
    @JsonProperty("topVideos")
    private List<List<String>> topVideos = null;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("views")
    public List<View> getViews() {
        return views;
    }

    @JsonProperty("views")
    public void setViews(List<View> views) {
        this.views = views;
    }

    @JsonProperty("viewsTotal")
    public Integer getViewsTotal() {
        return viewsTotal;
    }

    @JsonProperty("viewsTotal")
    public void setViewsTotal(Integer viewsTotal) {
        this.viewsTotal = viewsTotal;
    }

    @JsonProperty("estimatedMinutesWatched")
    public List<EstimatedMinutesWatched> getEstimatedMinutesWatched() {
        return estimatedMinutesWatched;
    }

    @JsonProperty("estimatedMinutesWatched")
    public void setEstimatedMinutesWatched(List<EstimatedMinutesWatched> estimatedMinutesWatched) {
        this.estimatedMinutesWatched = estimatedMinutesWatched;
    }

    @JsonProperty("minutesTotal")
    public Integer getMinutesTotal() {
        return minutesTotal;
    }

    @JsonProperty("minutesTotal")
    public void setMinutesTotal(Integer minutesTotal) {
        this.minutesTotal = minutesTotal;
    }

    @JsonProperty("topVideos")
    public List<List<String>> getTopVideos() {
        return topVideos;
    }

    @JsonProperty("topVideos")
    public void setTopVideos(List<List<String>> topVideos) {
        this.topVideos = topVideos;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
