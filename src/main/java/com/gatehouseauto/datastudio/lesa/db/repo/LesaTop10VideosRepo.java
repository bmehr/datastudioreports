package com.gatehouseauto.datastudio.lesa.db.repo;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.gatehouseauto.datastudio.db.domain.LesaTop10Videos;

@Repository(value="lesaTop10VideosRepo")
public interface LesaTop10VideosRepo  extends CrudRepository<LesaTop10Videos, Integer>{

}
