package com.gatehouseauto.datastudio.lesa.db.repo;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.gatehouseauto.datastudio.db.domain.LesaViews;

@Repository(value="lesaViewsRepo")
public interface LesaViewsRepo extends CrudRepository<LesaViews, Integer>{

}
