package com.gatehouseauto.datastudio.lesa.db.repo;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.gatehouseauto.datastudio.db.domain.LesaTotalViews;

@Repository(value="lesaTotalViewsRepo")
public interface LesaTotalViewsRepo extends CrudRepository<LesaTotalViews, Integer>{

}
