package com.gatehouseauto.datastudio.lesa.db.repo;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.gatehouseauto.datastudio.db.domain.LesaCredentials;

@Repository(value="lesaCredentialsRepo")
public interface LesaCredentialsRepo extends CrudRepository<LesaCredentials, Integer>{

}
