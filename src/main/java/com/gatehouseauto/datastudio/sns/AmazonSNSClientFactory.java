package com.gatehouseauto.datastudio.sns;

import java.net.MalformedURLException;

import javax.xml.rpc.ServiceException;

import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.regions.Region;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.sns.AmazonSNSClient;
import com.gatehouseauto.datastudio.constant.PropertyValue;

public class AmazonSNSClientFactory {
    private static volatile AmazonSNSClient amazonSNSClient = null;

    private AmazonSNSClientFactory(){}

    @SuppressWarnings("deprecation")
	public static AmazonSNSClient getAmazonSNSClient() throws ServiceException, MalformedURLException{
        if(amazonSNSClient == null && !"".equals(PropertyValue.AWS_ACCESSKEY_ID)){
            synchronized (AmazonS3Client.class) {
                // Double check
                if (amazonSNSClient == null && !"".equals(PropertyValue.AWS_ACCESSKEY_ID)) {
                    BasicAWSCredentials awsCredentials = new BasicAWSCredentials(PropertyValue.AWS_ACCESSKEY_ID, PropertyValue.AWS_SECRET_ACCESS_KEY);
                    amazonSNSClient = new AmazonSNSClient(awsCredentials);

                    amazonSNSClient.setRegion(Region.getRegion(Regions.US_EAST_1));
                }
            }
        }
        return amazonSNSClient;
    }
}
