package com.gatehouseauto.datastudio.sns;

import com.amazonaws.services.sns.AmazonSNSClient;
import com.amazonaws.services.sns.model.PublishRequest;
import com.gatehouseauto.datastudio.constant.PropertyValue;
import com.gatehouseauto.datastudio.util.LoggingUtils;


public class SNSHandler {
	private static final String TOPIC_ARN = PropertyValue.AWS_SNS_ARN;
	
	public static void sendSimpleMessage(String message, boolean exception) {
		try{
	        AmazonSNSClient amazonSNSClient = AmazonSNSClientFactory.getAmazonSNSClient();
	
	        String subjectSend = null;
	        if(exception){
	        	subjectSend = "@DataStudioReports! Exception";
	        }else{
	        	subjectSend = "@DataStudioReports! Good";
	        }		
	        
	        PublishRequest publishRequest = new PublishRequest(TOPIC_ARN, message);
	        publishRequest.setSubject(subjectSend);
	        amazonSNSClient.publish(publishRequest);
    	}catch(Exception e){
    		LoggingUtils.logInfo(LoggingUtils.getExceptionString(e));
    	}	        
    }
	
	public static void sendSimpleMessage(String message, String subjectSend) {
		try{
	        AmazonSNSClient amazonSNSClient = AmazonSNSClientFactory.getAmazonSNSClient();
	        
	        PublishRequest publishRequest = new PublishRequest(TOPIC_ARN, message);
	        publishRequest.setSubject(subjectSend);
	        amazonSNSClient.publish(publishRequest);
    	}catch(Exception e){
    		LoggingUtils.logInfo(LoggingUtils.getExceptionString(e));
    	}	        
    }
}
