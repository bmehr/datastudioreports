package com.gatehouseauto.datastudio;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import com.gatehouseauto.datastudio.constant.ApplicationConstants;
import com.gatehouseauto.datastudio.constant.PropertyValue;
import com.gatehouseauto.datastudio.sns.SNSHandler;
import com.gatehouseauto.datastudio.util.LoggingUtils;
import com.gatehouseauto.datastudio.util.PropertyLoaderUtil;


public class DataStudioReportsMain {
	
	/*
	 * sudo nohup java -jar  datastudio.jar tmpi:20180414
	 */
	public static void main(String args[]){
		try{
      	    if(PropertyValue.DB_URL == null || "".equals(PropertyValue.DB_URL.trim())){
    		   PropertyLoaderUtil.loadProperties();
    	    }
			
			@SuppressWarnings({ "resource", "unused" })
			ApplicationContext ctx = new AnnotationConfigApplicationContext(ApplicationConfig.class);
			
			if(args != null && args.length > 0){
				for(String arg:args){
					if(arg.contains("tmpi")){
						if(arg.contains(":")){
							String tmpiArg[] = arg.split(":");
							ApplicationConstants.TMPI_ARGUMENT_FILE_DATE = tmpiArg[1];
							
							LoggingUtils.logInfo(">>> ApplicationConstants.TMPI_DOWNLOADED_FILE_DATE................. : "+ ApplicationConstants.TMPI_ARGUMENT_FILE_DATE);
							System.out.println(">>>1 ApplicationConstants.TMPI_DOWNLOADED_FILE_DATE................. : "+ ApplicationConstants.TMPI_ARGUMENT_FILE_DATE);
						}
					}
				}
			}
            
            LoggingUtils.logInfo("................Application Started.................");

//            ReportRunner reportRunner = (ReportRunner)ctx.getBean("reportRunner");
//            reportRunner.run();
            
		}catch(Exception e){
			e.printStackTrace();
			String exception = LoggingUtils.getExceptionString(e);
			LoggingUtils.logInfo(exception);
			SNSHandler.sendSimpleMessage(exception, true);
		}
	}

}
