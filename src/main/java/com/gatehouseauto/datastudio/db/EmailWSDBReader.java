package com.gatehouseauto.datastudio.db;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.gatehouseauto.datastudio.db.domain.DealerInformation;

public class EmailWSDBReader {
	
	public static List<DealerInformation> getDealers() throws Exception{
		
		Class.forName("com.mysql.jdbc.Driver");  
		Connection connection = DriverManager.getConnection("jdbc:mysql://emailws-prod.cejpw7infdti.us-east-1.rds.amazonaws.com:3306/emailws","bruser","R4R1fBRRRqKu"); 
		connection.setReadOnly(true);
		
		Statement statement = connection.createStatement();  
		ResultSet resultSet = statement.executeQuery("select fid, dealer_name from dealerinformation where is_active = 1");  
		
		Date nowDate = new Date();
		
		List<DealerInformation> dealers = new ArrayList<DealerInformation>();
		while(resultSet.next()){
			DealerInformation dealerInformation = new DealerInformation();
			dealerInformation.setFid(resultSet.getString(1));
			dealerInformation.setDealerName(resultSet.getString(2));
			
			dealerInformation.setIsGuaranteedLeads(false);
			dealerInformation.setUpdatedOn(nowDate);
			
			dealers.add(dealerInformation);
		}
		connection.close(); 
		
		return dealers;
	}

}
