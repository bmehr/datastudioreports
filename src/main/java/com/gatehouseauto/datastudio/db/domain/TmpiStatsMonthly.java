/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gatehouseauto.datastudio.db.domain;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author basit
 */
@Entity
@Table(name = "tmpi_stats_monthly")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "TmpiStatsMonthly.findAll", query = "SELECT t FROM TmpiStatsMonthly t")
    , @NamedQuery(name = "TmpiStatsMonthly.findById", query = "SELECT t FROM TmpiStatsMonthly t WHERE t.id = :id")
    , @NamedQuery(name = "TmpiStatsMonthly.findByFid", query = "SELECT t FROM TmpiStatsMonthly t WHERE t.fid = :fid")
    , @NamedQuery(name = "TmpiStatsMonthly.findByFileDate", query = "SELECT t FROM TmpiStatsMonthly t WHERE t.fileDate = :fileDate")
    , @NamedQuery(name = "TmpiStatsMonthly.findByTmpUsedCarsSearchResultsPageViews", query = "SELECT t FROM TmpiStatsMonthly t WHERE t.tmpUsedCarsSearchResultsPageViews = :tmpUsedCarsSearchResultsPageViews")
    , @NamedQuery(name = "TmpiStatsMonthly.findByTmpNewCarsSearchResultsPageViews", query = "SELECT t FROM TmpiStatsMonthly t WHERE t.tmpNewCarsSearchResultsPageViews = :tmpNewCarsSearchResultsPageViews")
    , @NamedQuery(name = "TmpiStatsMonthly.findByTmpUsedCarsVehicleDetailPageViews", query = "SELECT t FROM TmpiStatsMonthly t WHERE t.tmpUsedCarsVehicleDetailPageViews = :tmpUsedCarsVehicleDetailPageViews")
    , @NamedQuery(name = "TmpiStatsMonthly.findByTmpNewCarsVehicleDetailPageViews", query = "SELECT t FROM TmpiStatsMonthly t WHERE t.tmpNewCarsVehicleDetailPageViews = :tmpNewCarsVehicleDetailPageViews")
    , @NamedQuery(name = "TmpiStatsMonthly.findByCreatedOn", query = "SELECT t FROM TmpiStatsMonthly t WHERE t.createdOn = :createdOn")})
public class TmpiStatsMonthly implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 45)
    @Column(name = "fid")
    private String fid;
    @Basic(optional = false)
    @NotNull
    @Column(name = "file_date")
    @Temporal(TemporalType.DATE)
    private Date fileDate;
    @Column(name = "tmp_used_cars_search_results_page_views")
    private Integer tmpUsedCarsSearchResultsPageViews;
    @Column(name = "tmp_new_cars_search_results_page_views")
    private Integer tmpNewCarsSearchResultsPageViews;
    @Column(name = "tmp_used_cars_vehicle_detail_page_views")
    private Integer tmpUsedCarsVehicleDetailPageViews;
    @Column(name = "tmp_new_cars_vehicle_detail_page_views")
    private Integer tmpNewCarsVehicleDetailPageViews;
    @Basic(optional = false)
    @NotNull
    @Column(name = "created_on")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdOn;

    public TmpiStatsMonthly() {
    }

    public TmpiStatsMonthly(Integer id) {
        this.id = id;
    }

    public TmpiStatsMonthly(Integer id, String fid, Date fileDate, Date createdOn) {
        this.id = id;
        this.fid = fid;
        this.fileDate = fileDate;
        this.createdOn = createdOn;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getFid() {
        return fid;
    }

    public void setFid(String fid) {
        this.fid = fid;
    }

    public Date getFileDate() {
        return fileDate;
    }

    public void setFileDate(Date fileDate) {
        this.fileDate = fileDate;
    }

    public Integer getTmpUsedCarsSearchResultsPageViews() {
        return tmpUsedCarsSearchResultsPageViews;
    }

    public void setTmpUsedCarsSearchResultsPageViews(Integer tmpUsedCarsSearchResultsPageViews) {
        this.tmpUsedCarsSearchResultsPageViews = tmpUsedCarsSearchResultsPageViews;
    }

    public Integer getTmpNewCarsSearchResultsPageViews() {
        return tmpNewCarsSearchResultsPageViews;
    }

    public void setTmpNewCarsSearchResultsPageViews(Integer tmpNewCarsSearchResultsPageViews) {
        this.tmpNewCarsSearchResultsPageViews = tmpNewCarsSearchResultsPageViews;
    }

    public Integer getTmpUsedCarsVehicleDetailPageViews() {
        return tmpUsedCarsVehicleDetailPageViews;
    }

    public void setTmpUsedCarsVehicleDetailPageViews(Integer tmpUsedCarsVehicleDetailPageViews) {
        this.tmpUsedCarsVehicleDetailPageViews = tmpUsedCarsVehicleDetailPageViews;
    }

    public Integer getTmpNewCarsVehicleDetailPageViews() {
        return tmpNewCarsVehicleDetailPageViews;
    }

    public void setTmpNewCarsVehicleDetailPageViews(Integer tmpNewCarsVehicleDetailPageViews) {
        this.tmpNewCarsVehicleDetailPageViews = tmpNewCarsVehicleDetailPageViews;
    }

    public Date getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(Date createdOn) {
        this.createdOn = createdOn;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TmpiStatsMonthly)) {
            return false;
        }
        TmpiStatsMonthly other = (TmpiStatsMonthly) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.gatehouseauto.datastudio.db.domain.TmpiStatsMonthly[ id=" + id + " ]";
    }
    
}
