/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gatehouseauto.datastudio.db.domain;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author basit
 */
@Entity
@Table(name = "vehicle_count")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "VehicleCount.findAll", query = "SELECT v FROM VehicleCount v")
    , @NamedQuery(name = "VehicleCount.findById", query = "SELECT v FROM VehicleCount v WHERE v.id = :id")
    , @NamedQuery(name = "VehicleCount.findByFid", query = "SELECT v FROM VehicleCount v WHERE v.fid = :fid")
    , @NamedQuery(name = "VehicleCount.findByUsedCount", query = "SELECT v FROM VehicleCount v WHERE v.usedCount = :usedCount")
    , @NamedQuery(name = "VehicleCount.findByNewCount", query = "SELECT v FROM VehicleCount v WHERE v.newCount = :newCount")
    , @NamedQuery(name = "VehicleCount.findByCpoCount", query = "SELECT v FROM VehicleCount v WHERE v.cpoCount = :cpoCount")
    , @NamedQuery(name = "VehicleCount.findByNoPhotos", query = "SELECT v FROM VehicleCount v WHERE v.noPhotos = :noPhotos")
    , @NamedQuery(name = "VehicleCount.findByUnder5", query = "SELECT v FROM VehicleCount v WHERE v.under5 = :under5")
    , @NamedQuery(name = "VehicleCount.findByStockImages", query = "SELECT v FROM VehicleCount v WHERE v.stockImages = :stockImages")
    , @NamedQuery(name = "VehicleCount.findByNoTagline", query = "SELECT v FROM VehicleCount v WHERE v.noTagline = :noTagline")
    , @NamedQuery(name = "VehicleCount.findByNoPrice", query = "SELECT v FROM VehicleCount v WHERE v.noPrice = :noPrice")
    , @NamedQuery(name = "VehicleCount.findByDateOn", query = "SELECT v FROM VehicleCount v WHERE v.dateOn = :dateOn")})
public class VehicleCount implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 45)
    @Column(name = "fid")
    private String fid;
    @Column(name = "used_count")
    private Integer usedCount;
    @Column(name = "new_count")
    private Integer newCount;
    @Column(name = "cpo_count")
    private Integer cpoCount;
    @Column(name = "no_photos")
    private Integer noPhotos;
    @Column(name = "under_5")
    private Integer under5;
    @Column(name = "stock_images")
    private Integer stockImages;
    @Column(name = "no_tagline")
    private Integer noTagline;
    @Column(name = "no_price")
    private Integer noPrice;
    @Basic(optional = false)
    @NotNull
    @Column(name = "date_on")
    @Temporal(TemporalType.DATE)
    private Date dateOn;

    public VehicleCount() {
    }

    public VehicleCount(Integer id) {
        this.id = id;
    }

    public VehicleCount(Integer id, String fid, Date dateOn) {
        this.id = id;
        this.fid = fid;
        this.dateOn = dateOn;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getFid() {
        return fid;
    }

    public void setFid(String fid) {
        this.fid = fid;
    }

    public Integer getUsedCount() {
        return usedCount;
    }

    public void setUsedCount(Integer usedCount) {
        this.usedCount = usedCount;
    }

    public Integer getNewCount() {
        return newCount;
    }

    public void setNewCount(Integer newCount) {
        this.newCount = newCount;
    }

    public Integer getCpoCount() {
        return cpoCount;
    }

    public void setCpoCount(Integer cpoCount) {
        this.cpoCount = cpoCount;
    }

    public Integer getNoPhotos() {
        return noPhotos;
    }

    public void setNoPhotos(Integer noPhotos) {
        this.noPhotos = noPhotos;
    }

    public Integer getUnder5() {
        return under5;
    }

    public void setUnder5(Integer under5) {
        this.under5 = under5;
    }

    public Integer getStockImages() {
        return stockImages;
    }

    public void setStockImages(Integer stockImages) {
        this.stockImages = stockImages;
    }

    public Integer getNoTagline() {
        return noTagline;
    }

    public void setNoTagline(Integer noTagline) {
        this.noTagline = noTagline;
    }

    public Integer getNoPrice() {
        return noPrice;
    }

    public void setNoPrice(Integer noPrice) {
        this.noPrice = noPrice;
    }

    public Date getDateOn() {
        return dateOn;
    }

    public void setDateOn(Date dateOn) {
        this.dateOn = dateOn;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof VehicleCount)) {
            return false;
        }
        VehicleCount other = (VehicleCount) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.gatehouseauto.datastudio.db.domain.VehicleCount[ id=" + id + " ]";
    }
    
}
