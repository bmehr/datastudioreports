/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gatehouseauto.datastudio.db.domain;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author basit
 */
@Entity
@Table(name = "tmpi_customer_list")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "TmpiCustomerList.findAll", query = "SELECT t FROM TmpiCustomerList t")
    , @NamedQuery(name = "TmpiCustomerList.findById", query = "SELECT t FROM TmpiCustomerList t WHERE t.id = :id")
    , @NamedQuery(name = "TmpiCustomerList.findByAccId", query = "SELECT t FROM TmpiCustomerList t WHERE t.accId = :accId")
    , @NamedQuery(name = "TmpiCustomerList.findByDealerName", query = "SELECT t FROM TmpiCustomerList t WHERE t.dealerName = :dealerName")
    , @NamedQuery(name = "TmpiCustomerList.findByPhone", query = "SELECT t FROM TmpiCustomerList t WHERE t.phone = :phone")
    , @NamedQuery(name = "TmpiCustomerList.findByLeads", query = "SELECT t FROM TmpiCustomerList t WHERE t.leads = :leads")
    , @NamedQuery(name = "TmpiCustomerList.findByDirectories", query = "SELECT t FROM TmpiCustomerList t WHERE t.directories = :directories")
    , @NamedQuery(name = "TmpiCustomerList.findByDisplay", query = "SELECT t FROM TmpiCustomerList t WHERE t.display = :display")
    , @NamedQuery(name = "TmpiCustomerList.findBySmartads", query = "SELECT t FROM TmpiCustomerList t WHERE t.smartads = :smartads")
    , @NamedQuery(name = "TmpiCustomerList.findBySocial", query = "SELECT t FROM TmpiCustomerList t WHERE t.social = :social")
    , @NamedQuery(name = "TmpiCustomerList.findByExported", query = "SELECT t FROM TmpiCustomerList t WHERE t.exported = :exported")
    , @NamedQuery(name = "TmpiCustomerList.findByFid", query = "SELECT t FROM TmpiCustomerList t WHERE t.fid = :fid")
    , @NamedQuery(name = "TmpiCustomerList.findByUpdatedOn", query = "SELECT t FROM TmpiCustomerList t WHERE t.updatedOn = :updatedOn")})
public class TmpiCustomerList implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 20)
    @Column(name = "acc_id")
    private String accId;
    @Size(max = 255)
    @Column(name = "dealer_name")
    private String dealerName;
    // @Pattern(regexp="^\\(?(\\d{3})\\)?[- ]?(\\d{3})[- ]?(\\d{4})$", message="Invalid phone/fax format, should be as xxx-xxx-xxxx")//if the field contains phone or fax number consider using this annotation to enforce field validation
    @Size(max = 20)
    @Column(name = "phone")
    private String phone;
    @Column(name = "leads")
    private Integer leads;
    @Column(name = "directories")
    private Integer directories;
    @Column(name = "display")
    private Integer display;
    @Column(name = "smartads")
    private Integer smartads;
    @Column(name = "social")
    private Integer social;
    @Size(max = 20)
    @Column(name = "exported")
    private String exported;
    @Size(max = 20)
    @Column(name = "fid")
    private String fid;
    @Column(name = "updated_on")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updatedOn;

    public TmpiCustomerList() {
    }

    public TmpiCustomerList(Integer id) {
        this.id = id;
    }

    public TmpiCustomerList(Integer id, String accId) {
        this.id = id;
        this.accId = accId;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getAccId() {
        return accId;
    }

    public void setAccId(String accId) {
        this.accId = accId;
    }

    public String getDealerName() {
        return dealerName;
    }

    public void setDealerName(String dealerName) {
        this.dealerName = dealerName;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public Integer getLeads() {
        return leads;
    }

    public void setLeads(Integer leads) {
        this.leads = leads;
    }

    public Integer getDirectories() {
        return directories;
    }

    public void setDirectories(Integer directories) {
        this.directories = directories;
    }

    public Integer getDisplay() {
        return display;
    }

    public void setDisplay(Integer display) {
        this.display = display;
    }

    public Integer getSmartads() {
        return smartads;
    }

    public void setSmartads(Integer smartads) {
        this.smartads = smartads;
    }

    public Integer getSocial() {
        return social;
    }

    public void setSocial(Integer social) {
        this.social = social;
    }

    public String getExported() {
        return exported;
    }

    public void setExported(String exported) {
        this.exported = exported;
    }

    public String getFid() {
        return fid;
    }

    public void setFid(String fid) {
        this.fid = fid;
    }

    public Date getUpdatedOn() {
        return updatedOn;
    }

    public void setUpdatedOn(Date updatedOn) {
        this.updatedOn = updatedOn;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TmpiCustomerList)) {
            return false;
        }
        TmpiCustomerList other = (TmpiCustomerList) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.gatehouseauto.datastudio.db.domain.TmpiCustomerList[ id=" + id + " ]";
    }
    
}
