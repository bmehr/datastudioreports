/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gatehouseauto.datastudio.db.domain;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author basit
 */
@Entity
@Table(name = "tmpi_email_leads")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "TmpiEmailLeads.findAll", query = "SELECT t FROM TmpiEmailLeads t")
    , @NamedQuery(name = "TmpiEmailLeads.findById", query = "SELECT t FROM TmpiEmailLeads t WHERE t.id = :id")
    , @NamedQuery(name = "TmpiEmailLeads.findByAccId", query = "SELECT t FROM TmpiEmailLeads t WHERE t.accId = :accId")
    , @NamedQuery(name = "TmpiEmailLeads.findByFid", query = "SELECT t FROM TmpiEmailLeads t WHERE t.fid = :fid")
    , @NamedQuery(name = "TmpiEmailLeads.findByVin", query = "SELECT t FROM TmpiEmailLeads t WHERE t.vin = :vin")
    , @NamedQuery(name = "TmpiEmailLeads.findByLeadId", query = "SELECT t FROM TmpiEmailLeads t WHERE t.leadId = :leadId")
    , @NamedQuery(name = "TmpiEmailLeads.findByLeadDate", query = "SELECT t FROM TmpiEmailLeads t WHERE t.leadDate = :leadDate")
    , @NamedQuery(name = "TmpiEmailLeads.findByEmail", query = "SELECT t FROM TmpiEmailLeads t WHERE t.email = :email")
    , @NamedQuery(name = "TmpiEmailLeads.findByFirstName", query = "SELECT t FROM TmpiEmailLeads t WHERE t.firstName = :firstName")
    , @NamedQuery(name = "TmpiEmailLeads.findByLastName", query = "SELECT t FROM TmpiEmailLeads t WHERE t.lastName = :lastName")
    , @NamedQuery(name = "TmpiEmailLeads.findByPhone", query = "SELECT t FROM TmpiEmailLeads t WHERE t.phone = :phone")
    , @NamedQuery(name = "TmpiEmailLeads.findByZip", query = "SELECT t FROM TmpiEmailLeads t WHERE t.zip = :zip")
    , @NamedQuery(name = "TmpiEmailLeads.findByComment", query = "SELECT t FROM TmpiEmailLeads t WHERE t.comment = :comment")
    , @NamedQuery(name = "TmpiEmailLeads.findByUpdatedOn", query = "SELECT t FROM TmpiEmailLeads t WHERE t.updatedOn = :updatedOn")})
public class TmpiEmailLeads implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Size(max = 20)
    @Column(name = "acc_id")
    private String accId;
    @Size(max = 10)
    @Column(name = "fid")
    private String fid;
    @Size(max = 25)
    @Column(name = "vin")
    private String vin;
    @Size(max = 20)
    @Column(name = "lead_id")
    private String leadId;
    @Column(name = "lead_date")
    @Temporal(TemporalType.DATE)
    private Date leadDate;
    // @Pattern(regexp="[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?", message="Invalid email")//if the field contains email address consider using this annotation to enforce field validation
    @Size(max = 100)
    @Column(name = "email")
    private String email;
    @Size(max = 250)
    @Column(name = "first_name")
    private String firstName;
    @Size(max = 250)
    @Column(name = "last_name")
    private String lastName;
    // @Pattern(regexp="^\\(?(\\d{3})\\)?[- ]?(\\d{3})[- ]?(\\d{4})$", message="Invalid phone/fax format, should be as xxx-xxx-xxxx")//if the field contains phone or fax number consider using this annotation to enforce field validation
    @Size(max = 20)
    @Column(name = "phone")
    private String phone;
    @Size(max = 10)
    @Column(name = "zip")
    private String zip;
    @Size(max = 400)
    @Column(name = "comment")
    private String comment;
    @Column(name = "updated_on")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updatedOn;

    public TmpiEmailLeads() {
    }

    public TmpiEmailLeads(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getAccId() {
        return accId;
    }

    public void setAccId(String accId) {
        this.accId = accId;
    }

    public String getFid() {
        return fid;
    }

    public void setFid(String fid) {
        this.fid = fid;
    }

    public String getVin() {
        return vin;
    }

    public void setVin(String vin) {
        this.vin = vin;
    }

    public String getLeadId() {
        return leadId;
    }

    public void setLeadId(String leadId) {
        this.leadId = leadId;
    }

    public Date getLeadDate() {
        return leadDate;
    }

    public void setLeadDate(Date leadDate) {
        this.leadDate = leadDate;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getZip() {
        return zip;
    }

    public void setZip(String zip) {
        this.zip = zip;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public Date getUpdatedOn() {
        return updatedOn;
    }

    public void setUpdatedOn(Date updatedOn) {
        this.updatedOn = updatedOn;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TmpiEmailLeads)) {
            return false;
        }
        TmpiEmailLeads other = (TmpiEmailLeads) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.gatehouseauto.datastudio.db.domain.TmpiEmailLeads[ id=" + id + " ]";
    }
    
}
