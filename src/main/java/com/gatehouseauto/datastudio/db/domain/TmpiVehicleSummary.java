/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gatehouseauto.datastudio.db.domain;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author basit
 */
@Entity
@Table(name = "tmpi_vehicle_summary")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "TmpiVehicleSummary.findAll", query = "SELECT t FROM TmpiVehicleSummary t")
    , @NamedQuery(name = "TmpiVehicleSummary.findById", query = "SELECT t FROM TmpiVehicleSummary t WHERE t.id = :id")
    , @NamedQuery(name = "TmpiVehicleSummary.findByAccId", query = "SELECT t FROM TmpiVehicleSummary t WHERE t.accId = :accId")
    , @NamedQuery(name = "TmpiVehicleSummary.findByPacId", query = "SELECT t FROM TmpiVehicleSummary t WHERE t.pacId = :pacId")
    , @NamedQuery(name = "TmpiVehicleSummary.findByPacName", query = "SELECT t FROM TmpiVehicleSummary t WHERE t.pacName = :pacName")
    , @NamedQuery(name = "TmpiVehicleSummary.findByPacShortName", query = "SELECT t FROM TmpiVehicleSummary t WHERE t.pacShortName = :pacShortName")
    , @NamedQuery(name = "TmpiVehicleSummary.findByPacSort", query = "SELECT t FROM TmpiVehicleSummary t WHERE t.pacSort = :pacSort")
    , @NamedQuery(name = "TmpiVehicleSummary.findByTotal", query = "SELECT t FROM TmpiVehicleSummary t WHERE t.total = :total")
    , @NamedQuery(name = "TmpiVehicleSummary.findByCount", query = "SELECT t FROM TmpiVehicleSummary t WHERE t.count = :count")
    , @NamedQuery(name = "TmpiVehicleSummary.findByExported", query = "SELECT t FROM TmpiVehicleSummary t WHERE t.exported = :exported")
    , @NamedQuery(name = "TmpiVehicleSummary.findByUpdatedOn", query = "SELECT t FROM TmpiVehicleSummary t WHERE t.updatedOn = :updatedOn")})
public class TmpiVehicleSummary implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Size(max = 10)
    @Column(name = "acc_id")
    private String accId;
    @Size(max = 10)
    @Column(name = "pac_id")
    private String pacId;
    @Size(max = 50)
    @Column(name = "pac_name")
    private String pacName;
    @Size(max = 50)
    @Column(name = "pac_short_name")
    private String pacShortName;
    @Size(max = 10)
    @Column(name = "pac_sort")
    private String pacSort;
    @Size(max = 10)
    @Column(name = "total")
    private String total;
    @Size(max = 10)
    @Column(name = "count")
    private String count;
    @Column(name = "exported")
    @Temporal(TemporalType.DATE)
    private Date exported;
    @Column(name = "updated_on")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updatedOn;

    public TmpiVehicleSummary() {
    }

    public TmpiVehicleSummary(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getAccId() {
        return accId;
    }

    public void setAccId(String accId) {
        this.accId = accId;
    }

    public String getPacId() {
        return pacId;
    }

    public void setPacId(String pacId) {
        this.pacId = pacId;
    }

    public String getPacName() {
        return pacName;
    }

    public void setPacName(String pacName) {
        this.pacName = pacName;
    }

    public String getPacShortName() {
        return pacShortName;
    }

    public void setPacShortName(String pacShortName) {
        this.pacShortName = pacShortName;
    }

    public String getPacSort() {
        return pacSort;
    }

    public void setPacSort(String pacSort) {
        this.pacSort = pacSort;
    }

    public String getTotal() {
        return total;
    }

    public void setTotal(String total) {
        this.total = total;
    }

    public String getCount() {
        return count;
    }

    public void setCount(String count) {
        this.count = count;
    }

    public Date getExported() {
        return exported;
    }

    public void setExported(Date exported) {
        this.exported = exported;
    }

    public Date getUpdatedOn() {
        return updatedOn;
    }

    public void setUpdatedOn(Date updatedOn) {
        this.updatedOn = updatedOn;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TmpiVehicleSummary)) {
            return false;
        }
        TmpiVehicleSummary other = (TmpiVehicleSummary) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.gatehouseauto.datastudio.db.domain.TmpiVehicleSummary[ id=" + id + " ]";
    }
    
}
