/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gatehouseauto.datastudio.db.domain;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author basit
 */
@Entity
@Table(name = "tmpi_flight_info")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "TmpiFlightInfo.findAll", query = "SELECT t FROM TmpiFlightInfo t")
    , @NamedQuery(name = "TmpiFlightInfo.findById", query = "SELECT t FROM TmpiFlightInfo t WHERE t.id = :id")
    , @NamedQuery(name = "TmpiFlightInfo.findByAccId", query = "SELECT t FROM TmpiFlightInfo t WHERE t.accId = :accId")
    , @NamedQuery(name = "TmpiFlightInfo.findByCampaignId", query = "SELECT t FROM TmpiFlightInfo t WHERE t.campaignId = :campaignId")
    , @NamedQuery(name = "TmpiFlightInfo.findByFlightId", query = "SELECT t FROM TmpiFlightInfo t WHERE t.flightId = :flightId")
    , @NamedQuery(name = "TmpiFlightInfo.findByStatus", query = "SELECT t FROM TmpiFlightInfo t WHERE t.status = :status")
    , @NamedQuery(name = "TmpiFlightInfo.findByStartDate", query = "SELECT t FROM TmpiFlightInfo t WHERE t.startDate = :startDate")
    , @NamedQuery(name = "TmpiFlightInfo.findByEndDate", query = "SELECT t FROM TmpiFlightInfo t WHERE t.endDate = :endDate")
    , @NamedQuery(name = "TmpiFlightInfo.findByFirstResultDate", query = "SELECT t FROM TmpiFlightInfo t WHERE t.firstResultDate = :firstResultDate")
    , @NamedQuery(name = "TmpiFlightInfo.findByLanguage", query = "SELECT t FROM TmpiFlightInfo t WHERE t.language = :language")
    , @NamedQuery(name = "TmpiFlightInfo.findByInventory", query = "SELECT t FROM TmpiFlightInfo t WHERE t.inventory = :inventory")
    , @NamedQuery(name = "TmpiFlightInfo.findByVdps", query = "SELECT t FROM TmpiFlightInfo t WHERE t.vdps = :vdps")
    , @NamedQuery(name = "TmpiFlightInfo.findByImpressions", query = "SELECT t FROM TmpiFlightInfo t WHERE t.impressions = :impressions")
    , @NamedQuery(name = "TmpiFlightInfo.findByClicks", query = "SELECT t FROM TmpiFlightInfo t WHERE t.clicks = :clicks")
    , @NamedQuery(name = "TmpiFlightInfo.findByProfileViews", query = "SELECT t FROM TmpiFlightInfo t WHERE t.profileViews = :profileViews")
    , @NamedQuery(name = "TmpiFlightInfo.findByManualTarget", query = "SELECT t FROM TmpiFlightInfo t WHERE t.manualTarget = :manualTarget")
    , @NamedQuery(name = "TmpiFlightInfo.findByActions", query = "SELECT t FROM TmpiFlightInfo t WHERE t.actions = :actions")
    , @NamedQuery(name = "TmpiFlightInfo.findByCost", query = "SELECT t FROM TmpiFlightInfo t WHERE t.cost = :cost")
    , @NamedQuery(name = "TmpiFlightInfo.findByPackage1", query = "SELECT t FROM TmpiFlightInfo t WHERE t.package1 = :package1")
    , @NamedQuery(name = "TmpiFlightInfo.findByCampaignName", query = "SELECT t FROM TmpiFlightInfo t WHERE t.campaignName = :campaignName")
    , @NamedQuery(name = "TmpiFlightInfo.findByCapturedDate", query = "SELECT t FROM TmpiFlightInfo t WHERE t.capturedDate = :capturedDate")
    , @NamedQuery(name = "TmpiFlightInfo.findByRenewalFlag", query = "SELECT t FROM TmpiFlightInfo t WHERE t.renewalFlag = :renewalFlag")
    , @NamedQuery(name = "TmpiFlightInfo.findByUpdatedOn", query = "SELECT t FROM TmpiFlightInfo t WHERE t.updatedOn = :updatedOn")})
public class TmpiFlightInfo implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Size(max = 20)
    @Column(name = "acc_id")
    private String accId;
    @Size(max = 20)
    @Column(name = "campaign_id")
    private String campaignId;
    @Size(max = 20)
    @Column(name = "flight_id")
    private String flightId;
    @Size(max = 20)
    @Column(name = "status")
    private String status;
    @Column(name = "start_date")
    @Temporal(TemporalType.DATE)
    private Date startDate;
    @Column(name = "end_date")
    @Temporal(TemporalType.DATE)
    private Date endDate;
    @Column(name = "first_result_date")
    @Temporal(TemporalType.DATE)
    private Date firstResultDate;
    @Size(max = 20)
    @Column(name = "language")
    private String language;
    @Size(max = 50)
    @Column(name = "inventory")
    private String inventory;
    @Size(max = 10)
    @Column(name = "vdps")
    private String vdps;
    @Size(max = 10)
    @Column(name = "impressions")
    private String impressions;
    @Size(max = 10)
    @Column(name = "clicks")
    private String clicks;
    @Size(max = 10)
    @Column(name = "profile_views")
    private String profileViews;
    @Size(max = 10)
    @Column(name = "manual_target")
    private String manualTarget;
    @Size(max = 10)
    @Column(name = "actions")
    private String actions;
    @Size(max = 10)
    @Column(name = "cost")
    private String cost;
    @Size(max = 50)
    @Column(name = "package")
    private String package1;
    @Size(max = 255)
    @Column(name = "campaign_name")
    private String campaignName;
    @Column(name = "captured_date")
    @Temporal(TemporalType.DATE)
    private Date capturedDate;
    @Column(name = "renewal_flag")
    private Integer renewalFlag;
    @Column(name = "updated_on")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updatedOn;

    public TmpiFlightInfo() {
    }

    public TmpiFlightInfo(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getAccId() {
        return accId;
    }

    public void setAccId(String accId) {
        this.accId = accId;
    }

    public String getCampaignId() {
        return campaignId;
    }

    public void setCampaignId(String campaignId) {
        this.campaignId = campaignId;
    }

    public String getFlightId() {
        return flightId;
    }

    public void setFlightId(String flightId) {
        this.flightId = flightId;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public Date getFirstResultDate() {
        return firstResultDate;
    }

    public void setFirstResultDate(Date firstResultDate) {
        this.firstResultDate = firstResultDate;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public String getInventory() {
        return inventory;
    }

    public void setInventory(String inventory) {
        this.inventory = inventory;
    }

    public String getVdps() {
        return vdps;
    }

    public void setVdps(String vdps) {
        this.vdps = vdps;
    }

    public String getImpressions() {
        return impressions;
    }

    public void setImpressions(String impressions) {
        this.impressions = impressions;
    }

    public String getClicks() {
        return clicks;
    }

    public void setClicks(String clicks) {
        this.clicks = clicks;
    }

    public String getProfileViews() {
        return profileViews;
    }

    public void setProfileViews(String profileViews) {
        this.profileViews = profileViews;
    }

    public String getManualTarget() {
        return manualTarget;
    }

    public void setManualTarget(String manualTarget) {
        this.manualTarget = manualTarget;
    }

    public String getActions() {
        return actions;
    }

    public void setActions(String actions) {
        this.actions = actions;
    }

    public String getCost() {
        return cost;
    }

    public void setCost(String cost) {
        this.cost = cost;
    }

    public String getPackage1() {
        return package1;
    }

    public void setPackage1(String package1) {
        this.package1 = package1;
    }

    public String getCampaignName() {
        return campaignName;
    }

    public void setCampaignName(String campaignName) {
        this.campaignName = campaignName;
    }

    public Date getCapturedDate() {
        return capturedDate;
    }

    public void setCapturedDate(Date capturedDate) {
        this.capturedDate = capturedDate;
    }

    public Integer getRenewalFlag() {
        return renewalFlag;
    }

    public void setRenewalFlag(Integer renewalFlag) {
        this.renewalFlag = renewalFlag;
    }

    public Date getUpdatedOn() {
        return updatedOn;
    }

    public void setUpdatedOn(Date updatedOn) {
        this.updatedOn = updatedOn;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TmpiFlightInfo)) {
            return false;
        }
        TmpiFlightInfo other = (TmpiFlightInfo) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.gatehouseauto.datastudio.db.domain.TmpiFlightInfo[ id=" + id + " ]";
    }
    
}
