/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gatehouseauto.datastudio.db.domain;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author basit
 */
@Entity
@Table(name = "lesa_total_views")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "LesaTotalViews.findAll", query = "SELECT l FROM LesaTotalViews l")
    , @NamedQuery(name = "LesaTotalViews.findById", query = "SELECT l FROM LesaTotalViews l WHERE l.id = :id")
    , @NamedQuery(name = "LesaTotalViews.findByFid", query = "SELECT l FROM LesaTotalViews l WHERE l.fid = :fid")
    , @NamedQuery(name = "LesaTotalViews.findByReportMonth", query = "SELECT l FROM LesaTotalViews l WHERE l.reportMonth = :reportMonth")
    , @NamedQuery(name = "LesaTotalViews.findByVehiclesWithVideo", query = "SELECT l FROM LesaTotalViews l WHERE l.vehiclesWithVideo = :vehiclesWithVideo")
    , @NamedQuery(name = "LesaTotalViews.findByVideoViews", query = "SELECT l FROM LesaTotalViews l WHERE l.videoViews = :videoViews")
    , @NamedQuery(name = "LesaTotalViews.findByTotalCustomers", query = "SELECT l FROM LesaTotalViews l WHERE l.totalCustomers = :totalCustomers")
    , @NamedQuery(name = "LesaTotalViews.findByAutotraderViews", query = "SELECT l FROM LesaTotalViews l WHERE l.autotraderViews = :autotraderViews")
    , @NamedQuery(name = "LesaTotalViews.findByCarscomViews", query = "SELECT l FROM LesaTotalViews l WHERE l.carscomViews = :carscomViews")
    , @NamedQuery(name = "LesaTotalViews.findByCreatedOn", query = "SELECT l FROM LesaTotalViews l WHERE l.createdOn = :createdOn")})
public class LesaTotalViews implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 8)
    @Column(name = "fid")
    private String fid;
    @Basic(optional = false)
    @NotNull
    @Column(name = "report_month")
    @Temporal(TemporalType.DATE)
    private Date reportMonth;
    @Column(name = "vehicles_with_video")
    private Integer vehiclesWithVideo;
    @Column(name = "video_views")
    private Integer videoViews;
    @Column(name = "total_customers")
    private Integer totalCustomers;
    @Column(name = "autotrader_views")
    private Integer autotraderViews;
    @Column(name = "carscom_views")
    private Integer carscomViews;
    @Column(name = "created_on")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdOn;

    public LesaTotalViews() {
    }

    public LesaTotalViews(Integer id) {
        this.id = id;
    }

    public LesaTotalViews(Integer id, String fid, Date reportMonth) {
        this.id = id;
        this.fid = fid;
        this.reportMonth = reportMonth;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getFid() {
        return fid;
    }

    public void setFid(String fid) {
        this.fid = fid;
    }

    public Date getReportMonth() {
        return reportMonth;
    }

    public void setReportMonth(Date reportMonth) {
        this.reportMonth = reportMonth;
    }

    public Integer getVehiclesWithVideo() {
        return vehiclesWithVideo;
    }

    public void setVehiclesWithVideo(Integer vehiclesWithVideo) {
        this.vehiclesWithVideo = vehiclesWithVideo;
    }

    public Integer getVideoViews() {
        return videoViews;
    }

    public void setVideoViews(Integer videoViews) {
        this.videoViews = videoViews;
    }

    public Integer getTotalCustomers() {
        return totalCustomers;
    }

    public void setTotalCustomers(Integer totalCustomers) {
        this.totalCustomers = totalCustomers;
    }

    public Integer getAutotraderViews() {
        return autotraderViews;
    }

    public void setAutotraderViews(Integer autotraderViews) {
        this.autotraderViews = autotraderViews;
    }

    public Integer getCarscomViews() {
        return carscomViews;
    }

    public void setCarscomViews(Integer carscomViews) {
        this.carscomViews = carscomViews;
    }

    public Date getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(Date createdOn) {
        this.createdOn = createdOn;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof LesaTotalViews)) {
            return false;
        }
        LesaTotalViews other = (LesaTotalViews) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.gatehouseauto.datastudio.db.domain.LesaTotalViews[ id=" + id + " ]";
    }
    
}
