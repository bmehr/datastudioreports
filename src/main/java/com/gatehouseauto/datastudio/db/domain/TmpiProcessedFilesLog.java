/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gatehouseauto.datastudio.db.domain;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author basit
 */
@Entity
@Table(name = "tmpi_processed_files_log")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "TmpiProcessedFilesLog.findAll", query = "SELECT t FROM TmpiProcessedFilesLog t")
    , @NamedQuery(name = "TmpiProcessedFilesLog.findById", query = "SELECT t FROM TmpiProcessedFilesLog t WHERE t.id = :id")
    , @NamedQuery(name = "TmpiProcessedFilesLog.findByFullFileName", query = "SELECT t FROM TmpiProcessedFilesLog t WHERE t.fullFileName = :fullFileName")
    , @NamedQuery(name = "TmpiProcessedFilesLog.findByMessage", query = "SELECT t FROM TmpiProcessedFilesLog t WHERE t.message = :message")
    , @NamedQuery(name = "TmpiProcessedFilesLog.findByCreatedOn", query = "SELECT t FROM TmpiProcessedFilesLog t WHERE t.createdOn = :createdOn")})
public class TmpiProcessedFilesLog implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 450)
    @Column(name = "full_file_name")
    private String fullFileName;
    @Size(max = 450)
    @Column(name = "message")
    private String message;
    @Column(name = "created_on")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdOn;

    public TmpiProcessedFilesLog() {
    }

    public TmpiProcessedFilesLog(Integer id) {
        this.id = id;
    }

    public TmpiProcessedFilesLog(Integer id, String fullFileName) {
        this.id = id;
        this.fullFileName = fullFileName;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getFullFileName() {
        return fullFileName;
    }

    public void setFullFileName(String fullFileName) {
        this.fullFileName = fullFileName;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Date getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(Date createdOn) {
        this.createdOn = createdOn;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TmpiProcessedFilesLog)) {
            return false;
        }
        TmpiProcessedFilesLog other = (TmpiProcessedFilesLog) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.gatehouseauto.datastudio.db.domain.TmpiProcessedFilesLog[ id=" + id + " ]";
    }
    
}
