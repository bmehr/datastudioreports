/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gatehouseauto.datastudio.db.domain;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author basit
 */
@Entity
@Table(name = "dealer_inventory")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "DealerInventory.findAll", query = "SELECT d FROM DealerInventory d")
    , @NamedQuery(name = "DealerInventory.findById", query = "SELECT d FROM DealerInventory d WHERE d.id = :id")
    , @NamedQuery(name = "DealerInventory.findByFid", query = "SELECT d FROM DealerInventory d WHERE d.fid = :fid")
    , @NamedQuery(name = "DealerInventory.findByStockNumber", query = "SELECT d FROM DealerInventory d WHERE d.stockNumber = :stockNumber")
    , @NamedQuery(name = "DealerInventory.findByInventoryDate", query = "SELECT d FROM DealerInventory d WHERE d.inventoryDate = :inventoryDate")
    , @NamedQuery(name = "DealerInventory.findByVehicleType", query = "SELECT d FROM DealerInventory d WHERE d.vehicleType = :vehicleType")
    , @NamedQuery(name = "DealerInventory.findByVehicleStatus", query = "SELECT d FROM DealerInventory d WHERE d.vehicleStatus = :vehicleStatus")
    , @NamedQuery(name = "DealerInventory.findByInvoicePrice", query = "SELECT d FROM DealerInventory d WHERE d.invoicePrice = :invoicePrice")
    , @NamedQuery(name = "DealerInventory.findByPackAmount", query = "SELECT d FROM DealerInventory d WHERE d.packAmount = :packAmount")
    , @NamedQuery(name = "DealerInventory.findByCost", query = "SELECT d FROM DealerInventory d WHERE d.cost = :cost")
    , @NamedQuery(name = "DealerInventory.findByHoldbackAmount", query = "SELECT d FROM DealerInventory d WHERE d.holdbackAmount = :holdbackAmount")
    , @NamedQuery(name = "DealerInventory.findByListPrice", query = "SELECT d FROM DealerInventory d WHERE d.listPrice = :listPrice")
    , @NamedQuery(name = "DealerInventory.findByMsrp", query = "SELECT d FROM DealerInventory d WHERE d.msrp = :msrp")
    , @NamedQuery(name = "DealerInventory.findByLotLocation", query = "SELECT d FROM DealerInventory d WHERE d.lotLocation = :lotLocation")
    , @NamedQuery(name = "DealerInventory.findByVehicleCondition", query = "SELECT d FROM DealerInventory d WHERE d.vehicleCondition = :vehicleCondition")
    , @NamedQuery(name = "DealerInventory.findByIsCertified", query = "SELECT d FROM DealerInventory d WHERE d.isCertified = :isCertified")
    , @NamedQuery(name = "DealerInventory.findByCertificationNumber", query = "SELECT d FROM DealerInventory d WHERE d.certificationNumber = :certificationNumber")
    , @NamedQuery(name = "DealerInventory.findByVin", query = "SELECT d FROM DealerInventory d WHERE d.vin = :vin")
    , @NamedQuery(name = "DealerInventory.findByMake", query = "SELECT d FROM DealerInventory d WHERE d.make = :make")
    , @NamedQuery(name = "DealerInventory.findByModel", query = "SELECT d FROM DealerInventory d WHERE d.model = :model")
    , @NamedQuery(name = "DealerInventory.findByModelYear", query = "SELECT d FROM DealerInventory d WHERE d.modelYear = :modelYear")
    , @NamedQuery(name = "DealerInventory.findByModelCode", query = "SELECT d FROM DealerInventory d WHERE d.modelCode = :modelCode")
    , @NamedQuery(name = "DealerInventory.findByTrimLevel", query = "SELECT d FROM DealerInventory d WHERE d.trimLevel = :trimLevel")
    , @NamedQuery(name = "DealerInventory.findBySubTrimLevel", query = "SELECT d FROM DealerInventory d WHERE d.subTrimLevel = :subTrimLevel")
    , @NamedQuery(name = "DealerInventory.findByClassification", query = "SELECT d FROM DealerInventory d WHERE d.classification = :classification")
    , @NamedQuery(name = "DealerInventory.findByVehicleTypeCode", query = "SELECT d FROM DealerInventory d WHERE d.vehicleTypeCode = :vehicleTypeCode")
    , @NamedQuery(name = "DealerInventory.findByOdometer", query = "SELECT d FROM DealerInventory d WHERE d.odometer = :odometer")
    , @NamedQuery(name = "DealerInventory.findByPayloadCapacity", query = "SELECT d FROM DealerInventory d WHERE d.payloadCapacity = :payloadCapacity")
    , @NamedQuery(name = "DealerInventory.findBySeatingCapacity", query = "SELECT d FROM DealerInventory d WHERE d.seatingCapacity = :seatingCapacity")
    , @NamedQuery(name = "DealerInventory.findByWheelBase", query = "SELECT d FROM DealerInventory d WHERE d.wheelBase = :wheelBase")
    , @NamedQuery(name = "DealerInventory.findByBodyDescription", query = "SELECT d FROM DealerInventory d WHERE d.bodyDescription = :bodyDescription")
    , @NamedQuery(name = "DealerInventory.findByBodyDoorCount", query = "SELECT d FROM DealerInventory d WHERE d.bodyDoorCount = :bodyDoorCount")
    , @NamedQuery(name = "DealerInventory.findByDriveTrainDescription", query = "SELECT d FROM DealerInventory d WHERE d.driveTrainDescription = :driveTrainDescription")
    , @NamedQuery(name = "DealerInventory.findByEngineDescription", query = "SELECT d FROM DealerInventory d WHERE d.engineDescription = :engineDescription")
    , @NamedQuery(name = "DealerInventory.findByEngineCylinderCount", query = "SELECT d FROM DealerInventory d WHERE d.engineCylinderCount = :engineCylinderCount")
    , @NamedQuery(name = "DealerInventory.findByTransmissionDescription", query = "SELECT d FROM DealerInventory d WHERE d.transmissionDescription = :transmissionDescription")
    , @NamedQuery(name = "DealerInventory.findByTransmissionType", query = "SELECT d FROM DealerInventory d WHERE d.transmissionType = :transmissionType")
    , @NamedQuery(name = "DealerInventory.findByExteriorColorDescription", query = "SELECT d FROM DealerInventory d WHERE d.exteriorColorDescription = :exteriorColorDescription")
    , @NamedQuery(name = "DealerInventory.findByExteriorColorBaseColor", query = "SELECT d FROM DealerInventory d WHERE d.exteriorColorBaseColor = :exteriorColorBaseColor")
    , @NamedQuery(name = "DealerInventory.findByInteriorDescription", query = "SELECT d FROM DealerInventory d WHERE d.interiorDescription = :interiorDescription")
    , @NamedQuery(name = "DealerInventory.findByInteriorColor", query = "SELECT d FROM DealerInventory d WHERE d.interiorColor = :interiorColor")
    , @NamedQuery(name = "DealerInventory.findByCreatedDate", query = "SELECT d FROM DealerInventory d WHERE d.createdDate = :createdDate")
    , @NamedQuery(name = "DealerInventory.findByLastModifiedDate", query = "SELECT d FROM DealerInventory d WHERE d.lastModifiedDate = :lastModifiedDate")
    , @NamedQuery(name = "DealerInventory.findByModifiedFlag", query = "SELECT d FROM DealerInventory d WHERE d.modifiedFlag = :modifiedFlag")
    , @NamedQuery(name = "DealerInventory.findByNumberOfImages", query = "SELECT d FROM DealerInventory d WHERE d.numberOfImages = :numberOfImages")
    , @NamedQuery(name = "DealerInventory.findByImageUrlPattern", query = "SELECT d FROM DealerInventory d WHERE d.imageUrlPattern = :imageUrlPattern")
    , @NamedQuery(name = "DealerInventory.findByImageSetModificationDate", query = "SELECT d FROM DealerInventory d WHERE d.imageSetModificationDate = :imageSetModificationDate")
    , @NamedQuery(name = "DealerInventory.findByIsSold", query = "SELECT d FROM DealerInventory d WHERE d.isSold = :isSold")
    , @NamedQuery(name = "DealerInventory.findByIsDms", query = "SELECT d FROM DealerInventory d WHERE d.isDms = :isDms")
    , @NamedQuery(name = "DealerInventory.findByCreatedOn", query = "SELECT d FROM DealerInventory d WHERE d.createdOn = :createdOn")
    , @NamedQuery(name = "DealerInventory.findByDateSold", query = "SELECT d FROM DealerInventory d WHERE d.dateSold = :dateSold")
    , @NamedQuery(name = "DealerInventory.findByLastUpdate", query = "SELECT d FROM DealerInventory d WHERE d.lastUpdate = :lastUpdate")
    , @NamedQuery(name = "DealerInventory.findByHighPrice", query = "SELECT d FROM DealerInventory d WHERE d.highPrice = :highPrice")
    , @NamedQuery(name = "DealerInventory.findByLowPrice", query = "SELECT d FROM DealerInventory d WHERE d.lowPrice = :lowPrice")
    , @NamedQuery(name = "DealerInventory.findByHaveHighLow", query = "SELECT d FROM DealerInventory d WHERE d.haveHighLow = :haveHighLow")
    , @NamedQuery(name = "DealerInventory.findByHighLowDate", query = "SELECT d FROM DealerInventory d WHERE d.highLowDate = :highLowDate")})
public class DealerInventory implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 8)
    @Column(name = "fid")
    private String fid;
    @Size(max = 20)
    @Column(name = "stock_number")
    private String stockNumber;
    @Size(max = 20)
    @Column(name = "inventory_date")
    private String inventoryDate;
    @Size(max = 10)
    @Column(name = "vehicle_type")
    private String vehicleType;
    @Size(max = 20)
    @Column(name = "vehicle_status")
    private String vehicleStatus;
    @Size(max = 20)
    @Column(name = "invoice_price")
    private String invoicePrice;
    @Size(max = 20)
    @Column(name = "pack_amount")
    private String packAmount;
    @Size(max = 20)
    @Column(name = "cost")
    private String cost;
    @Size(max = 20)
    @Column(name = "holdback_amount")
    private String holdbackAmount;
    @Size(max = 20)
    @Column(name = "list_price")
    private String listPrice;
    @Size(max = 20)
    @Column(name = "msrp")
    private String msrp;
    @Size(max = 20)
    @Column(name = "lot_location")
    private String lotLocation;
    @Size(max = 20)
    @Column(name = "vehicle_condition")
    private String vehicleCondition;
    @Lob
    @Size(max = 65535)
    @Column(name = "tagline")
    private String tagline;
    @Column(name = "is_certified")
    private Character isCertified;
    @Size(max = 20)
    @Column(name = "certification_number")
    private String certificationNumber;
    @Size(max = 17)
    @Column(name = "vin")
    private String vin;
    @Size(max = 30)
    @Column(name = "make")
    private String make;
    @Size(max = 40)
    @Column(name = "model")
    private String model;
    @Size(max = 4)
    @Column(name = "model_year")
    private String modelYear;
    @Size(max = 25)
    @Column(name = "model_code")
    private String modelCode;
    @Size(max = 100)
    @Column(name = "trim_level")
    private String trimLevel;
    @Size(max = 100)
    @Column(name = "sub_trim_level")
    private String subTrimLevel;
    @Size(max = 100)
    @Column(name = "classification")
    private String classification;
    @Size(max = 100)
    @Column(name = "vehicle_type_code")
    private String vehicleTypeCode;
    @Size(max = 100)
    @Column(name = "odometer")
    private String odometer;
    @Size(max = 100)
    @Column(name = "payload_capacity")
    private String payloadCapacity;
    @Size(max = 100)
    @Column(name = "seating_capacity")
    private String seatingCapacity;
    @Size(max = 100)
    @Column(name = "wheel_base")
    private String wheelBase;
    @Size(max = 100)
    @Column(name = "body_description")
    private String bodyDescription;
    @Column(name = "body_door_count")
    private Character bodyDoorCount;
    @Size(max = 100)
    @Column(name = "drive_train_description")
    private String driveTrainDescription;
    @Size(max = 100)
    @Column(name = "engine_description")
    private String engineDescription;
    @Size(max = 100)
    @Column(name = "engine_cylinder_count")
    private String engineCylinderCount;
    @Size(max = 100)
    @Column(name = "transmission_description")
    private String transmissionDescription;
    @Size(max = 100)
    @Column(name = "transmission_type")
    private String transmissionType;
    @Size(max = 100)
    @Column(name = "exterior_color_description")
    private String exteriorColorDescription;
    @Size(max = 100)
    @Column(name = "exterior_color_base_color")
    private String exteriorColorBaseColor;
    @Size(max = 100)
    @Column(name = "interior_description")
    private String interiorDescription;
    @Size(max = 100)
    @Column(name = "interior_color")
    private String interiorColor;
    @Lob
    @Column(name = "standard_features")
    private byte[] standardFeatures;
    @Lob
    @Column(name = "dealer_added_features")
    private byte[] dealerAddedFeatures;
    @Size(max = 20)
    @Column(name = "created_date")
    private String createdDate;
    @Size(max = 20)
    @Column(name = "last_modified_date")
    private String lastModifiedDate;
    @Size(max = 100)
    @Column(name = "modified_flag")
    private String modifiedFlag;
    @Column(name = "number_of_images")
    private Short numberOfImages;
    @Size(max = 254)
    @Column(name = "image_url_pattern")
    private String imageUrlPattern;
    @Size(max = 20)
    @Column(name = "image_set_modification_date")
    private String imageSetModificationDate;
    @Lob
    @Column(name = "stock_image_urls")
    private byte[] stockImageUrls;
    @Column(name = "is_sold")
    private Short isSold;
    @Column(name = "is_dms")
    private Short isDms;
    @Basic(optional = false)
    @NotNull
    @Column(name = "created_on")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdOn;
    @Column(name = "date_sold")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateSold;
    @Basic(optional = false)
    @NotNull
    @Column(name = "last_update")
    @Temporal(TemporalType.TIMESTAMP)
    private Date lastUpdate;
    @Size(max = 20)
    @Column(name = "high_price")
    private String highPrice;
    @Size(max = 20)
    @Column(name = "low_price")
    private String lowPrice;
    @Column(name = "have_high_low")
    private Short haveHighLow;
    @Column(name = "high_low_date")
    @Temporal(TemporalType.DATE)
    private Date highLowDate;

    public DealerInventory() {
    }

    public DealerInventory(Integer id) {
        this.id = id;
    }

    public DealerInventory(Integer id, String fid, Date createdOn, Date lastUpdate) {
        this.id = id;
        this.fid = fid;
        this.createdOn = createdOn;
        this.lastUpdate = lastUpdate;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getFid() {
        return fid;
    }

    public void setFid(String fid) {
        this.fid = fid;
    }

    public String getStockNumber() {
        return stockNumber;
    }

    public void setStockNumber(String stockNumber) {
        this.stockNumber = stockNumber;
    }

    public String getInventoryDate() {
        return inventoryDate;
    }

    public void setInventoryDate(String inventoryDate) {
        this.inventoryDate = inventoryDate;
    }

    public String getVehicleType() {
        return vehicleType;
    }

    public void setVehicleType(String vehicleType) {
        this.vehicleType = vehicleType;
    }

    public String getVehicleStatus() {
        return vehicleStatus;
    }

    public void setVehicleStatus(String vehicleStatus) {
        this.vehicleStatus = vehicleStatus;
    }

    public String getInvoicePrice() {
        return invoicePrice;
    }

    public void setInvoicePrice(String invoicePrice) {
        this.invoicePrice = invoicePrice;
    }

    public String getPackAmount() {
        return packAmount;
    }

    public void setPackAmount(String packAmount) {
        this.packAmount = packAmount;
    }

    public String getCost() {
        return cost;
    }

    public void setCost(String cost) {
        this.cost = cost;
    }

    public String getHoldbackAmount() {
        return holdbackAmount;
    }

    public void setHoldbackAmount(String holdbackAmount) {
        this.holdbackAmount = holdbackAmount;
    }

    public String getListPrice() {
        return listPrice;
    }

    public void setListPrice(String listPrice) {
        this.listPrice = listPrice;
    }

    public String getMsrp() {
        return msrp;
    }

    public void setMsrp(String msrp) {
        this.msrp = msrp;
    }

    public String getLotLocation() {
        return lotLocation;
    }

    public void setLotLocation(String lotLocation) {
        this.lotLocation = lotLocation;
    }

    public String getVehicleCondition() {
        return vehicleCondition;
    }

    public void setVehicleCondition(String vehicleCondition) {
        this.vehicleCondition = vehicleCondition;
    }

    public String getTagline() {
        return tagline;
    }

    public void setTagline(String tagline) {
        this.tagline = tagline;
    }

    public Character getIsCertified() {
        return isCertified;
    }

    public void setIsCertified(Character isCertified) {
        this.isCertified = isCertified;
    }

    public String getCertificationNumber() {
        return certificationNumber;
    }

    public void setCertificationNumber(String certificationNumber) {
        this.certificationNumber = certificationNumber;
    }

    public String getVin() {
        return vin;
    }

    public void setVin(String vin) {
        this.vin = vin;
    }

    public String getMake() {
        return make;
    }

    public void setMake(String make) {
        this.make = make;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getModelYear() {
        return modelYear;
    }

    public void setModelYear(String modelYear) {
        this.modelYear = modelYear;
    }

    public String getModelCode() {
        return modelCode;
    }

    public void setModelCode(String modelCode) {
        this.modelCode = modelCode;
    }

    public String getTrimLevel() {
        return trimLevel;
    }

    public void setTrimLevel(String trimLevel) {
        this.trimLevel = trimLevel;
    }

    public String getSubTrimLevel() {
        return subTrimLevel;
    }

    public void setSubTrimLevel(String subTrimLevel) {
        this.subTrimLevel = subTrimLevel;
    }

    public String getClassification() {
        return classification;
    }

    public void setClassification(String classification) {
        this.classification = classification;
    }

    public String getVehicleTypeCode() {
        return vehicleTypeCode;
    }

    public void setVehicleTypeCode(String vehicleTypeCode) {
        this.vehicleTypeCode = vehicleTypeCode;
    }

    public String getOdometer() {
        return odometer;
    }

    public void setOdometer(String odometer) {
        this.odometer = odometer;
    }

    public String getPayloadCapacity() {
        return payloadCapacity;
    }

    public void setPayloadCapacity(String payloadCapacity) {
        this.payloadCapacity = payloadCapacity;
    }

    public String getSeatingCapacity() {
        return seatingCapacity;
    }

    public void setSeatingCapacity(String seatingCapacity) {
        this.seatingCapacity = seatingCapacity;
    }

    public String getWheelBase() {
        return wheelBase;
    }

    public void setWheelBase(String wheelBase) {
        this.wheelBase = wheelBase;
    }

    public String getBodyDescription() {
        return bodyDescription;
    }

    public void setBodyDescription(String bodyDescription) {
        this.bodyDescription = bodyDescription;
    }

    public Character getBodyDoorCount() {
        return bodyDoorCount;
    }

    public void setBodyDoorCount(Character bodyDoorCount) {
        this.bodyDoorCount = bodyDoorCount;
    }

    public String getDriveTrainDescription() {
        return driveTrainDescription;
    }

    public void setDriveTrainDescription(String driveTrainDescription) {
        this.driveTrainDescription = driveTrainDescription;
    }

    public String getEngineDescription() {
        return engineDescription;
    }

    public void setEngineDescription(String engineDescription) {
        this.engineDescription = engineDescription;
    }

    public String getEngineCylinderCount() {
        return engineCylinderCount;
    }

    public void setEngineCylinderCount(String engineCylinderCount) {
        this.engineCylinderCount = engineCylinderCount;
    }

    public String getTransmissionDescription() {
        return transmissionDescription;
    }

    public void setTransmissionDescription(String transmissionDescription) {
        this.transmissionDescription = transmissionDescription;
    }

    public String getTransmissionType() {
        return transmissionType;
    }

    public void setTransmissionType(String transmissionType) {
        this.transmissionType = transmissionType;
    }

    public String getExteriorColorDescription() {
        return exteriorColorDescription;
    }

    public void setExteriorColorDescription(String exteriorColorDescription) {
        this.exteriorColorDescription = exteriorColorDescription;
    }

    public String getExteriorColorBaseColor() {
        return exteriorColorBaseColor;
    }

    public void setExteriorColorBaseColor(String exteriorColorBaseColor) {
        this.exteriorColorBaseColor = exteriorColorBaseColor;
    }

    public String getInteriorDescription() {
        return interiorDescription;
    }

    public void setInteriorDescription(String interiorDescription) {
        this.interiorDescription = interiorDescription;
    }

    public String getInteriorColor() {
        return interiorColor;
    }

    public void setInteriorColor(String interiorColor) {
        this.interiorColor = interiorColor;
    }

    public byte[] getStandardFeatures() {
        return standardFeatures;
    }

    public void setStandardFeatures(byte[] standardFeatures) {
        this.standardFeatures = standardFeatures;
    }

    public byte[] getDealerAddedFeatures() {
        return dealerAddedFeatures;
    }

    public void setDealerAddedFeatures(byte[] dealerAddedFeatures) {
        this.dealerAddedFeatures = dealerAddedFeatures;
    }

    public String getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(String createdDate) {
        this.createdDate = createdDate;
    }

    public String getLastModifiedDate() {
        return lastModifiedDate;
    }

    public void setLastModifiedDate(String lastModifiedDate) {
        this.lastModifiedDate = lastModifiedDate;
    }

    public String getModifiedFlag() {
        return modifiedFlag;
    }

    public void setModifiedFlag(String modifiedFlag) {
        this.modifiedFlag = modifiedFlag;
    }

    public Short getNumberOfImages() {
        return numberOfImages;
    }

    public void setNumberOfImages(Short numberOfImages) {
        this.numberOfImages = numberOfImages;
    }

    public String getImageUrlPattern() {
        return imageUrlPattern;
    }

    public void setImageUrlPattern(String imageUrlPattern) {
        this.imageUrlPattern = imageUrlPattern;
    }

    public String getImageSetModificationDate() {
        return imageSetModificationDate;
    }

    public void setImageSetModificationDate(String imageSetModificationDate) {
        this.imageSetModificationDate = imageSetModificationDate;
    }

    public byte[] getStockImageUrls() {
        return stockImageUrls;
    }

    public void setStockImageUrls(byte[] stockImageUrls) {
        this.stockImageUrls = stockImageUrls;
    }

    public Short getIsSold() {
        return isSold;
    }

    public void setIsSold(Short isSold) {
        this.isSold = isSold;
    }

    public Short getIsDms() {
        return isDms;
    }

    public void setIsDms(Short isDms) {
        this.isDms = isDms;
    }

    public Date getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(Date createdOn) {
        this.createdOn = createdOn;
    }

    public Date getDateSold() {
        return dateSold;
    }

    public void setDateSold(Date dateSold) {
        this.dateSold = dateSold;
    }

    public Date getLastUpdate() {
        return lastUpdate;
    }

    public void setLastUpdate(Date lastUpdate) {
        this.lastUpdate = lastUpdate;
    }

    public String getHighPrice() {
        return highPrice;
    }

    public void setHighPrice(String highPrice) {
        this.highPrice = highPrice;
    }

    public String getLowPrice() {
        return lowPrice;
    }

    public void setLowPrice(String lowPrice) {
        this.lowPrice = lowPrice;
    }

    public Short getHaveHighLow() {
        return haveHighLow;
    }

    public void setHaveHighLow(Short haveHighLow) {
        this.haveHighLow = haveHighLow;
    }

    public Date getHighLowDate() {
        return highLowDate;
    }

    public void setHighLowDate(Date highLowDate) {
        this.highLowDate = highLowDate;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof DealerInventory)) {
            return false;
        }
        DealerInventory other = (DealerInventory) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.gatehouseauto.datastudio.db.domain.DealerInventory[ id=" + id + " ]";
    }
    
}
