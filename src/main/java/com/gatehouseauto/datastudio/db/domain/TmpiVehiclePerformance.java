/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gatehouseauto.datastudio.db.domain;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author basit
 */
@Entity
@Table(name = "tmpi_vehicle_performance")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "TmpiVehiclePerformance.findAll", query = "SELECT t FROM TmpiVehiclePerformance t")
    , @NamedQuery(name = "TmpiVehiclePerformance.findById", query = "SELECT t FROM TmpiVehiclePerformance t WHERE t.id = :id")
    , @NamedQuery(name = "TmpiVehiclePerformance.findByAccId", query = "SELECT t FROM TmpiVehiclePerformance t WHERE t.accId = :accId")
    , @NamedQuery(name = "TmpiVehiclePerformance.findByLstId", query = "SELECT t FROM TmpiVehiclePerformance t WHERE t.lstId = :lstId")
    , @NamedQuery(name = "TmpiVehiclePerformance.findByCampaignId", query = "SELECT t FROM TmpiVehiclePerformance t WHERE t.campaignId = :campaignId")
    , @NamedQuery(name = "TmpiVehiclePerformance.findByFlightId", query = "SELECT t FROM TmpiVehiclePerformance t WHERE t.flightId = :flightId")
    , @NamedQuery(name = "TmpiVehiclePerformance.findByFileDate", query = "SELECT t FROM TmpiVehiclePerformance t WHERE t.fileDate = :fileDate")
    , @NamedQuery(name = "TmpiVehiclePerformance.findBySource", query = "SELECT t FROM TmpiVehiclePerformance t WHERE t.source = :source")
    , @NamedQuery(name = "TmpiVehiclePerformance.findByType", query = "SELECT t FROM TmpiVehiclePerformance t WHERE t.type = :type")
    , @NamedQuery(name = "TmpiVehiclePerformance.findByTotal", query = "SELECT t FROM TmpiVehiclePerformance t WHERE t.total = :total")
    , @NamedQuery(name = "TmpiVehiclePerformance.findByUpdatedOn", query = "SELECT t FROM TmpiVehiclePerformance t WHERE t.updatedOn = :updatedOn")})
public class TmpiVehiclePerformance implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Size(max = 20)
    @Column(name = "acc_id")
    private String accId;
    @Size(max = 20)
    @Column(name = "lst_id")
    private String lstId;
    @Size(max = 20)
    @Column(name = "campaign_id")
    private String campaignId;
    @Size(max = 20)
    @Column(name = "flight_id")
    private String flightId;
    @Column(name = "file_date")
    @Temporal(TemporalType.DATE)
    private Date fileDate;
    @Size(max = 50)
    @Column(name = "source")
    private String source;
    @Size(max = 50)
    @Column(name = "type")
    private String type;
    @Size(max = 10)
    @Column(name = "total")
    private String total;
    @Column(name = "updated_on")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updatedOn;

    public TmpiVehiclePerformance() {
    }

    public TmpiVehiclePerformance(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getAccId() {
        return accId;
    }

    public void setAccId(String accId) {
        this.accId = accId;
    }

    public String getLstId() {
        return lstId;
    }

    public void setLstId(String lstId) {
        this.lstId = lstId;
    }

    public String getCampaignId() {
        return campaignId;
    }

    public void setCampaignId(String campaignId) {
        this.campaignId = campaignId;
    }

    public String getFlightId() {
        return flightId;
    }

    public void setFlightId(String flightId) {
        this.flightId = flightId;
    }

    public Date getFileDate() {
        return fileDate;
    }

    public void setFileDate(Date fileDate) {
        this.fileDate = fileDate;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getTotal() {
        return total;
    }

    public void setTotal(String total) {
        this.total = total;
    }

    public Date getUpdatedOn() {
        return updatedOn;
    }

    public void setUpdatedOn(Date updatedOn) {
        this.updatedOn = updatedOn;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TmpiVehiclePerformance)) {
            return false;
        }
        TmpiVehiclePerformance other = (TmpiVehiclePerformance) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.gatehouseauto.datastudio.db.domain.TmpiVehiclePerformance[ id=" + id + " ]";
    }
    
}
