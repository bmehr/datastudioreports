/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gatehouseauto.datastudio.db.domain;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author basit
 */
@Entity
@Table(name = "lesa_credentials")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "LesaCredentials.findAll", query = "SELECT l FROM LesaCredentials l")
    , @NamedQuery(name = "LesaCredentials.findById", query = "SELECT l FROM LesaCredentials l WHERE l.id = :id")
    , @NamedQuery(name = "LesaCredentials.findByFid", query = "SELECT l FROM LesaCredentials l WHERE l.fid = :fid")
    , @NamedQuery(name = "LesaCredentials.findByLesaDealerId", query = "SELECT l FROM LesaCredentials l WHERE l.lesaDealerId = :lesaDealerId")
    , @NamedQuery(name = "LesaCredentials.findByUserName", query = "SELECT l FROM LesaCredentials l WHERE l.userName = :userName")
    , @NamedQuery(name = "LesaCredentials.findByPassword", query = "SELECT l FROM LesaCredentials l WHERE l.password = :password")
    , @NamedQuery(name = "LesaCredentials.findByCreatedOn", query = "SELECT l FROM LesaCredentials l WHERE l.createdOn = :createdOn")})
public class LesaCredentials implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 8)
    @Column(name = "fid")
    private String fid;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 8)
    @Column(name = "lesa_dealer_id")
    private String lesaDealerId;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 85)
    @Column(name = "user_name")
    private String userName;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 85)
    @Column(name = "password")
    private String password;
    @Basic(optional = false)
    @NotNull
    @Column(name = "created_on")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdOn;

    public LesaCredentials() {
    }

    public LesaCredentials(Integer id) {
        this.id = id;
    }

    public LesaCredentials(Integer id, String fid, String lesaDealerId, String userName, String password, Date createdOn) {
        this.id = id;
        this.fid = fid;
        this.lesaDealerId = lesaDealerId;
        this.userName = userName;
        this.password = password;
        this.createdOn = createdOn;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getFid() {
        return fid;
    }

    public void setFid(String fid) {
        this.fid = fid;
    }

    public String getLesaDealerId() {
        return lesaDealerId;
    }

    public void setLesaDealerId(String lesaDealerId) {
        this.lesaDealerId = lesaDealerId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Date getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(Date createdOn) {
        this.createdOn = createdOn;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof LesaCredentials)) {
            return false;
        }
        LesaCredentials other = (LesaCredentials) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.gatehouseauto.datastudio.db.domain.LesaCredentials[ id=" + id + " ]";
    }
    
}
