/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gatehouseauto.datastudio.db.domain;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author basit
 */
@Entity
@Table(name = "tmpi_int_fb_stats_monthly")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "TmpiIntFbStatsMonthly.findAll", query = "SELECT t FROM TmpiIntFbStatsMonthly t")
    , @NamedQuery(name = "TmpiIntFbStatsMonthly.findById", query = "SELECT t FROM TmpiIntFbStatsMonthly t WHERE t.id = :id")
    , @NamedQuery(name = "TmpiIntFbStatsMonthly.findByFid", query = "SELECT t FROM TmpiIntFbStatsMonthly t WHERE t.fid = :fid")
    , @NamedQuery(name = "TmpiIntFbStatsMonthly.findByAccId", query = "SELECT t FROM TmpiIntFbStatsMonthly t WHERE t.accId = :accId")
    , @NamedQuery(name = "TmpiIntFbStatsMonthly.findByCampaignId", query = "SELECT t FROM TmpiIntFbStatsMonthly t WHERE t.campaignId = :campaignId")
    , @NamedQuery(name = "TmpiIntFbStatsMonthly.findByStatsMonth", query = "SELECT t FROM TmpiIntFbStatsMonthly t WHERE t.statsMonth = :statsMonth")
    , @NamedQuery(name = "TmpiIntFbStatsMonthly.findByStatus", query = "SELECT t FROM TmpiIntFbStatsMonthly t WHERE t.status = :status")
    , @NamedQuery(name = "TmpiIntFbStatsMonthly.findByStartedOn", query = "SELECT t FROM TmpiIntFbStatsMonthly t WHERE t.startedOn = :startedOn")
    , @NamedQuery(name = "TmpiIntFbStatsMonthly.findByLastRenewOn", query = "SELECT t FROM TmpiIntFbStatsMonthly t WHERE t.lastRenewOn = :lastRenewOn")
    , @NamedQuery(name = "TmpiIntFbStatsMonthly.findByLastRenewEndOn", query = "SELECT t FROM TmpiIntFbStatsMonthly t WHERE t.lastRenewEndOn = :lastRenewEndOn")
    , @NamedQuery(name = "TmpiIntFbStatsMonthly.findByRenewCount", query = "SELECT t FROM TmpiIntFbStatsMonthly t WHERE t.renewCount = :renewCount")
    , @NamedQuery(name = "TmpiIntFbStatsMonthly.findByLanguage", query = "SELECT t FROM TmpiIntFbStatsMonthly t WHERE t.language = :language")
    , @NamedQuery(name = "TmpiIntFbStatsMonthly.findByInventory", query = "SELECT t FROM TmpiIntFbStatsMonthly t WHERE t.inventory = :inventory")
    , @NamedQuery(name = "TmpiIntFbStatsMonthly.findByCost", query = "SELECT t FROM TmpiIntFbStatsMonthly t WHERE t.cost = :cost")
    , @NamedQuery(name = "TmpiIntFbStatsMonthly.findByManualTargetMonthly", query = "SELECT t FROM TmpiIntFbStatsMonthly t WHERE t.manualTargetMonthly = :manualTargetMonthly")
    , @NamedQuery(name = "TmpiIntFbStatsMonthly.findByVdpsTarget", query = "SELECT t FROM TmpiIntFbStatsMonthly t WHERE t.vdpsTarget = :vdpsTarget")
    , @NamedQuery(name = "TmpiIntFbStatsMonthly.findByImpressionsTarget", query = "SELECT t FROM TmpiIntFbStatsMonthly t WHERE t.impressionsTarget = :impressionsTarget")
    , @NamedQuery(name = "TmpiIntFbStatsMonthly.findByClicksTarget", query = "SELECT t FROM TmpiIntFbStatsMonthly t WHERE t.clicksTarget = :clicksTarget")
    , @NamedQuery(name = "TmpiIntFbStatsMonthly.findByVdpsMonthly", query = "SELECT t FROM TmpiIntFbStatsMonthly t WHERE t.vdpsMonthly = :vdpsMonthly")
    , @NamedQuery(name = "TmpiIntFbStatsMonthly.findByImpressionsMonthly", query = "SELECT t FROM TmpiIntFbStatsMonthly t WHERE t.impressionsMonthly = :impressionsMonthly")
    , @NamedQuery(name = "TmpiIntFbStatsMonthly.findBySerpsMonthly", query = "SELECT t FROM TmpiIntFbStatsMonthly t WHERE t.serpsMonthly = :serpsMonthly")
    , @NamedQuery(name = "TmpiIntFbStatsMonthly.findByUpdatedOn", query = "SELECT t FROM TmpiIntFbStatsMonthly t WHERE t.updatedOn = :updatedOn")})
public class TmpiIntFbStatsMonthly implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 8)
    @Column(name = "fid")
    private String fid;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 20)
    @Column(name = "acc_id")
    private String accId;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 20)
    @Column(name = "campaign_id")
    private String campaignId;
    @Basic(optional = false)
    @NotNull
    @Column(name = "stats_month")
    @Temporal(TemporalType.DATE)
    private Date statsMonth;
    @Size(max = 20)
    @Column(name = "status")
    private String status;
    @Column(name = "started_on")
    @Temporal(TemporalType.DATE)
    private Date startedOn;
    @Column(name = "last_renew_on")
    @Temporal(TemporalType.DATE)
    private Date lastRenewOn;
    @Column(name = "last_renew_end_on")
    @Temporal(TemporalType.DATE)
    private Date lastRenewEndOn;
    @Basic(optional = false)
    @NotNull
    @Column(name = "renew_count")
    private int renewCount;
    @Size(max = 75)
    @Column(name = "language")
    private String language;
    @Size(max = 25)
    @Column(name = "inventory")
    private String inventory;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "cost")
    private Float cost;
    @Column(name = "manual_target_monthly")
    private Integer manualTargetMonthly;
    @Column(name = "vdps_target")
    private Integer vdpsTarget;
    @Column(name = "impressions_target")
    private Integer impressionsTarget;
    @Column(name = "clicks_target")
    private Integer clicksTarget;
    @Column(name = "vdps_monthly")
    private Integer vdpsMonthly;
    @Column(name = "impressions_monthly")
    private Integer impressionsMonthly;
    @Column(name = "serps_monthly")
    private Integer serpsMonthly;
    @Basic(optional = false)
    @NotNull
    @Column(name = "updated_on")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updatedOn;

    public TmpiIntFbStatsMonthly() {
    }

    public TmpiIntFbStatsMonthly(Integer id) {
        this.id = id;
    }

    public TmpiIntFbStatsMonthly(Integer id, String fid, String accId, String campaignId, Date statsMonth, int renewCount, Date updatedOn) {
        this.id = id;
        this.fid = fid;
        this.accId = accId;
        this.campaignId = campaignId;
        this.statsMonth = statsMonth;
        this.renewCount = renewCount;
        this.updatedOn = updatedOn;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getFid() {
        return fid;
    }

    public void setFid(String fid) {
        this.fid = fid;
    }

    public String getAccId() {
        return accId;
    }

    public void setAccId(String accId) {
        this.accId = accId;
    }

    public String getCampaignId() {
        return campaignId;
    }

    public void setCampaignId(String campaignId) {
        this.campaignId = campaignId;
    }

    public Date getStatsMonth() {
        return statsMonth;
    }

    public void setStatsMonth(Date statsMonth) {
        this.statsMonth = statsMonth;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Date getStartedOn() {
        return startedOn;
    }

    public void setStartedOn(Date startedOn) {
        this.startedOn = startedOn;
    }

    public Date getLastRenewOn() {
        return lastRenewOn;
    }

    public void setLastRenewOn(Date lastRenewOn) {
        this.lastRenewOn = lastRenewOn;
    }

    public Date getLastRenewEndOn() {
        return lastRenewEndOn;
    }

    public void setLastRenewEndOn(Date lastRenewEndOn) {
        this.lastRenewEndOn = lastRenewEndOn;
    }

    public int getRenewCount() {
        return renewCount;
    }

    public void setRenewCount(int renewCount) {
        this.renewCount = renewCount;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public String getInventory() {
        return inventory;
    }

    public void setInventory(String inventory) {
        this.inventory = inventory;
    }

    public Float getCost() {
        return cost;
    }

    public void setCost(Float cost) {
        this.cost = cost;
    }

    public Integer getManualTargetMonthly() {
        return manualTargetMonthly;
    }

    public void setManualTargetMonthly(Integer manualTargetMonthly) {
        this.manualTargetMonthly = manualTargetMonthly;
    }

    public Integer getVdpsTarget() {
        return vdpsTarget;
    }

    public void setVdpsTarget(Integer vdpsTarget) {
        this.vdpsTarget = vdpsTarget;
    }

    public Integer getImpressionsTarget() {
        return impressionsTarget;
    }

    public void setImpressionsTarget(Integer impressionsTarget) {
        this.impressionsTarget = impressionsTarget;
    }

    public Integer getClicksTarget() {
        return clicksTarget;
    }

    public void setClicksTarget(Integer clicksTarget) {
        this.clicksTarget = clicksTarget;
    }

    public Integer getVdpsMonthly() {
        return vdpsMonthly;
    }

    public void setVdpsMonthly(Integer vdpsMonthly) {
        this.vdpsMonthly = vdpsMonthly;
    }

    public Integer getImpressionsMonthly() {
        return impressionsMonthly;
    }

    public void setImpressionsMonthly(Integer impressionsMonthly) {
        this.impressionsMonthly = impressionsMonthly;
    }

    public Integer getSerpsMonthly() {
        return serpsMonthly;
    }

    public void setSerpsMonthly(Integer serpsMonthly) {
        this.serpsMonthly = serpsMonthly;
    }

    public Date getUpdatedOn() {
        return updatedOn;
    }

    public void setUpdatedOn(Date updatedOn) {
        this.updatedOn = updatedOn;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TmpiIntFbStatsMonthly)) {
            return false;
        }
        TmpiIntFbStatsMonthly other = (TmpiIntFbStatsMonthly) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.gatehouseauto.datastudio.db.domain.TmpiIntFbStatsMonthly[ id=" + id + " ]";
    }
    
}
