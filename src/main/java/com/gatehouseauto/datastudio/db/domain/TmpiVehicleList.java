/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gatehouseauto.datastudio.db.domain;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author basit
 */
@Entity
@Table(name = "tmpi_vehicle_list")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "TmpiVehicleList.findAll", query = "SELECT t FROM TmpiVehicleList t")
    , @NamedQuery(name = "TmpiVehicleList.findById", query = "SELECT t FROM TmpiVehicleList t WHERE t.id = :id")
    , @NamedQuery(name = "TmpiVehicleList.findByAccId", query = "SELECT t FROM TmpiVehicleList t WHERE t.accId = :accId")
    , @NamedQuery(name = "TmpiVehicleList.findByLstId", query = "SELECT t FROM TmpiVehicleList t WHERE t.lstId = :lstId")
    , @NamedQuery(name = "TmpiVehicleList.findByVin", query = "SELECT t FROM TmpiVehicleList t WHERE t.vin = :vin")
    , @NamedQuery(name = "TmpiVehicleList.findByCapturedDate", query = "SELECT t FROM TmpiVehicleList t WHERE t.capturedDate = :capturedDate")
    , @NamedQuery(name = "TmpiVehicleList.findByYear", query = "SELECT t FROM TmpiVehicleList t WHERE t.year = :year")
    , @NamedQuery(name = "TmpiVehicleList.findByMake", query = "SELECT t FROM TmpiVehicleList t WHERE t.make = :make")
    , @NamedQuery(name = "TmpiVehicleList.findByModel", query = "SELECT t FROM TmpiVehicleList t WHERE t.model = :model")
    , @NamedQuery(name = "TmpiVehicleList.findByTrim", query = "SELECT t FROM TmpiVehicleList t WHERE t.trim = :trim")
    , @NamedQuery(name = "TmpiVehicleList.findByPrice", query = "SELECT t FROM TmpiVehicleList t WHERE t.price = :price")
    , @NamedQuery(name = "TmpiVehicleList.findByPriceRange", query = "SELECT t FROM TmpiVehicleList t WHERE t.priceRange = :priceRange")
    , @NamedQuery(name = "TmpiVehicleList.findByCreated", query = "SELECT t FROM TmpiVehicleList t WHERE t.created = :created")
    , @NamedQuery(name = "TmpiVehicleList.findByLastUpload", query = "SELECT t FROM TmpiVehicleList t WHERE t.lastUpload = :lastUpload")
    , @NamedQuery(name = "TmpiVehicleList.findByStatus", query = "SELECT t FROM TmpiVehicleList t WHERE t.status = :status")
    , @NamedQuery(name = "TmpiVehicleList.findByUpdatedOn", query = "SELECT t FROM TmpiVehicleList t WHERE t.updatedOn = :updatedOn")})
public class TmpiVehicleList implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Size(max = 20)
    @Column(name = "acc_id")
    private String accId;
    @Size(max = 20)
    @Column(name = "lst_id")
    private String lstId;
    @Size(max = 20)
    @Column(name = "vin")
    private String vin;
    @Column(name = "captured_date")
    @Temporal(TemporalType.DATE)
    private Date capturedDate;
    @Size(max = 10)
    @Column(name = "year")
    private String year;
    @Size(max = 25)
    @Column(name = "make")
    private String make;
    @Size(max = 100)
    @Column(name = "model")
    private String model;
    @Size(max = 100)
    @Column(name = "trim")
    private String trim;
    @Size(max = 35)
    @Column(name = "price")
    private String price;
    @Size(max = 20)
    @Column(name = "price_range")
    private String priceRange;
    @Column(name = "created")
    @Temporal(TemporalType.DATE)
    private Date created;
    @Column(name = "last_upload")
    @Temporal(TemporalType.DATE)
    private Date lastUpload;
    @Size(max = 20)
    @Column(name = "status")
    private String status;
    @Column(name = "updated_on")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updatedOn;

    public TmpiVehicleList() {
    }

    public TmpiVehicleList(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getAccId() {
        return accId;
    }

    public void setAccId(String accId) {
        this.accId = accId;
    }

    public String getLstId() {
        return lstId;
    }

    public void setLstId(String lstId) {
        this.lstId = lstId;
    }

    public String getVin() {
        return vin;
    }

    public void setVin(String vin) {
        this.vin = vin;
    }

    public Date getCapturedDate() {
        return capturedDate;
    }

    public void setCapturedDate(Date capturedDate) {
        this.capturedDate = capturedDate;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public String getMake() {
        return make;
    }

    public void setMake(String make) {
        this.make = make;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getTrim() {
        return trim;
    }

    public void setTrim(String trim) {
        this.trim = trim;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getPriceRange() {
        return priceRange;
    }

    public void setPriceRange(String priceRange) {
        this.priceRange = priceRange;
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    public Date getLastUpload() {
        return lastUpload;
    }

    public void setLastUpload(Date lastUpload) {
        this.lastUpload = lastUpload;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Date getUpdatedOn() {
        return updatedOn;
    }

    public void setUpdatedOn(Date updatedOn) {
        this.updatedOn = updatedOn;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TmpiVehicleList)) {
            return false;
        }
        TmpiVehicleList other = (TmpiVehicleList) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.gatehouseauto.datastudio.db.domain.TmpiVehicleList[ id=" + id + " ]";
    }
    
}
