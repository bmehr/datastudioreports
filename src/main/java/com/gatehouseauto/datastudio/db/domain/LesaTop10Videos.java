/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gatehouseauto.datastudio.db.domain;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author basit
 */
@Entity
@Table(name = "lesa_top_10_videos")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "LesaTop10Videos.findAll", query = "SELECT l FROM LesaTop10Videos l")
    , @NamedQuery(name = "LesaTop10Videos.findById", query = "SELECT l FROM LesaTop10Videos l WHERE l.id = :id")
    , @NamedQuery(name = "LesaTop10Videos.findByFid", query = "SELECT l FROM LesaTop10Videos l WHERE l.fid = :fid")
    , @NamedQuery(name = "LesaTop10Videos.findByReportMonth", query = "SELECT l FROM LesaTop10Videos l WHERE l.reportMonth = :reportMonth")
    , @NamedQuery(name = "LesaTop10Videos.findByVideo", query = "SELECT l FROM LesaTop10Videos l WHERE l.video = :video")
    , @NamedQuery(name = "LesaTop10Videos.findByWatchTimeMinutes", query = "SELECT l FROM LesaTop10Videos l WHERE l.watchTimeMinutes = :watchTimeMinutes")
    , @NamedQuery(name = "LesaTop10Videos.findByViews", query = "SELECT l FROM LesaTop10Videos l WHERE l.views = :views")})
public class LesaTop10Videos implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 8)
    @Column(name = "fid")
    private String fid;
    @Basic(optional = false)
    @NotNull
    @Column(name = "report_month")
    @Temporal(TemporalType.DATE)
    private Date reportMonth;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 450)
    @Column(name = "video")
    private String video;
    @Basic(optional = false)
    @NotNull
    @Column(name = "watch_time_minutes")
    private int watchTimeMinutes;
    @Basic(optional = false)
    @NotNull
    @Column(name = "views")
    private int views;

    public LesaTop10Videos() {
    }

    public LesaTop10Videos(Integer id) {
        this.id = id;
    }

    public LesaTop10Videos(Integer id, String fid, Date reportMonth, String video, int watchTimeMinutes, int views) {
        this.id = id;
        this.fid = fid;
        this.reportMonth = reportMonth;
        this.video = video;
        this.watchTimeMinutes = watchTimeMinutes;
        this.views = views;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getFid() {
        return fid;
    }

    public void setFid(String fid) {
        this.fid = fid;
    }

    public Date getReportMonth() {
        return reportMonth;
    }

    public void setReportMonth(Date reportMonth) {
        this.reportMonth = reportMonth;
    }

    public String getVideo() {
        return video;
    }

    public void setVideo(String video) {
        this.video = video;
    }

    public int getWatchTimeMinutes() {
        return watchTimeMinutes;
    }

    public void setWatchTimeMinutes(int watchTimeMinutes) {
        this.watchTimeMinutes = watchTimeMinutes;
    }

    public int getViews() {
        return views;
    }

    public void setViews(int views) {
        this.views = views;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof LesaTop10Videos)) {
            return false;
        }
        LesaTop10Videos other = (LesaTop10Videos) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.gatehouseauto.datastudio.db.domain.LesaTop10Videos[ id=" + id + " ]";
    }
    
}
