/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gatehouseauto.datastudio.db.domain;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author basit
 */
@Entity
@Table(name = "dealer_information")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "DealerInformation.findAll", query = "SELECT d FROM DealerInformation d")
    , @NamedQuery(name = "DealerInformation.findById", query = "SELECT d FROM DealerInformation d WHERE d.id = :id")
    , @NamedQuery(name = "DealerInformation.findByFid", query = "SELECT d FROM DealerInformation d WHERE d.fid = :fid")
    , @NamedQuery(name = "DealerInformation.findByDealerName", query = "SELECT d FROM DealerInformation d WHERE d.dealerName = :dealerName")
    , @NamedQuery(name = "DealerInformation.findByIsGuaranteedLeads", query = "SELECT d FROM DealerInformation d WHERE d.isGuaranteedLeads = :isGuaranteedLeads")
    , @NamedQuery(name = "DealerInformation.findByUpdatedOn", query = "SELECT d FROM DealerInformation d WHERE d.updatedOn = :updatedOn")})
public class DealerInformation implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 8)
    @Column(name = "fid")
    private String fid;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 254)
    @Column(name = "dealer_name")
    private String dealerName;
    @Basic(optional = false)
    @NotNull
    @Column(name = "is_guaranteed_leads")
    private boolean isGuaranteedLeads;
    @Basic(optional = false)
    @NotNull
    @Column(name = "updated_on")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updatedOn;

    public DealerInformation() {
    }

    public DealerInformation(Integer id) {
        this.id = id;
    }

    public DealerInformation(Integer id, String fid, String dealerName, boolean isGuaranteedLeads, Date updatedOn) {
        this.id = id;
        this.fid = fid;
        this.dealerName = dealerName;
        this.isGuaranteedLeads = isGuaranteedLeads;
        this.updatedOn = updatedOn;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getFid() {
        return fid;
    }

    public void setFid(String fid) {
        this.fid = fid;
    }

    public String getDealerName() {
        return dealerName;
    }

    public void setDealerName(String dealerName) {
        this.dealerName = dealerName;
    }

    public boolean getIsGuaranteedLeads() {
        return isGuaranteedLeads;
    }

    public void setIsGuaranteedLeads(boolean isGuaranteedLeads) {
        this.isGuaranteedLeads = isGuaranteedLeads;
    }

    public Date getUpdatedOn() {
        return updatedOn;
    }

    public void setUpdatedOn(Date updatedOn) {
        this.updatedOn = updatedOn;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof DealerInformation)) {
            return false;
        }
        DealerInformation other = (DealerInformation) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.gatehouseauto.datastudio.db.domain.DealerInformation[ id=" + id + " ]";
    }
    
}
