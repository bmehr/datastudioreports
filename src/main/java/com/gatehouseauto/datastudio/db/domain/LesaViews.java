/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gatehouseauto.datastudio.db.domain;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author basit
 */
@Entity
@Table(name = "lesa_views")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "LesaViews.findAll", query = "SELECT l FROM LesaViews l")
    , @NamedQuery(name = "LesaViews.findById", query = "SELECT l FROM LesaViews l WHERE l.id = :id")
    , @NamedQuery(name = "LesaViews.findByFid", query = "SELECT l FROM LesaViews l WHERE l.fid = :fid")
    , @NamedQuery(name = "LesaViews.findByReportMonth", query = "SELECT l FROM LesaViews l WHERE l.reportMonth = :reportMonth")
    , @NamedQuery(name = "LesaViews.findByViewType", query = "SELECT l FROM LesaViews l WHERE l.viewType = :viewType")
    , @NamedQuery(name = "LesaViews.findByColumnName", query = "SELECT l FROM LesaViews l WHERE l.columnName = :columnName")
    , @NamedQuery(name = "LesaViews.findByCount", query = "SELECT l FROM LesaViews l WHERE l.count = :count")})
public class LesaViews implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 8)
    @Column(name = "fid")
    private String fid;
    @Basic(optional = false)
    @NotNull
    @Column(name = "report_month")
    @Temporal(TemporalType.DATE)
    private Date reportMonth;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 85)
    @Column(name = "view_type")
    private String viewType;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 400)
    @Column(name = "column_name")
    private String columnName;
    @Basic(optional = false)
    @NotNull
    @Column(name = "count")
    private int count;

    public LesaViews() {
    }

    public LesaViews(Integer id) {
        this.id = id;
    }

    public LesaViews(Integer id, String fid, Date reportMonth, String viewType, String columnName, int count) {
        this.id = id;
        this.fid = fid;
        this.reportMonth = reportMonth;
        this.viewType = viewType;
        this.columnName = columnName;
        this.count = count;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getFid() {
        return fid;
    }

    public void setFid(String fid) {
        this.fid = fid;
    }

    public Date getReportMonth() {
        return reportMonth;
    }

    public void setReportMonth(Date reportMonth) {
        this.reportMonth = reportMonth;
    }

    public String getViewType() {
        return viewType;
    }

    public void setViewType(String viewType) {
        this.viewType = viewType;
    }

    public String getColumnName() {
        return columnName;
    }

    public void setColumnName(String columnName) {
        this.columnName = columnName;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof LesaViews)) {
            return false;
        }
        LesaViews other = (LesaViews) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.gatehouseauto.datastudio.db.domain.LesaViews[ id=" + id + " ]";
    }
    
}
