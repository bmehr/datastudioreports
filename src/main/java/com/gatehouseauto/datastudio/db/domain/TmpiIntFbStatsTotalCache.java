/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gatehouseauto.datastudio.db.domain;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author basit
 */
@Entity
@Table(name = "tmpi_int_fb_stats_total_cache")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "TmpiIntFbStatsTotalCache.findAll", query = "SELECT t FROM TmpiIntFbStatsTotalCache t")
    , @NamedQuery(name = "TmpiIntFbStatsTotalCache.findById", query = "SELECT t FROM TmpiIntFbStatsTotalCache t WHERE t.id = :id")
    , @NamedQuery(name = "TmpiIntFbStatsTotalCache.findByFid", query = "SELECT t FROM TmpiIntFbStatsTotalCache t WHERE t.fid = :fid")
    , @NamedQuery(name = "TmpiIntFbStatsTotalCache.findByAccId", query = "SELECT t FROM TmpiIntFbStatsTotalCache t WHERE t.accId = :accId")
    , @NamedQuery(name = "TmpiIntFbStatsTotalCache.findByCampaignId", query = "SELECT t FROM TmpiIntFbStatsTotalCache t WHERE t.campaignId = :campaignId")
    , @NamedQuery(name = "TmpiIntFbStatsTotalCache.findByStatus", query = "SELECT t FROM TmpiIntFbStatsTotalCache t WHERE t.status = :status")
    , @NamedQuery(name = "TmpiIntFbStatsTotalCache.findByStartedOn", query = "SELECT t FROM TmpiIntFbStatsTotalCache t WHERE t.startedOn = :startedOn")
    , @NamedQuery(name = "TmpiIntFbStatsTotalCache.findByLastRenewOn", query = "SELECT t FROM TmpiIntFbStatsTotalCache t WHERE t.lastRenewOn = :lastRenewOn")
    , @NamedQuery(name = "TmpiIntFbStatsTotalCache.findByLastRenewEndOn", query = "SELECT t FROM TmpiIntFbStatsTotalCache t WHERE t.lastRenewEndOn = :lastRenewEndOn")
    , @NamedQuery(name = "TmpiIntFbStatsTotalCache.findByRenewCount", query = "SELECT t FROM TmpiIntFbStatsTotalCache t WHERE t.renewCount = :renewCount")
    , @NamedQuery(name = "TmpiIntFbStatsTotalCache.findByLanguage", query = "SELECT t FROM TmpiIntFbStatsTotalCache t WHERE t.language = :language")
    , @NamedQuery(name = "TmpiIntFbStatsTotalCache.findByInventory", query = "SELECT t FROM TmpiIntFbStatsTotalCache t WHERE t.inventory = :inventory")
    , @NamedQuery(name = "TmpiIntFbStatsTotalCache.findByCost", query = "SELECT t FROM TmpiIntFbStatsTotalCache t WHERE t.cost = :cost")
    , @NamedQuery(name = "TmpiIntFbStatsTotalCache.findByManualTargetMonthly", query = "SELECT t FROM TmpiIntFbStatsTotalCache t WHERE t.manualTargetMonthly = :manualTargetMonthly")
    , @NamedQuery(name = "TmpiIntFbStatsTotalCache.findByVdpsTarget", query = "SELECT t FROM TmpiIntFbStatsTotalCache t WHERE t.vdpsTarget = :vdpsTarget")
    , @NamedQuery(name = "TmpiIntFbStatsTotalCache.findByImpressionsTarget", query = "SELECT t FROM TmpiIntFbStatsTotalCache t WHERE t.impressionsTarget = :impressionsTarget")
    , @NamedQuery(name = "TmpiIntFbStatsTotalCache.findByClicksTarget", query = "SELECT t FROM TmpiIntFbStatsTotalCache t WHERE t.clicksTarget = :clicksTarget")
    , @NamedQuery(name = "TmpiIntFbStatsTotalCache.findByVdpsTotal", query = "SELECT t FROM TmpiIntFbStatsTotalCache t WHERE t.vdpsTotal = :vdpsTotal")
    , @NamedQuery(name = "TmpiIntFbStatsTotalCache.findByImpressionsTotal", query = "SELECT t FROM TmpiIntFbStatsTotalCache t WHERE t.impressionsTotal = :impressionsTotal")
    , @NamedQuery(name = "TmpiIntFbStatsTotalCache.findBySerpsTotal", query = "SELECT t FROM TmpiIntFbStatsTotalCache t WHERE t.serpsTotal = :serpsTotal")
    , @NamedQuery(name = "TmpiIntFbStatsTotalCache.findByUpdatedOn", query = "SELECT t FROM TmpiIntFbStatsTotalCache t WHERE t.updatedOn = :updatedOn")})
public class TmpiIntFbStatsTotalCache implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 8)
    @Column(name = "fid")
    private String fid;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 20)
    @Column(name = "acc_id")
    private String accId;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 20)
    @Column(name = "campaign_id")
    private String campaignId;
    @Size(max = 20)
    @Column(name = "status")
    private String status;
    @Column(name = "started_on")
    @Temporal(TemporalType.DATE)
    private Date startedOn;
    @Column(name = "last_renew_on")
    @Temporal(TemporalType.DATE)
    private Date lastRenewOn;
    @Column(name = "last_renew_end_on")
    @Temporal(TemporalType.DATE)
    private Date lastRenewEndOn;
    @Basic(optional = false)
    @NotNull
    @Column(name = "renew_count")
    private int renewCount;
    @Size(max = 75)
    @Column(name = "language")
    private String language;
    @Size(max = 25)
    @Column(name = "inventory")
    private String inventory;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "cost")
    private Float cost;
    @Column(name = "manual_target_monthly")
    private Integer manualTargetMonthly;
    @Column(name = "vdps_target")
    private Integer vdpsTarget;
    @Column(name = "impressions_target")
    private Integer impressionsTarget;
    @Column(name = "clicks_target")
    private Integer clicksTarget;
    @Column(name = "vdps_total")
    private Integer vdpsTotal;
    @Column(name = "impressions_total")
    private Integer impressionsTotal;
    @Column(name = "serps_total")
    private Integer serpsTotal;
    @Basic(optional = false)
    @NotNull
    @Column(name = "updated_on")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updatedOn;

    public TmpiIntFbStatsTotalCache() {
    }

    public TmpiIntFbStatsTotalCache(Integer id) {
        this.id = id;
    }

    public TmpiIntFbStatsTotalCache(Integer id, String fid, String accId, String campaignId, int renewCount, Date updatedOn) {
        this.id = id;
        this.fid = fid;
        this.accId = accId;
        this.campaignId = campaignId;
        this.renewCount = renewCount;
        this.updatedOn = updatedOn;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getFid() {
        return fid;
    }

    public void setFid(String fid) {
        this.fid = fid;
    }

    public String getAccId() {
        return accId;
    }

    public void setAccId(String accId) {
        this.accId = accId;
    }

    public String getCampaignId() {
        return campaignId;
    }

    public void setCampaignId(String campaignId) {
        this.campaignId = campaignId;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Date getStartedOn() {
        return startedOn;
    }

    public void setStartedOn(Date startedOn) {
        this.startedOn = startedOn;
    }

    public Date getLastRenewOn() {
        return lastRenewOn;
    }

    public void setLastRenewOn(Date lastRenewOn) {
        this.lastRenewOn = lastRenewOn;
    }

    public Date getLastRenewEndOn() {
        return lastRenewEndOn;
    }

    public void setLastRenewEndOn(Date lastRenewEndOn) {
        this.lastRenewEndOn = lastRenewEndOn;
    }

    public int getRenewCount() {
        return renewCount;
    }

    public void setRenewCount(int renewCount) {
        this.renewCount = renewCount;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public String getInventory() {
        return inventory;
    }

    public void setInventory(String inventory) {
        this.inventory = inventory;
    }

    public Float getCost() {
        return cost;
    }

    public void setCost(Float cost) {
        this.cost = cost;
    }

    public Integer getManualTargetMonthly() {
        return manualTargetMonthly;
    }

    public void setManualTargetMonthly(Integer manualTargetMonthly) {
        this.manualTargetMonthly = manualTargetMonthly;
    }

    public Integer getVdpsTarget() {
        return vdpsTarget;
    }

    public void setVdpsTarget(Integer vdpsTarget) {
        this.vdpsTarget = vdpsTarget;
    }

    public Integer getImpressionsTarget() {
        return impressionsTarget;
    }

    public void setImpressionsTarget(Integer impressionsTarget) {
        this.impressionsTarget = impressionsTarget;
    }

    public Integer getClicksTarget() {
        return clicksTarget;
    }

    public void setClicksTarget(Integer clicksTarget) {
        this.clicksTarget = clicksTarget;
    }

    public Integer getVdpsTotal() {
        return vdpsTotal;
    }

    public void setVdpsTotal(Integer vdpsTotal) {
        this.vdpsTotal = vdpsTotal;
    }

    public Integer getImpressionsTotal() {
        return impressionsTotal;
    }

    public void setImpressionsTotal(Integer impressionsTotal) {
        this.impressionsTotal = impressionsTotal;
    }

    public Integer getSerpsTotal() {
        return serpsTotal;
    }

    public void setSerpsTotal(Integer serpsTotal) {
        this.serpsTotal = serpsTotal;
    }

    public Date getUpdatedOn() {
        return updatedOn;
    }

    public void setUpdatedOn(Date updatedOn) {
        this.updatedOn = updatedOn;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TmpiIntFbStatsTotalCache)) {
            return false;
        }
        TmpiIntFbStatsTotalCache other = (TmpiIntFbStatsTotalCache) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.gatehouseauto.datastudio.db.domain.TmpiIntFbStatsTotalCache[ id=" + id + " ]";
    }
    
}
