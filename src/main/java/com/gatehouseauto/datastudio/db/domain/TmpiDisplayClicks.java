/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gatehouseauto.datastudio.db.domain;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author basit
 */
@Entity
@Table(name = "tmpi_display_clicks")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "TmpiDisplayClicks.findAll", query = "SELECT t FROM TmpiDisplayClicks t")
    , @NamedQuery(name = "TmpiDisplayClicks.findById", query = "SELECT t FROM TmpiDisplayClicks t WHERE t.id = :id")
    , @NamedQuery(name = "TmpiDisplayClicks.findByAccId", query = "SELECT t FROM TmpiDisplayClicks t WHERE t.accId = :accId")
    , @NamedQuery(name = "TmpiDisplayClicks.findByCampaignId", query = "SELECT t FROM TmpiDisplayClicks t WHERE t.campaignId = :campaignId")
    , @NamedQuery(name = "TmpiDisplayClicks.findByFid", query = "SELECT t FROM TmpiDisplayClicks t WHERE t.fid = :fid")
    , @NamedQuery(name = "TmpiDisplayClicks.findByCampaignName", query = "SELECT t FROM TmpiDisplayClicks t WHERE t.campaignName = :campaignName")
    , @NamedQuery(name = "TmpiDisplayClicks.findByCapturedDate", query = "SELECT t FROM TmpiDisplayClicks t WHERE t.capturedDate = :capturedDate")
    , @NamedQuery(name = "TmpiDisplayClicks.findByClicks", query = "SELECT t FROM TmpiDisplayClicks t WHERE t.clicks = :clicks")
    , @NamedQuery(name = "TmpiDisplayClicks.findByImpressions", query = "SELECT t FROM TmpiDisplayClicks t WHERE t.impressions = :impressions")
    , @NamedQuery(name = "TmpiDisplayClicks.findByIsDynamic", query = "SELECT t FROM TmpiDisplayClicks t WHERE t.isDynamic = :isDynamic")
    , @NamedQuery(name = "TmpiDisplayClicks.findByIsSocial", query = "SELECT t FROM TmpiDisplayClicks t WHERE t.isSocial = :isSocial")
    , @NamedQuery(name = "TmpiDisplayClicks.findByUpdatedOn", query = "SELECT t FROM TmpiDisplayClicks t WHERE t.updatedOn = :updatedOn")})
public class TmpiDisplayClicks implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Column(name = "acc_id")
    private Integer accId;
    @Size(max = 20)
    @Column(name = "campaign_id")
    private String campaignId;
    @Size(max = 10)
    @Column(name = "fid")
    private String fid;
    @Size(max = 255)
    @Column(name = "campaign_name")
    private String campaignName;
    @Size(max = 20)
    @Column(name = "captured_date")
    private String capturedDate;
    @Column(name = "clicks")
    private Integer clicks;
    @Column(name = "impressions")
    private Integer impressions;
    @Column(name = "is_dynamic")
    private Integer isDynamic;
    @Column(name = "is_social")
    private Integer isSocial;
    @Column(name = "updated_on")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updatedOn;

    public TmpiDisplayClicks() {
    }

    public TmpiDisplayClicks(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getAccId() {
        return accId;
    }

    public void setAccId(Integer accId) {
        this.accId = accId;
    }

    public String getCampaignId() {
        return campaignId;
    }

    public void setCampaignId(String campaignId) {
        this.campaignId = campaignId;
    }

    public String getFid() {
        return fid;
    }

    public void setFid(String fid) {
        this.fid = fid;
    }

    public String getCampaignName() {
        return campaignName;
    }

    public void setCampaignName(String campaignName) {
        this.campaignName = campaignName;
    }

    public String getCapturedDate() {
        return capturedDate;
    }

    public void setCapturedDate(String capturedDate) {
        this.capturedDate = capturedDate;
    }

    public Integer getClicks() {
        return clicks;
    }

    public void setClicks(Integer clicks) {
        this.clicks = clicks;
    }

    public Integer getImpressions() {
        return impressions;
    }

    public void setImpressions(Integer impressions) {
        this.impressions = impressions;
    }

    public Integer getIsDynamic() {
        return isDynamic;
    }

    public void setIsDynamic(Integer isDynamic) {
        this.isDynamic = isDynamic;
    }

    public Integer getIsSocial() {
        return isSocial;
    }

    public void setIsSocial(Integer isSocial) {
        this.isSocial = isSocial;
    }

    public Date getUpdatedOn() {
        return updatedOn;
    }

    public void setUpdatedOn(Date updatedOn) {
        this.updatedOn = updatedOn;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TmpiDisplayClicks)) {
            return false;
        }
        TmpiDisplayClicks other = (TmpiDisplayClicks) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.gatehouseauto.datastudio.db.domain.TmpiDisplayClicks[ id=" + id + " ]";
    }
    
}
