package com.gatehouseauto.datastudio.db.repo;

import java.math.BigInteger;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.gatehouseauto.datastudio.db.domain.VehicleCount;

@Repository
public class CustomRepository {

	@PersistenceContext
    private EntityManager entityManager; 
	
	@Transactional
	public int truncateDealerInventoryToday(){
		Query queryTruncate = entityManager.createNativeQuery(
				"truncate dealer_inventory_today" );
		return queryTruncate.executeUpdate();
	}
    
	@SuppressWarnings({ "unchecked" })
	@Transactional
	public List<VehicleCount> getDealerInventoryStatistics() {
		List<VehicleCount> vehicleCounts = new ArrayList<VehicleCount>();
		
		LocalDate currentDate = LocalDate.now();
		String dateStr = new StringBuilder().append(currentDate.getYear()).append("-").append(currentDate.getMonthValue()).append("-").append(currentDate.getDayOfMonth()).toString();
		
		/*
		 * If statistics exist  for the same date then delete existing records and add new records
		 */
		Query queryCount = entityManager.createNativeQuery(
				"SELECT count(*) FROM vehicle_count where date_on = '" + dateStr +"'" );
		
		@SuppressWarnings("rawtypes")
		List countResultList = queryCount.getResultList();
		if(countResultList != null){
			if( (new BigInteger(""+countResultList.get(0)).intValue()) > 0){
				queryCount = null;
				
				Query queryDelete = entityManager.createNativeQuery(
						"delete from vehicle_count where date_on = '" + dateStr + "'" );
				queryDelete.executeUpdate();	
				
				queryDelete = null;
			}
		}
		
		Query queryResult = entityManager.createNativeQuery(
				"SELECT "
				+ " fid, "
				+ " count(IF(`number_of_images` = 0, 1, NULL)) AS no_photos, " //1
				+ " count(IF(`number_of_images` > 0 AND `number_of_images` < 5, 1, NULL)) AS under_5, " //2
				+ " count(IF(LENGTH(`stock_image_urls`) > 2, 1, NULL)) AS stock_images, " //3
				+ " count(IF(`tagline` = '', 1, NULL)) AS no_tagline," //4
				+ " count(IF((`list_price` = '' OR `list_price` = 0) AND (`invoice_price` = '' OR `invoice_price` = 0), 1, NULL)) AS no_price, " //5
				+ " count(IF(vehicle_type = 'New', 1, NULL)) AS new,  " //6
				+ " count(IF(vehicle_type = 'Used', 1, NULL)) AS used, " //7
				+ " count(IF(vehicle_type = 'Used' AND is_certified = 'Y', 1, NULL)) AS cpo " //8
				+ " from dealer_inventory where is_sold = 0 group by fid " );

		List<Object> results = queryResult.getResultList();
		
		Date now = new Date();
		if(results != null && results.size() > 0){		
			for(Object result : results){
				VehicleCount vehicleCount = new VehicleCount();
				
				Object[] objs = (Object[])result;
			
				vehicleCount.setFid(""+objs[0]);
				
				vehicleCount.setNoPhotos(new BigInteger(""+objs[1]).intValue() );
				vehicleCount.setUnder5(new BigInteger(""+objs[2]).intValue());
				vehicleCount.setStockImages(new BigInteger(""+objs[3]).intValue());				
				vehicleCount.setNoTagline(new BigInteger(""+objs[4]).intValue());
				vehicleCount.setNoPrice(new BigInteger(""+objs[5]).intValue());
				vehicleCount.setNewCount(new BigInteger(""+objs[6]).intValue() );
				vehicleCount.setUsedCount(new BigInteger(""+objs[7]).intValue());
				vehicleCount.setCpoCount(new BigInteger(""+objs[8]).intValue());			
				
				vehicleCount.setDateOn(now);
				
				vehicleCounts.add(vehicleCount);
			}
		}
		
		return vehicleCounts;
	}
	
	
	@Transactional
	public void movedTodayDealerInventory(){
		
		Query queryTruncate = entityManager.createNativeQuery(
				"truncate dealer_inventory" );
		queryTruncate.executeUpdate();
		
		Query queryUpdateDealerInventory = entityManager.createNativeQuery(
				new StringBuilder("insert into dealer_stats.dealer_inventory (fid, stock_number, inventory_date, vehicle_type, vehicle_status, list_price, tagline, is_certified, ")
				.append(" vin, created_date, last_modified_date, modified_flag, number_of_images) ")
				.append(" select fid, stock_number, inventory_date, vehicle_type, vehicle_status, list_price, tagline, is_certified, vin, created_date, last_modified_date, ")
				.append(" modified_flag, number_of_images from dealer_stats.dealer_inventory_today" ).toString()  );
		queryUpdateDealerInventory.executeUpdate();
	}
	
	@Transactional
	public void truncateInsertDealerInventorySoldToday(){
		
		/*
		 * truncating dealer_inventory_sold_today so it can used for the today latest file
		 */		
		Query queryTruncate = entityManager.createNativeQuery(
				"truncate dealer_inventory_sold_today" );
		queryTruncate.executeUpdate();
		
		/*
		 * Insert records into dealer_inventory_sold_today that exists in dealer_inventory but not exist in the dealer_inventory_today table.
		 */
		Query queryInsertDealerInventorySold = entityManager.createNativeQuery(
				new StringBuilder("insert into dealer_stats.dealer_inventory_sold_today (fid, stock_number, inventory_date, vehicle_type, vehicle_status, list_price, tagline, is_certified, vin, ")
					.append( " created_date, last_modified_date, modified_flag, number_of_images) ")
					.append(" ( select fid, stock_number, inventory_date, vehicle_type, vehicle_status, list_price, tagline, is_certified, vin, created_date, last_modified_date, modified_flag, number_of_images  ")
					.append(" from dealer_inventory ")
					.append(" where  CONCAT(vin, ',', fid,',',stock_number) not in (select  CONCAT(dit.vin, ',', dit.fid,',',dit.stock_number) from dealer_inventory_today dit) )" ).toString()  );
		queryInsertDealerInventorySold.executeUpdate();
		
		entityManager.close();
	}
	
	@Transactional
	public void filterDealerInventorySoldToday(){
		/*
		 * Delete from dealer_inventory_sold_today that already exist in the dealer_inventory_sold
		 */
		Query queryDeleteDealerInventorySoldToday = entityManager.createNativeQuery(
				new StringBuilder(" delete from dealer_stats.dealer_inventory_sold_today  ")
				.append("  where CONCAT(vin, ',', fid,',',stock_number) in (select  CONCAT(vin, ',', fid,',',stock_number) from dealer_inventory_sold) ").toString()  );
		queryDeleteDealerInventorySoldToday.executeUpdate();
		
		entityManager.close();
	}
	
	@Transactional
	public void moveDealerInventorySoldToday(){
		/*
		 * moved records from dealer_inventory_sold_today to the dealer_inventory_sold table.
		 */
		Query queryMovedDealerInventorySold = entityManager.createNativeQuery(
				new StringBuilder("insert into dealer_stats.dealer_inventory_sold (fid, stock_number, inventory_date, vehicle_type, vehicle_status, list_price, tagline, is_certified, vin, created_date, last_modified_date, modified_flag, number_of_images , created_on) ")
				.append(" select fid, stock_number, inventory_date, vehicle_type, vehicle_status, list_price, tagline, is_certified, vin, created_date, last_modified_date, modified_flag, number_of_images, current_timestamp   ")
				.append(" from dealer_inventory_sold_today ").toString()  );
		queryMovedDealerInventorySold.executeUpdate();
	}
	
	@Transactional
	public void deleteDuplicateDealerInventorySold(){
		Query queryMovedDealerInventorySold = entityManager.createNativeQuery(
				new StringBuilder("delete dis from dealer_stats.dealer_inventory_sold dis ")
				.append(" inner join dealer_stats.dealer_inventory di on  dis.vin = di.vin  ")
				.append(" and dis.fid = di.fid and dis.stock_number = di.stock_number  ")
				.toString()  );
		queryMovedDealerInventorySold.executeUpdate();
	}
	
	
}
