package com.gatehouseauto.datastudio.db.repo;

//import java.sql.Connection;
//import java.sql.DriverManager;
//import java.sql.PreparedStatement;
//import java.sql.ResultSet;
//import java.sql.Statement;
//import java.util.ArrayList;
//import java.util.List;
//
//import com.jcraft.jsch.JSch;
//import com.jcraft.jsch.Session;

public class DashboardSSHRepo {
	
//	private static Connection connection = null;
//	private static Session session= null;
//	
//	
//	public static void main(String arg[]){
//		DashboardSSHRepo dashboardSSHRepo = new DashboardSSHRepo();
//		try{
//			
//			dashboardSSHRepo.getActiveDealers();
//			
//		}catch(Exception e){
//			e.printStackTrace();
//		}
//	}
//	
//	private List<String> getActiveDealers() throws Exception{
//		try{
//			getConnection();
//			
//          Statement statement = connection.createStatement();  
//          ResultSet resultSet = statement.executeQuery("SELECT fid FROM dashboard_1.dealer_information where is_active = 1");  
//          
//          List<String> fids = new ArrayList<>();
//          
//          while(resultSet.next()){  
//        	  fids.add(resultSet.getString("fid"));
//          }
//          
//          resultSet.close();
//          
//          String sql = "SELECT	count(IF(`number_of_images` = 0, 1, NULL)) AS no_photos, "
//          		+ "	count(IF(`number_of_images` > 0 AND `number_of_images` < 5, 1, NULL)) AS under_5, "
//          		+ " count(IF(LENGTH(`stock_image_urls`) > 2, 1, NULL)) AS stock_images, "
//          		+ " count(IF(`tagline` = '', 1, NULL)) AS no_tagline, "
//          		+ " count(IF((`list_price` = '' OR `list_price` = 0) AND (`invoice_price` = '' OR `invoice_price` = 0), 1, NULL)) AS no_price, "
//          		+ " count(IF(`vehicle_type` = 'New', 1, NULL)) AS new, "
//          		+ "	count(IF(`vehicle_type` = 'Used', 1, NULL)) AS used, "
//          		+ " count(IF(`vehicle_type` = 'Used' AND `is_certified` = 'Y', 1, NULL)) AS cpo "
//          		+ " FROM dashboard_1.dealer_inventory WHERE fid = ? "
//          		+ "  AND is_sold = 0";
//          
//          PreparedStatement preparedStatement = connection.prepareStatement(sql);
//          
//          for(String fid : fids){
//        	  preparedStatement.setString(1, fid);     	  
//        	  resultSet = preparedStatement.executeQuery();
//        	  
//        	  System.out.print(" no_photos : "+ resultSet.getInt("no_photos"));
//        	  System.out.print(" under_5 : "+ resultSet.getInt("under_5"));
//        	  System.out.print(" stock_images : "+ resultSet.getInt("stock_images"));
//        	  System.out.print(" no_tagline : "+ resultSet.getInt("no_tagline"));
//        	  System.out.print(" no_price : "+ resultSet.getInt("no_price"));
//        	  System.out.print(" new : "+ resultSet.getInt("new"));
//        	  System.out.print(" used : "+ resultSet.getInt("used"));
//        	  System.out.print(" cpo : "+ resultSet.getInt("cpo"));
//        	  
//        	  System.out.println();
//        	  System.out.println("*************************************");
//        	  System.out.println();
//        	  
//        	  
//        	  resultSet.close();
//        	  Thread.sleep(50);
//          }
//          
//          
//          
//          connection.close();  
//          
//          
//          System.out.println ("Database connection established");
//          System.out.println("DONE");
//			
//			
//		}catch(Exception e){
//			e.printStackTrace();
//			
//		}finally{
//	    	if(connection != null && !connection.isClosed()){
//	    		System.out.println("Closing Database Connection");
//	    		connection.close();
//	    	}
//	    	if(session !=null && session.isConnected()){
//	    		System.out.println("Closing SSH Connection");
//	    		session.disconnect();
//	    	}
//	    }
//		
//		return null;
//	}
//	
//	
//	private void getConnection() throws Exception{
//		int lport=3306;
//	    String rhost="127.0.0.1";
//	    String host="....134";
//	    int rport=3306;
//	    String user="dashboard";
//	    String password="..kA";
//	    String dbuserName = "db_user";
//        String dbpassword = "";
//        String url = "jdbc:mysql://127.0.0.1:"+lport+"/dashboard_1";
//        String driverName="com.mysql.jdbc.Driver";
//        
//
//    	//Set StrictHostKeyChecking property to no to avoid UnknownHostKey issue
//    	java.util.Properties config = new java.util.Properties(); 
//    	config.put("StrictHostKeyChecking", "no");
//    	JSch jsch = new JSch();
//    	session=jsch.getSession(user, host, 22);
//    	session.setPassword(password);
//    	session.setConfig(config);
//    	session.connect();
//    	System.out.println("Connected");
//    	int assinged_port=session.setPortForwardingL(lport, rhost, rport);
//        System.out.println("localhost:"+assinged_port+" -> "+rhost+":"+rport);
//    	System.out.println("Port Forwarded");
//    	
//    	//mysql database connectivity
//        Class.forName(driverName).newInstance();
//        connection = DriverManager.getConnection (url, dbuserName, dbpassword);
//
//	}
//	
	
}
