package com.gatehouseauto.datastudio.db.repo;

import java.util.Date;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.gatehouseauto.datastudio.db.domain.DealerInventory;

@Repository(value="dealerInventoryRepository")
public interface DealerInventoryRepository  extends CrudRepository<DealerInventory, Long> {
	
	@Query("SELECT di FROM DealerInventory di WHERE di.vin = (:vin) AND di.fid = (:fid) AND di.stockNumber = (:stockNumber) ")
	public DealerInventory findByVinFidStockNo(@Param("vin") String vin, @Param("fid") String fid, @Param("stockNumber") String stockNumber);
	
	@Modifying
	@Transactional
	@Query("update DealerInventory di set di.inventoryDate = ?1 , di.vehicleType = ?2 , di.vehicleStatus = ?3, di.isCertified = ?4, di.createdDate = ?5, di.lastModifiedDate = ?6, di.modifiedFlag = ?7 , di.lastUpdate = ?8   where di.vin = ?9 AND di.fid = ?10 AND di.stockNumber = ?11")
	int updateDealerInventory(String inventoryDate, String vehicleType, String vehicleStatus, char isCertified, String createdDate, String lastModifiedDate, String modifiedFlag, Date lastUpdate, String vin, String fid, String stockNumber);
}
