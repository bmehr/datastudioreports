package com.gatehouseauto.datastudio.db.repo;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.gatehouseauto.datastudio.db.domain.VehicleCount;

@Repository(value="vehicleCountRepository")
public interface VehicleCountRepository extends CrudRepository<VehicleCount, Long> {
	public List<VehicleCount> findById(long id);
}

