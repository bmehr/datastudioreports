package com.gatehouseauto.datastudio.constant;

public class PropertyValue {
	public static String APPLICATION_MODE = "";
	
	public static String DB_URL = "";
	public static String DB_USR = "";
	public static String DB_PWD = "";
	
	public static String AWS_ACCESSKEY_ID = "";
	public static String AWS_SECRET_ACCESS_KEY = "";
	public static String AWS_SNS_ARN = "";
	
	public static String IMPORT_FILE_NAME = "";
}
