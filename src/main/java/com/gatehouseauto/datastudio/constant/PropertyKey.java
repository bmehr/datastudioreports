package com.gatehouseauto.datastudio.constant;

public interface PropertyKey {
	public String APPLICATION_MODE = "application-mode";
	
	public String DB_URL_DEV = "db-url-dev";
	public String DB_USR_DEV = "db-usr-dev";
	public String DB_PWD_DEV = "db-pwd-dev";
	
	public String DB_URL_PROD = "db-url-prod";
	public String DB_USR_PROD = "db-usr-prod";
	public String DB_PWD_PROD = "db-pwd-prod";
	
	public String AWS_ACCESSKEY_ID = "aws-accesskey-id";
	public String AWS_SECRET_ACCESS_KEY = "aws-secret-access-key";
	public String AWS_SNS_ARN = "aws-sns-arn";
	
	public String IMPORT_FILE_NAME = "import-file-name";
}
