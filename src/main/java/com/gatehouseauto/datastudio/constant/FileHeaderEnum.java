package com.gatehouseauto.datastudio.constant;

import java.util.HashMap;
import java.util.Map;

public enum FileHeaderEnum {
	FID("fid", 0),  
	STOCK_NUMBER("stock_number", 1), 
	INVENTORY_DATE("inventory_date", 2), 
	VEHIVLE_TYPE("vehicle_type", 3), 
	VEHICLE_STATUS("vehicle_status", 4), 
	INVOICE_PRICE("invoice_price", 5), 
	PACK_AMOUNT("pack_amount", 6), 
	COST("cost", 7), 
	HOLDBACK_AMOUNT("holdback_amount", 8), 
	LIST_PRICE("list_price", 9), 
	MSRP("msrp", 10), 
	LOT_LOCATION("lot_location", 11), 
	VEHICLE_CONDITION("vehicle_condition", 12), 
	TAGLINE("tagline", 13), 
	IS_CERTIFIED("is_certified", 14), 
	CERTIFICATION_NUMBER("certification_number", 15), 
	VIN("vin", 16), 
	MAKE("make", 17), 
	MODEL("model", 18), 
	MODEL_YEAR("model_year", 19), 
	MODEL_CODE("model_code", 20),
	TRIM_LEVEL("trim_level", 21), 
	SUB_TRIM_LEVEL("sub_trim_level", 22), 
	CLASSIFICATION("classification", 23), 
	VEHICLE_TYPE_CODE("vehicle_type_code", 24), 
	ODOMETER("odometer", 25), 
	PAYLOAD_CAPACITY("payload_capacity", 26), 
	SEATING_CAPACITY("seating_capacity", 27), 
	WHEEL_BASE("wheel_base", 28), 
	BODY_DESCRIPTION("body_description", 29), 
	BODY_DOOR_COUNT("body_door_count", 30),
	DRIVE_TRAIN_DESCRIPTION("drive_train_description", 31), 
	ENGINE_DESCRIPTION("engine_description", 32), 
	ENGINE_CYLINDER_COUNT("engine_cylinder_count", 33), 
	TRANSMISSION_DESCRIPTION("transmission_description", 34), 
	TRANSMISSION_TYPE("transmission_type", 35), 
	EXTERIOR_COLOR_DESCRIPTION("exterior_color_description", 36), 
	EXTERIOR_COLOR_BASR_COLOR("exterior_color_base_color", 37), 
	INTERIOR_DESCRIPTION("interior_description", 38), 
	INTERIOR_COLOR("interior_color", 39),
	STANDARD_FEATURES("standard_features", 40), 
	DEALER_ADDED_FEATURES("dealer_added_features", 41), 
	CREATED_DATE("created_date", 42), 
	LAST_MODIFIED_DATE("last_modified_date", 43), 
	MODIFIED_FLAG("modified_flag", 44), 
	NUMBER_OF_IMAGES("number_of_images", 45), 
	IMAGE_URL_PATTERN("image_url_pattern", 46), 
	IMAGE_SET_MODIFICATION_DATE("image_set_modification_date", 47), 
	STOCK_IMAGE_URLS("stock_image_urls", 48);
	
	private String headerName;
	private int headerNo;
	
	private static Map<Integer, String> headerMap = new HashMap<Integer, String>();
	  
	FileHeaderEnum(String headerName, int headerNo){
		this.headerName = headerName;
		this.headerNo = headerNo;
	}

	public String getHeaderName() {
		return headerName;
	}

	public void setHeaderName(String headerName) {
		this.headerName = headerName;
	}

	public int getHeaderNo() {
		return headerNo;
	}

	public void setHeaderNo(int headerNo) {
		this.headerNo = headerNo;
	}

	public static String getHeaderName(int headerNo){
		if(headerMap.isEmpty()){
			for(FileHeaderEnum fileHeaderEnum : values()){
				headerMap.put(Integer.valueOf(fileHeaderEnum.getHeaderNo()), fileHeaderEnum.getHeaderName());
			}
		}
		return headerMap.get(Integer.valueOf(headerNo));		
	}


}
