package com.gatehouseauto.datastudio.reportgenerator;

import java.io.File;
import java.io.FileReader;
import java.io.Reader;
import java.time.LocalDate;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVRecord;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.gatehouseauto.datastudio.constant.ApplicationConstants;
import com.gatehouseauto.datastudio.constant.FileHeaderEnum;
import com.gatehouseauto.datastudio.db.domain.DealerInventoryToday;
import com.gatehouseauto.datastudio.db.domain.VehicleCount;
import com.gatehouseauto.datastudio.db.repo.CustomRepository;
import com.gatehouseauto.datastudio.db.repo.DealerInventoryRepository;
import com.gatehouseauto.datastudio.db.repo.DealerInventorySoldRepo;
import com.gatehouseauto.datastudio.db.repo.DealerInventoryTodayRepo;
import com.gatehouseauto.datastudio.db.repo.VehicleCountRepository;
import com.gatehouseauto.datastudio.sns.SNSHandler;
import com.gatehouseauto.datastudio.util.CommonUtils;
import com.gatehouseauto.datastudio.util.LoggingUtils;
import com.gatehouseauto.datastudio.util.PropertyLoaderUtil;
import com.gatehouseauto.datastudio.util.VastFTPHandler;

@Component(value="dealerInvImportCounts")
public class DealerInvImportCounts {
	
	@Autowired
	protected DealerInventoryRepository dealerInventoryRepository;
	
	@Autowired
	protected DealerInventorySoldRepo dealerInventorySoldRepo;
	
	@Autowired
	protected DealerInventoryTodayRepo dealerInventoryTodayRepo;
	
	@Autowired
	protected VehicleCountRepository vehicleCountRepository;	
	
	@Autowired
	protected CustomRepository customRepository;
	
	public void run() throws Exception{
		long startTime = System.currentTimeMillis();
		StringBuilder sbLog = new StringBuilder();
		
        LocalDate todayLocalDate = LocalDate.now();
        if(ApplicationConstants.TMPI_ARGUMENT_FILE_DATE == null || "".equals(ApplicationConstants.TMPI_ARGUMENT_FILE_DATE)){
        	ApplicationConstants.TMPI_ARGUMENT_FILE_DATE = todayLocalDate.getYear() + CommonUtils.leftPadding(todayLocalDate.getMonthValue()) + CommonUtils.leftPadding(todayLocalDate.getDayOfMonth());
        	ApplicationConstants.VAST_FILE_DATE = CommonUtils.leftPadding(todayLocalDate.getDayOfMonth()) + "_" + CommonUtils.leftPadding(todayLocalDate.getMonthValue()) + "_" + todayLocalDate.getYear() ;
        }        
		
		/*
		 * Using dealer_inventory_today table to download latest inventory file into database
		 */
		customRepository.truncateDealerInventoryToday();
		
		/*
		 * Download file from ftp
		 */
		
		String downloadedZipFileName = null;
		while(downloadedZipFileName == null || "".equals(downloadedZipFileName)){
			
			if(downloadedZipFileName != null){
				break;
			}
			
			downloadedZipFileName = VastFTPHandler.downlaodTodayFile();
			if(downloadedZipFileName == null || "".equals(downloadedZipFileName)){
				SNSHandler.sendSimpleMessage("File not exist will retry after 30 minutues again.........", "@DataStudioReports! Inventory-Count-FailedFTP");
				Thread.sleep(60000 * 30); //30 minutes
			}
		}
		
		LoggingUtils.logInfo("Downloaded File Name ......" + downloadedZipFileName);
		
		/*
		 * database operations
		 */
		
		String csvFileName = PropertyLoaderUtil.usedPath + downloadedZipFileName;
		
		Reader fileReader = new FileReader(csvFileName);
		Iterable<CSVRecord> records = CSVFormat.EXCEL.parse(fileReader);

		boolean skipHeader = true;
		for (CSVRecord record : records) {
			
			if(skipHeader){
				skipHeader = false;
				continue;
			}
			 
			DealerInventoryToday dealerInventoryToday = new DealerInventoryToday();			 
			for(int i=0; record.size() > i; i++){
				 
				if(i==0){
					dealerInventoryToday.setFid(record.get(FileHeaderEnum.FID.getHeaderNo()));					 
				} else if(i==1){
					dealerInventoryToday.setStockNumber(record.get(FileHeaderEnum.STOCK_NUMBER.getHeaderNo()));					 
				}else if(i==2){
					dealerInventoryToday.setInventoryDate(record.get(FileHeaderEnum.INVENTORY_DATE.getHeaderNo()));					 
				}else if(i==3){
					dealerInventoryToday.setVehicleType(record.get(FileHeaderEnum.VEHIVLE_TYPE.getHeaderNo()));					 
				}else if(i==4){
					dealerInventoryToday.setVehicleStatus(record.get(FileHeaderEnum.VEHICLE_STATUS.getHeaderNo()));					 
				}else if(i==9){
					dealerInventoryToday.setVehicleStatus(record.get(FileHeaderEnum.LIST_PRICE.getHeaderNo()));					 
				}else if(i==13){
					dealerInventoryToday.setTagline( record.get(FileHeaderEnum.TAGLINE.getHeaderNo()) );					 
				}else if(i==14){
					dealerInventoryToday.setIsCertified(record.get(FileHeaderEnum.IS_CERTIFIED.getHeaderNo()).charAt(0) );					 
				}else if(i==16){
					dealerInventoryToday.setVin(record.get(FileHeaderEnum.VIN.getHeaderNo()));					 
				}else if(i==42){
					dealerInventoryToday.setCreatedDate(record.get(FileHeaderEnum.CREATED_DATE.getHeaderNo()));					 
				}else if(i==43){
					dealerInventoryToday.setLastModifiedDate(record.get(FileHeaderEnum.LAST_MODIFIED_DATE.getHeaderNo()));					 
				}else if(i==44){
					dealerInventoryToday.setModifiedFlag(record.get(FileHeaderEnum.MODIFIED_FLAG.getHeaderNo()));					 
				}else if(i==45){
					String numberOfImages = record.get(FileHeaderEnum.NUMBER_OF_IMAGES.getHeaderNo());
					short imagesNo = 0;
					if("".equals(numberOfImages) || numberOfImages==null){
						imagesNo = 0;
					}else{
						imagesNo = Short.valueOf(numberOfImages);
					}
					dealerInventoryToday.setNumberOfImages(imagesNo);					 
				}else if(i==48){
					dealerInventoryToday.setStockImageUrls( record.get(FileHeaderEnum.STOCK_IMAGE_URLS.getHeaderNo()).getBytes() );					 
					
				}
			}
			 
			dealerInventoryToday.setCreatedOn(new Date());
			dealerInventoryToday.setLastUpdate(new Date());
			dealerInventoryToday.setIsSold(Short.valueOf((short)0));
			 
			dealerInventoryTodayRepo.save(dealerInventoryToday);				
		}
		
		if(fileReader != null){
			fileReader.close();
		}
		
		/*
		 * Removed csv file
		 */
		File csvFileToDelete = new File(csvFileName);
		if(csvFileToDelete.exists()){
			csvFileToDelete.delete();
		}		
		
		/*
		 * if the dealer_inventory tables empty then just moved everything into the dealer_inventory from dealer_inventory_today table.
		 */
		long dealerInventoryCounts = dealerInventoryRepository.count();
		if(dealerInventoryCounts == 0){
			customRepository.movedTodayDealerInventory();
		}else{			
			customRepository.truncateInsertDealerInventorySoldToday();	
			customRepository.filterDealerInventorySoldToday();
			customRepository.moveDealerInventorySoldToday();
			customRepository.deleteDuplicateDealerInventorySold();
			
			customRepository.movedTodayDealerInventory();
		}		
		 
		/*
		 * Calculate dealer inventory statistics
		 */
		
		List<VehicleCount> vehicleCounts = customRepository.getDealerInventoryStatistics();
		if(!vehicleCounts.isEmpty()){
			vehicleCountRepository.save(vehicleCounts);
		}
		
		
		/*
		 * Logic for the Sold vehicles
		 */
		
		
		sbLog.append(csvFileName).append("   -  \n  ");
		long differenceInMilli = System.currentTimeMillis()-startTime;
		sbLog
		.append(" -minutes:")
		.append(TimeUnit.MILLISECONDS.toMinutes(differenceInMilli))
		.append(" -hours:")
		.append(TimeUnit.MILLISECONDS.toHours(differenceInMilli));
		
		/*
		 * End log/notification
		 */
		ApplicationConstants.TMPI_ARGUMENT_FILE_DATE = null;
		
		SNSHandler.sendSimpleMessage(sbLog.toString(), "@DataStudioReports! Inventory-Count-Good");
		 
		LoggingUtils.requestEndLog(startTime, "End... File Name : " + csvFileName); 

	}
	
}
