package com.gatehouseauto.datastudio.reportgenerator;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;


@Component(value="reportRunner")
public class ReportRunner {
	
	@Autowired
	protected DealerInvImportCounts dealerInvImportCounts;
	
	@Autowired
	protected TMPiFileImportsInvStats tMPiFileImportsInvStats;
	
	@Autowired
	protected TMPiIntFBStatsmMonthly tMPiIntFBStatsmMonthly;
	
	
	@Scheduled(cron="0 0 16 * * *")
	public void run() throws Exception{
		
//    	ApplicationConstants.TMPI_ARGUMENT_FILE_DATE = "20180729";

       dealerInvImportCounts.run();
       tMPiFileImportsInvStats.run(); 
       tMPiIntFBStatsmMonthly.run();   
           
//       LesaReportGenerator lesaReportGenerator = (LesaReportGenerator)ctx.getBean("lesaReportGenerator");
//       lesaReportGenerator.run();
	}
	
}
