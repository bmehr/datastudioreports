package com.gatehouseauto.datastudio.reportgenerator;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.gatehouseauto.datastudio.sns.SNSHandler;
import com.gatehouseauto.datastudio.tmpi.db.repo.TmpiIntFBMonthlyCustomRepository;
import com.gatehouseauto.datastudio.util.LoggingUtils;

@Component(value="tMPiIntFBStatsmMonthly")
public class TMPiIntFBStatsmMonthly {
	
	@Autowired
	protected TmpiIntFBMonthlyCustomRepository tmpiIntFBMonthlyCustomRepository;
	
	public void run() throws Exception{
		long startTimestamp = System.currentTimeMillis();
		StringBuilder sbLog = new StringBuilder();
		
		LoggingUtils.logInfo("TMPi Intelligent Facebook Statistics started......");
		
		tmpiIntFBMonthlyCustomRepository.truncateTmpiFbStatsCache();
		
		List<String> campaignIds = tmpiIntFBMonthlyCustomRepository.getCampaignIds();
		
		System.out.println( "campaignIds size() ......................  : " + (campaignIds==null?0:campaignIds.size()));
		for(String campaignId : campaignIds){
			tmpiIntFBMonthlyCustomRepository.updateIntFBMonthlyStatsCache(campaignId);
		}
		
		/*
		 * Truncate tmpi_intelli_fb_stats tables from update from the cache
		 */
		tmpiIntFBMonthlyCustomRepository.truncateTmpiFbStats();
		
		/*
		 * Update tmpi_intelli_fb_stats from the cache
		 */
		tmpiIntFBMonthlyCustomRepository.updateIntStatsFromCache();
		
		long differenceInMilli = System.currentTimeMillis()-startTimestamp;
		
		sbLog
		.append(" campaign Counts : ").append(campaignIds.size())
		.append(" \n  -minute:")
		.append(TimeUnit.MILLISECONDS.toMinutes(differenceInMilli))
		.append(" -hours:")
		.append(TimeUnit.MILLISECONDS.toHours(differenceInMilli));
		SNSHandler.sendSimpleMessage(sbLog.toString(), "@DataStudioReports! TMPI-Stats-Good");
		
		LoggingUtils.requestEndLog(startTimestamp, "@DataStudioReports! TMPI-Stats-Good "); 
		
	}
	

}
