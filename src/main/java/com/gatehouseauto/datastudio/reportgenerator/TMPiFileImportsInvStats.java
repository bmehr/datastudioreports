package com.gatehouseauto.datastudio.reportgenerator;

import java.io.File;
import java.time.LocalDate;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.gatehouseauto.datastudio.constant.ApplicationConstants;
import com.gatehouseauto.datastudio.db.EmailWSDBReader;
import com.gatehouseauto.datastudio.db.domain.DealerInformation;
import com.gatehouseauto.datastudio.db.domain.TmpiProcessedFilesLog;
import com.gatehouseauto.datastudio.sns.SNSHandler;
import com.gatehouseauto.datastudio.tmpi.FTPTMPiHandler;
import com.gatehouseauto.datastudio.tmpi.constants.TMPiFilesEnum;
import com.gatehouseauto.datastudio.tmpi.db.TMPiDAO;
import com.gatehouseauto.datastudio.tmpi.db.repo.DealerInformationRepo;
import com.gatehouseauto.datastudio.tmpi.db.repo.TmpiCustomRepository;
import com.gatehouseauto.datastudio.tmpi.db.repo.TmpiProcessedFilesLogRepo;
import com.gatehouseauto.datastudio.util.CommonUtils;
import com.gatehouseauto.datastudio.util.LoggingUtils;
import com.gatehouseauto.datastudio.util.PropertyLoaderUtil;
import com.gatehouseauto.datastudio.util.UnZip;

@Component(value="tMPiFileImportsInvStats")
public class TMPiFileImportsInvStats {
	
	@Autowired
	protected TMPiDAO tMPiDAO;
	
	@Autowired
	protected DealerInformationRepo dealerInformationRepo;
	
	@Autowired
	protected TmpiProcessedFilesLogRepo tmpiProcessedFilesLogRepo;
	
	@Autowired
	protected TmpiCustomRepository tmpiCustomRepository;
	
	public void run() throws Exception{
		long startTimestamp = System.currentTimeMillis();
		StringBuilder sbLog = new StringBuilder();
		
        LocalDate todayLocalDate = LocalDate.now();
        
        if(ApplicationConstants.TMPI_ARGUMENT_FILE_DATE == null || "".equals(ApplicationConstants.TMPI_ARGUMENT_FILE_DATE)){
        	ApplicationConstants.TMPI_ARGUMENT_FILE_DATE = todayLocalDate.getYear() + CommonUtils.leftPadding(todayLocalDate.getMonthValue()) + CommonUtils.leftPadding(todayLocalDate.getDayOfMonth());
        }
		
		/*
		 * updating dealerinformation from the emailws table
		 */
		List<DealerInformation> dealers = EmailWSDBReader.getDealers();
		dealerInformationRepo.deleteAll();
		dealerInformationRepo.save(dealers);
		
		String downloadedZipFileName = FTPTMPiHandler.downlaodTodayFile();
		
		String zipFolder = downloadedZipFileName.substring(0, downloadedZipFileName.length()-4);
		String unZipFolderFullPath = PropertyLoaderUtil.usedPath+zipFolder+"/";
		
		sbLog.append(" downloadedZipFileName: ").append(downloadedZipFileName);
		
		UnZip unZip = new UnZip();		
		unZip.unZipIt(PropertyLoaderUtil.usedPath+downloadedZipFileName, unZipFolderFullPath);
		File zipFileToDelete = new File(PropertyLoaderUtil.usedPath+downloadedZipFileName);	
		if(zipFileToDelete.exists()){
			zipFileToDelete.delete();
		}
		
		File exportedDirectory = new File(unZipFolderFullPath);
		for(File exportedFile : exportedDirectory.listFiles()){
			String fileName = exportedFile.getName();
			if(fileName.contains(TMPiFilesEnum.CUSTOMER_LIST.getFileName())){
				long startTime = System.currentTimeMillis();
				
				tMPiDAO.customerList(unZipFolderFullPath+exportedFile.getName());
				
				log(startTime, sbLog, " \n CUSTOMER_LIST!");
				
			}else if(fileName.contains(TMPiFilesEnum.FLIGHT_INFO.getFileName())){
				long startTime = System.currentTimeMillis();
				
				tMPiDAO.flightInfo(unZipFolderFullPath+exportedFile.getName());
				
				log(startTime, sbLog,  " \n FLIGHT_INFO!");
				
			}else if(fileName.contains(TMPiFilesEnum.DISPLAY_CLICKS.getFileName())){
				long startTime = System.currentTimeMillis();
				
				tMPiDAO.displayClicks(unZipFolderFullPath+exportedFile.getName());
				
				log(startTime, sbLog, " \n DISPLAY_CLICKS!");
				
			}else if(fileName.contains(TMPiFilesEnum.EMAIL_LEADS.getFileName())){
				long startTime = System.currentTimeMillis();
				
				tMPiDAO.emailLeads(unZipFolderFullPath+exportedFile.getName());
				
				log(startTime, sbLog, " \n MAIL_LEADS!");
				
			}else if(fileName.contains(TMPiFilesEnum.PHONE_LEADS.getFileName())){
				long startTime = System.currentTimeMillis();
				
				tMPiDAO.phoneLeads(unZipFolderFullPath+exportedFile.getName());
				
				log(startTime, sbLog, " \n PHONE_LEADS!");
				
			}else if(fileName.contains(TMPiFilesEnum.VEHICLE_LIST.getFileName())){
				long startTime = System.currentTimeMillis();
				
				tMPiDAO.vehicleList(unZipFolderFullPath+exportedFile.getName());
				
				log(startTime, sbLog, " \n VEHICLE_LIST!");
				
			}else if(fileName.contains(TMPiFilesEnum.VEHICLE_SUMMARY.getFileName())){
				long startTime = System.currentTimeMillis();
				
				tMPiDAO.vehicleSummary(unZipFolderFullPath+exportedFile.getName());
				
				log(startTime, sbLog, " \n VEHICLE_SUMMARY!");
				
			}else if(fileName.contains(TMPiFilesEnum.VEHICLE_PERFORMANCE.getFileName())){
				long startTime = System.currentTimeMillis();
				
				tMPiDAO.vehiclePerformance(unZipFolderFullPath+exportedFile.getName());
				
				log(startTime, sbLog, " \n VEHICLE_PERFORMANCE!");
			}
		}
		
		tMPiDAO.commitAllTrx();
		
		TmpiProcessedFilesLog tmpiProcessedFilesLog = new TmpiProcessedFilesLog();
		tmpiProcessedFilesLog.setFullFileName(PropertyLoaderUtil.usedPath+downloadedZipFileName);
		tmpiProcessedFilesLog.setCreatedOn(new Date());
		
		long differenceInMilli = System.currentTimeMillis()-startTimestamp;
		
		if(exportedDirectory != null && exportedDirectory.exists()){
			CommonUtils.deleteDirectory(exportedDirectory);
		}
		
		sbLog
		.append(" \n  -minute:")
		.append(TimeUnit.MILLISECONDS.toMinutes(differenceInMilli))
		.append(" -hours:")
		.append(TimeUnit.MILLISECONDS.toHours(differenceInMilli));
		String message = sbLog.toString();
		if(message.length() > 445){
			message = message.substring(0, 444);
		}
		tmpiProcessedFilesLog.setMessage(message);
		tmpiProcessedFilesLogRepo.save(tmpiProcessedFilesLog);
		
		/*
		 * Post Processing
		 */
		tmpiCustomRepository.updateProductsPostProcess();
		tmpiCustomRepository.updateDailyStats();
		tmpiCustomRepository.updateMonthlyStats();
		
		/*
		 * It is important to null out below value because then it will get intialize with current date on next schedule trigger
		 */
		ApplicationConstants.TMPI_ARGUMENT_FILE_DATE = null;
		
		SNSHandler.sendSimpleMessage(sbLog.toString(), "@DataStudioReports! TMPI-INV-Good");
		
		LoggingUtils.requestEndLog(startTimestamp, "@DataStudioReports! TMPI-INV-Good "); 
		
	}
	
	private void log(long startTime, StringBuilder sbLog, String tableName){
		long differenceInMilli = System.currentTimeMillis()-startTime;
		
		sbLog.append(" @ ").append(tableName)
			.append(" -sec:")
			.append(TimeUnit.MILLISECONDS.toSeconds(differenceInMilli))
			.append(" -minute:")
			.append(TimeUnit.MILLISECONDS.toMinutes(differenceInMilli));
	}

}
