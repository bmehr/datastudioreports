package com.gatehouseauto.datastudio.util;

import java.io.FileInputStream;
import java.io.InputStream;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.LinkOption;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Properties;

import com.gatehouseauto.datastudio.constant.PropertyKey;
import com.gatehouseauto.datastudio.constant.PropertyValue;

public class PropertyLoaderUtil {
	
	public static String usedPath = "";
	
	private static final String WINDOWS_PATH = "C:\\temp\\datastudioreports\\";
	private static final String NON_WINDOWS_PATH = "/opt/datastudioreports/";
	
	public static void loadProperties(){		
		String propertyFileName = "";
		String currentOS = System.getProperty("os.name").toLowerCase();
		try{
            if(currentOS.contains("window")){
            	usedPath = WINDOWS_PATH;
            	System.setProperty("log4j.configurationFile", "log4j2_win.xml");
            	
            	propertyFileName = "c://temp//datastudioreports//datastudioreports.properties";
            	
            	System.out.println("Operating System Window............................................ : " + currentOS);
            	
            }else{
            	usedPath = NON_WINDOWS_PATH;
            	System.setProperty("log4j.configurationFile", "log4j2.xml");
            	
            	propertyFileName = "/opt/datastudioreports/datastudioreports.properties";
            	
            	System.out.println("Operating System NON Window............................................ : "+ currentOS);
            } 
			
			Properties properties = new Properties();
			
			final Path pathFile = Paths.get(propertyFileName);
			if (Files.exists(pathFile, LinkOption.NOFOLLOW_LINKS)) {
				
				System.out.println("Going to load property file from local directory...");
				
			   properties.load(new FileInputStream(propertyFileName));
			    
			 }else {				 
				propertyFileName = "datastudioreports.properties";
				System.out.println("Cant load property file from local directory, now trying to load property file from the classpath : "+propertyFileName);
				 
				InputStream inputStream = PropertyLoaderUtil.class.getClassLoader().getResourceAsStream(propertyFileName);
				if (inputStream == null) {
					
					System.out.println("ResourceAsStream failed, now will try to load using SystemResource..");
					
					URL url = ClassLoader.getSystemResource(propertyFileName);
					
					System.out.println("url:"+url);
					
					if (url != null) {
						inputStream = url.openStream();
					}
				}	
				
			    properties.load(inputStream);			      
			 }
			
			PropertyValue.APPLICATION_MODE =  properties.getProperty(PropertyKey.APPLICATION_MODE);
			 
			System.out.println("..................APPLICATION_MODE..........................  "+PropertyValue.APPLICATION_MODE);
	
			 if(PropertyValue.APPLICATION_MODE != null && 
					   !"".equals(PropertyValue.APPLICATION_MODE.trim()) &&
					   "prod".equals(PropertyValue.APPLICATION_MODE)){
					   
				  LoggingUtils.logInfo("property file in production ...."); 
				  
				  PropertyValue.DB_URL =  properties.getProperty(PropertyKey.DB_URL_PROD);
				  PropertyValue.DB_USR =  properties.getProperty(PropertyKey.DB_USR_PROD); 
				  PropertyValue.DB_PWD =  properties.getProperty(PropertyKey.DB_PWD_PROD);
				  
			 }else{
				  LoggingUtils.logInfo("property file in dev -or local  ...."); 
				  
				  PropertyValue.DB_URL =  properties.getProperty(PropertyKey.DB_URL_DEV);
				  PropertyValue.DB_USR =  properties.getProperty(PropertyKey.DB_USR_DEV); 
				  PropertyValue.DB_PWD =  properties.getProperty(PropertyKey.DB_PWD_DEV);
			   }
			 
			  PropertyValue.AWS_ACCESSKEY_ID =  properties.getProperty(PropertyKey.AWS_ACCESSKEY_ID); 
			  PropertyValue.AWS_SECRET_ACCESS_KEY =  properties.getProperty(PropertyKey.AWS_SECRET_ACCESS_KEY);
			  PropertyValue.AWS_SNS_ARN =  properties.getProperty(PropertyKey.AWS_SNS_ARN);
			  PropertyValue.IMPORT_FILE_NAME =  properties.getProperty(PropertyKey.IMPORT_FILE_NAME);
		}catch(Exception e){
			e.printStackTrace();
			
			String exception = LoggingUtils.getExceptionString(e);
			LoggingUtils.logInfo(exception);
		}
	}
}
