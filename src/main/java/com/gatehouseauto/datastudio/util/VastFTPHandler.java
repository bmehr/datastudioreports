package com.gatehouseauto.datastudio.util;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import org.apache.commons.net.ftp.FTP;
import org.apache.commons.net.ftp.FTPClient;
import org.apache.commons.net.ftp.FTPFile;
import org.apache.commons.net.ftp.FTPReply;

import com.gatehouseauto.datastudio.constant.ApplicationConstants;


public class VastFTPHandler {
	private static String FTPSERVER_ADDRESS = "ftp.vast.com";
	private static String FTPSERVER_USR = "ghmhomenet";  // dmigatehousemedia  , 
	private static String FTPSERVER_PWD = "8VCqcxXdgZ";   // PUuWs8pJ, 
//	private static String FTPSERVER_FOLDERNAME = "Export";


	private static FTPClient ftp = new FTPClient();
	
	public static String downlaodTodayFile(){
		try{
			ftp.connect(FTPSERVER_ADDRESS);		
			
			if(!ftp.login(FTPSERVER_USR, FTPSERVER_PWD))
            {
                ftp.logout();
                return null;
            }
			
            int reply = ftp.getReplyCode();
            //FTPReply stores a set of constants for FTP reply codes.
            if (!FTPReply.isPositiveCompletion(reply))
            {
                ftp.disconnect();
                return null;
            }
            
            ftp.enterLocalPassiveMode();
            ftp.setFileType(FTP.BINARY_FILE_TYPE);
//            ftp.changeWorkingDirectory(FTPSERVER_FOLDERNAME);
            
            FTPFile ftpFiles[] = ftp.listFiles()	;
            List<FTPFile> todayFTPFile = new ArrayList<FTPFile>();
            for(FTPFile ftpFile : ftpFiles){
             	String fileName = ftpFile.getName().substring(0, ftpFile.getName().length()-4);
            	 
            	if(fileName.contains(ApplicationConstants.VAST_FILE_DATE)){
            		todayFTPFile.add(ftpFile);
            	}
            	 
            	 
            }
            
            String selectedFileName = "";

            if(todayFTPFile.size() == 2){
            	Calendar fileCalendar1 = todayFTPFile.get(0).getTimestamp();
            	Calendar fileCalendar2 = todayFTPFile.get(1).getTimestamp();
            	
            	if(fileCalendar1.after(fileCalendar2)){
            		selectedFileName = todayFTPFile.get(0).getName();
            	}else{
            		selectedFileName = todayFTPFile.get(1).getName();
            	}
             }else if(todayFTPFile.size() == 1){
            	 selectedFileName = todayFTPFile.get(0).getName();
             }else{
            	 return null;
             }
            
//           ////////////////////////
//            if(PropertyValue.IMPORT_FILE_NAME != null && !"".equals(PropertyValue.IMPORT_FILE_NAME)){
//            	selectedFileName = PropertyValue.IMPORT_FILE_NAME;
//            } 
//          ////////////////
            	
            File downloadFile = new File(PropertyLoaderUtil.usedPath+selectedFileName);
            
            OutputStream outputStream = new BufferedOutputStream(new FileOutputStream(downloadFile));
            ftp.retrieveFile(selectedFileName, outputStream);
            outputStream.close();
 
            return selectedFileName;
		}catch(Exception e){
			e.printStackTrace();
		}

		return null;
	}

}
