package com.gatehouseauto.datastudio.util;

import java.io.File;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Date;

public class CommonUtils {
	
	public static String leftPadding(int intValue){
		String strValue = intValue+"";
		if(strValue.length() == 1){
			return "0" + intValue;
		}
		
		return strValue;
	}

	public static Date parseDateObject(Object dateObject){
		Date returnValue = null;
		try{
			returnValue = (Date)dateObject;
		}catch(Exception e){}
		return returnValue;
	}
	
	public static int parseIntString(String intString){
		int returnValue = 0;
		try{
			returnValue = new Integer(intString.trim());
		}catch(Exception e){}
		return returnValue;
	}

	public static float parseFloatString(String floatString){
		float returnValue = 0f;
		try{
			returnValue = new Float(floatString.trim());
		}catch(Exception e){}
		return returnValue;
	}
	
	public static LocalDate toLocalDate(String dateString){
		String format = "yyyyMMdd";
		try{
			DateTimeFormatter formatter = DateTimeFormatter.ofPattern(format);
			return LocalDate.parse(dateString, formatter);
	 
		}catch(Exception e){}
		
		return null;
	}
	
	public static Date toDate(String dateString, String format){
		Date resultDate = null;
		try{
			SimpleDateFormat formatter = new SimpleDateFormat(format);
			synchronized(formatter){
				resultDate = formatter.parse(dateString);
				return resultDate;
			}
	 
		}catch(Exception e){}
		
		return resultDate;
	}
	
	
	public static boolean deleteDirectory(File directoryToBeDeleted) {
	    File[] allContents = directoryToBeDeleted.listFiles();
	    if (allContents != null) {
	        for (File file : allContents) {
	            deleteDirectory(file);
	        }
	    }
	    return directoryToBeDeleted.delete();
	}

}
